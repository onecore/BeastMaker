# -*- mode: python -*-
a = Analysis(['BeastMaker.py'],
             pathex=['c:\\Users\\Mark\\Desktop\\Sources\\BeastMaker'],
             hiddenimports=[],
             hookspath=None,
             runtime_hooks=None)
pyz = PYZ(a.pure)
exe = EXE(pyz,
          a.scripts,
          a.binaries,
          a.zipfiles,
          a.datas,
          name='BeastMaker.exe',
          debug=False,
          strip=None,
          upx=True,
          console=False , version='version.txt', icon='bmu.ico')
