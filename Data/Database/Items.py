Menus = [
                "Helms",
                "Armors",
                "Pants",
                "Gloves",
                "Boots",
                "Swords",
                "Axes",
                "Scepters",
                "Spears",
                "Bows",
                "Staffs",
                "Shields",
                "Wings/Jewel/Seeds",
                "Pets / Accessories",
                "Scrolls",
                "Others"
        ]

Dict = {
                "Helms":7,
                "Armors":8,
                "Pants":9,
                "Gloves":10,
                "Boots":11,
                "Swords":0,
                "Axes":1,
                "Scepters":2,
                "Spears":3,
                "Bows":4,
                "Staffs":5,
                "Shields":6,
                "Wings/Jewel/Seeds":12,
                "Pets / Accessories":13,
                "Scrolls":15,
                "Others":14
                }
