try:
    import cred
    # git ignored.
except ImportError:
    pass

versions = None

try:
    import DS2
    versions = 'SQL Server'
except ImportError: pass

try:
    import DS1
    versions = 'SQL Native Client'
except ImportError: pass


try:
    import DS3
    versions = 'SQL Server Native Client 10.0'
except ImportError: pass

AutoAuth = True                   # True/False
SQLVersion = 'SQL Native Client' # SQL Native Client / SQL Server
SQLConnection = cred.creds  # Local,Remote Address or Instance/Domain
SQLUser = cred.credu                    # Username
SQLPass = cred.cred   # Password
SQLDbase = 'MuOnline'
InstantOnly = False               # Only Dropper no DB Conn
UseSerialAuth = False              # Serial Authentication instead of IP
IsSa = [0,0]            # file Supplied from Management (hashed)

