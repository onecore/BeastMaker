# // Weapons Data
Scepters = [

"Mace",
"Morning Star",
"Fail",
"Great Hammer",
"Morning Star",
"Crystal Sword",
"Chaos Dragon Axe",
"Elemental Mace",
"Battle Scepter",
"Master Scepter",
"Great Scepter",
"Lord Scepter",
"Great Lord Scepter",
"Divine Scepter of ARC..",
"Solay Scepter",
"Shining Scepter",
"Frost Mace",
"Absolute Scepter",
"Stryker Scepter",

]

Spears = [
    "LightSpear",
    "Spear",
    "DragonLance",
    "GiantTrident",
    "SerpentSpear",
    "DoublePoleaxe",
    "Halberd",
    "Berdysh",
    "GreatScythe",
    "BillofBalrog",
    "DragonSpear",
    "Beuroba",
]


Axe = [


"Small Axe",
"Hand Axe",
"Double Axe",
"Tomahawk",
"Elven Axe",
"Battle Axe",
"Nikkea Axe",
"Larkan Axe",

]

Staffs = ['SkullStaff', 'AngelicStaff', 'SerpentStaff', 'ThunderStaff', 'GorgonStaff', 'LegendaryStaff', 'StaffofResurrection', 'ChaosLightingStaff', 'StaffofDestruction', 'DragonSoulStaff', 'DivineStaffofArchangel', 'StaffofKundun', 'GrandViperStaff', 'PlatinaStaff', 'MisteryStaff', 'ViolentWindStaff', 'RedWingStaff', 'AncientStaff', 'BlackRoseStaff', 'AuraStaff', 'LiliumStaff', 'BookofSahamutt', 'BookofNeil', 'BookofGhostPhantom', 'DeadlyStaff', 'ImperialStaff', 'MerlinStaff', 'Chromaticksstaff', 'RavenStick', '//SE', 'DivineStickofArchangel']

Shields = ['SmallShield', 'HornShield', 'KiteShield', 'ElvenShield', 'Buckler', 'DragonSlayerShield', 'SkullShield', 'SpikedShield', 'TowerShield', 'IronShield', 'BigRoundShield', 'SerpentShield', 'BronzeShield', 'ChaosShield', 'LegendaryShield', 'GrandSoulShield', 'ElementalShield', 'CrimsonGlory', 'SalamanderShield', 'FrostBarrier', 'GuardianShiled', 'CrossShield']

# wings / jewels 12
Misc1 = ['WingsofElf', 'WingsofHeaven', 'WingsofSatan', 'WingsofSpirits', 'WingsofSoul', 'WingsofDragon', 'WingsofDarkness', 'OrbofTwistingSlash', 'HealingOrb', 'OrbofGreaterDefense', 'OrbofGreaterDamage', 'OrbofSummoning', 'OrbofRagefulBlow', 'OrbofImpale', 'OrbofGreaterFortitude', 'JewelofChaos', 'OrbofFireSlash', 'OrbofPenetration', 'OrbofIceArrow', 'OrbofDeathStab', 'ScrollofFireBurst', 'ScrollofSummon', 'ScrollofCriticalDamage', 'ScrollofElectricSpark', 'PietraSegreta', 'JewelofBlessCompresse', 'JewelofSoulCompresse', 'RedRibbonBox', 'GreenRibbonBox', 'BlueRibbonBox', 'ScrollofFireScream', 'WingofStorm', 'WingofVortex', 'WingofIllusion', 'WingofDoom', 'MantleofMonarch', 'WingofMistery', 'WingofDespair', 'WingofViolentWind', 'OrbofExplosion', 'OrbofFiveShot', 'OrbofReduceDmgShield', 'OrbofSwordSlash', 'ScrollOfBirds', 'WingofFighter', 'RageWings', 'SeedofFire', 'SeedofWater', 'SeedofIce', 'SeedofWind', 'SeedofLightning', 'SeedofGround', 'SeedSpear(Empty)', 'SeedSpear(Empty)', 'SeedSpear(Empty)', 'SeedSpear(Empty)', 'SeedSpear(Empty)', 'SeedSpear(Fire)', 'SeedSpear(Water)', 'SeedSpear(Ice)', 'SeedSpear(Wind)', 'SeedSpear(Lightning)', 'SeedSpear(Ground)', 'SeedSpear(Fire)', 'SeedSpear(Water)', 'SeedSpear(Ice)', 'SeedSpear(Wind)', 'SeedSpear(Lightning)', 'SeedSpear(Ground)', 'SeedSpear(Fire)', 'SeedSpear(Water)', 'SeedSpear(Ice)', 'SeedSpear(Wind)', 'SeedSpear(Lightning)', 'SeedSpear(Ground)', 'SeedSpear(Fire)', 'SeedSpear(Water)', 'SeedSpear(Ice)', 'SeedSpear(Wind)', 'SeedSpear(Lightning)', 'SeedSpear(Ground)', 'SeedSpear(Fire)', 'SeedSpear(Water)', 'SeedSpear(Ice)', 'SeedSpear(Wind)', 'SeedSpear(Lightning)', 'SeedSpear(Ground)', '//', 'SmallCapeofLord', 'SmallWingofCurse', 'SmallWingsofElf', 'SmallWingsofHeaven', 'SmallWingsofSatan', 'SmallWingsofRage', 'JewelofLifeBundle', 'JewelofCreationBundle', 'JewelofGuardianBundle', 'GemstoneBundle', 'JewelofHarmonyBundle', 'JewelofChaosBundle', 'LowerRefiningStoneBundle', 'HigherRefiningStoneBundle']

# accs / pets   13
Misc2 = ['GuardianAngel', 'Satan', 'HornofUnicorn', 'HornofDinorant', 'DarkHorse', 'DarkRaven', 'Contract(Summon)', 'RingofIce', 'RingofPoison', 'RingofTransformation', 'Order(Guardian/LifeStone)', 'PendantofLightning', 'PendantofFire', "Loch'sFeather", 'Fruit', 'ScrollofArchangel', 'BloodBone', 'CloakofInvisibility', 'DivineWeaponofArchangel', 'RingofWizardry', 'RingofFire', 'RingofEarth', 'RingofWind', 'RingofMagic', 'PendantofIce', 'PendantofWind', 'PendantofWater', 'PendantofAbility', 'ArmorofGuardman', 'CapeofLord', 'Spirit', 'SplinterofArmor', 'BlessofGuardian', 'ClawofBeast', 'PieceofHorn', 'BrokenHorn', 'HornofFenrir', 'MoonstonePendant', 'Ring', 'Ring', 'Ring', 'GMRing', 'GreenSymbol', 'PinkSymbol', 'YellowSymbol', 'DevilSquarefreeadmission', 'BloodCastlefreetickets', 'KalimaTicketfreetickets', 'OldScroll', 'IllusionSorcererCovenant', 'ScrollofBlood', 'CondorFlame', 'CondorFeather', 'ResetFruitStrength', 'ResetFruitQuickness', 'ResetFruitHealth', 'ResetFruitEnergy', 'ResetFruitControl', 'SealofMobility', 'Indulgence', 'IllusionTempleTicket', 'SealofHealing', 'SealofDivinity', 'Demon', 'SpiritofGuardian', 'InvitationtoSantaVillage', 'PetRudolf', 'SnowmanTransformationRing', 'TalismanofResurrection', 'TalismanofMobility', 'Sword/Mace/Spear', 'Staff', 'Bow/Crossbow', 'Scepter', 'Stick', 'PandaRing', 'PetPanda', 'TalismanofGuardian', 'TalismanofItemProtection', 'TalismanofWingsofSatan', 'TalismanofWingsofHeaven', 'TalismanofWingsofElf', 'TalismanofWingofCurse', 'TalismanofCapeofEmperor', 'TalismanofWingsofDragon', 'TalismanofWingsofSoul', 'TalismanofWingsofSpirits', 'TalismanofWingofDespair', 'TalismanofWingsofDarkness', 'MasterSealofAscension', 'MasterSealofWealth', "Gladiator'sHonor", 'SealofStrength', 'MageomsakaerikteoCard', 'DakeurodeuCharacterCards', 'Characterslotkey', 'PartyEXPScroll', 'MaxAGBoostAura', 'MaxSDBoostAura', 'PetUnicorn', "LethalWizard'sRing", 'SapphireRing', 'RubyRing', 'TopazRing', 'AmethystRing', 'RubyNecklace', 'EmeraldNecklace', 'SapphireNecklace', 'SealofWealth', 'MinimumVitalityPotion', 'LowVitalityPotion', 'MediumVitalityPotion', 'HighVitalityPotion', 'OpenAccessTickettoChaos', 'SkeletonTransformationRing', 'PetSkeleton', 'PaidChannelAccessTicket', 'OpenAccessTickettoDoppel', 'OpenAccessTickettoVarka', 'OpenAccessTickettoVarkama', 'Maejoperphase', 'GoatStatues', 'Ohkeucham', 'Meyipeulcham', 'Goldeunohkeucham', 'Goldeunmeyipeulcham', 'Oldshoe', 'LuckyticketstArmor', 'stLuckyPantsTickets', 'LuckyticketstHelm', 'LuckyGlovestticket', 'BootsstLuckyTicket', 'LuckyticketndArmor', 'ndLuckyPantsTickets', 'LuckyticketndHelm', 'LuckyGlovendticket', 'BootsndLuckyTicket']

# others 14
Misc3 =['Apple', 'SmallHealingPotion', 'HealingPotion', 'LargeHealingPotion', 'SmallManaPotion', 'ManaPotion', 'LargeManaPotion', 'SiegePotion', 'Antidote', 'Alcohol', 'TownPortalScrool', 'BoxofLuck', 'Cuoredeldrago', 'JewelofBless', 'JewelofSoul', 'Zen', 'JewelofLife', 'DevilSquareEye', 'DevilSquareKey', 'DevilSquareInvitation', 'RemedyofLove', 'Rena', 'JewelofCreation', 'ScrolloftheEmperor', 'BrokenSword', 'TearofElf', 'SoulShardofWizard', 'GateperKalima', 'SymbolofKundun', 'JewelofGuardian', 'PinkChocolateBox', 'RedChocolateBox', 'BlueChocolateBox', 'SmallSDPotion', 'SDPotion', 'LargeSDPotion', 'SmallComplexPotion', 'ComplexPotion', 'LargeComplexPotion', 'Gemstone', 'JewelofHarmony', 'LowerRefiningStone', 'HigherRefiningStone', 'HaloweenPumpkin', 'HaloweenScroll', 'HaloweenScroll', 'HaloweenScroll', 'HaloweenStrangeItem', 'HaloweenPumpkin', 'SkyEventTicket', 'GMGift', 'TalismanofLuck', 'ChaosCard', 'GreenChaosBox', 'RedChaosBox', 'PurpleChaosBox', 'RareItemTicket', 'RareItemTicket', 'RareItemTicket', 'RareItemTicket', 'RareItemTicket', 'Firecracker', 'CursedCastleWater', 'FlameofDeathBeamKnight', 'HornofHellMaine', 'FeatherofPhoenixofDarkn', "Churchill'sEyes", 'EliteHealingPotion', 'EliteManaPotion', 'ScrollofQuickness', 'ScrollofDefense', 'ScrollofWrath', 'ScrollofWizardry', 'ScrollofHealth', 'ScrollofMana', 'ElixirofStrength', 'ElixirofAgility', 'ElixirofHealth', 'ElixirofEnergy', 'ElixirofControl', 'RareItemTicket', 'CherryBlossomPlayBox', 'CherryBlossomWine', 'CherryBlossomRiceCake', 'CherryBlossomFlowerPetal', 'WhiteCherryBlossomBranch', 'RedCherryBlossomBranch', 'GoldenCherryBlossomBranch', 'SummonerCharacterCard', 'ChaosCardGold', 'ChaosCardRare', 'MediumEliteHealingPotion', 'ChaosCardMini', 'TalismanofChaosAssembly', 'ScrollofBattle', 'ScrollofStrength', 'ChristmasFirecracker', 'LuckyCoin', 'NoName', 'GaionsOrder', 'FirstSecromiconFragment', 'SecondSecromiconFragment', 'ThirdSecromiconFragment', 'FourthSecromiconFragment', 'FifthSecromiconFragment', 'SixthSecromiconFragment', 'CompleteSecromicon', 'SignofDimensions', 'MirrorofDimensions', 'SilverKey', 'GoldKey', 'MasterSkillReset', 'StatAdjustment', 'CharacterRelocationService', 'CharacterRenameService', 'ServerRelocationService', 'PremiumPackage', 'GoblinGoldCoin', 'SealedGoldenBox', 'SealedSilverBox', 'GoldenBox', 'SilverBox', 'PackageBox', 'DayPass', 'DayPass', 'DayPassPoints', 'DayPassPoints', 'HourPass', 'HourPass', 'HourPass', 'EliteSDPotion', 'PackageBoxA', 'PackageBoxB', 'PackageBoxC', 'PackageBoxD', 'PackageBoxE', 'PackageBoxF', 'ScrollofHealing', 'ShiningJewelryCase', 'ElegantJewelryCase', 'SteelJewelryCase', 'OldJewelryCase', 'RareItemTicket', 'RareItemTicket', 'RareItemTicket', 'RareItemTicket', 'RareItemTicket', 'RareItemTicket', 'levelreferral', 'levelconfirmationofreferral', 'Stardust', 'Kalteuseok', 'ArmorofTantalus', 'Ashesofthebutchersticks', 'Greenbox', 'Redbox', 'Borabitsangja', 'Extensionofthejewelry', 'Riseofthejewelry', 'MagicBackpack', 'VaultExpansionCertif']

# scrolls 15
Misc4 = ['ScrollofPoison', 'ScrollofMeteorite', 'ScrollofLightning', 'ScrollofFireBall', 'ScrollofFlame', 'ScrollofTeleport', 'ScrollofIce', 'ScrollofTwister', 'ScrollofEvilSpirits', 'ScrollofHellFire', 'ScrollofPowerWave', 'ScrollofAquaBeam', 'ScrollofBlast', 'ScrollofInferno', 'ScrollofMassTeleport', 'ScrollofSoulBarrier', 'ScrollofDecay', 'ScrollofIceStorm', 'ScrollofNova', 'ScrollofDrainLife', 'ScrollofChainLighting', 'ScrollofElectricSurge', 'ScrollofDamageReflex', 'ScrollofNight', 'ScrollofSleep', 'ScrollofMagicSpeedUp', 'ScrollofMagicDefenseUp', 'ScrollofStorm', 'ScrollofMagicCircle', 'ScrollofLigthingStorm', 'ScrollofChainDrive', 'ScrollofDarkSide', 'ScrollofDragonLore', 'ScrollofDragonSlayer', 'ScrollofRedArmorIgnore', 'ScrollofFitness', 'Defensesuccessrateincrease']


Bows = [
"ShortBow",
"Bow",
"ElvenBow",
"BattleBow",
"TigerBow",
"SilverBow",
"ChaosNatureBow",
"Bolt",
"Crossbow",
"GoldenCrossbow",
"Arquebus",
"LightCrossbow",
"SerpentCrossbow",
"BluewingCrossbow",
"AquagoldCrossbow",
"Arrows",
"SaintCrossbow",
"CelestialBow",
"DivineCBofArchangel",
"GreatReignCrossbow",
"ArrowViperBow",
"SylphWindBow",
"AlbatrossBow",
"StingerBow",
"AirLynBow",
]

Sword = [
"Kris",
"Short Sword",
"Rapier",
"Katana",
"Sword of Assassin",
"Blade",
"Gladius",
"Falchion",
"Serpent Sword",
"Salamander",
"Light Sabe",
"Legary Sword",
"Helical Sword",
"Double Blade",
"Lightning Sword",
"Giant Sword",
"Sword of Destruction",
"Spirit Sword",
"Thunder Blade",
"Sword of Archangel",
"Knight Blade",
"Black Reign Blade",
"Bone Blade",
"Explosion Blade",
"DayBreak",
"Sword Dancer",
"Flamberge",
"Sword Bracker",
"Imperial Sword",


]
