# // Sockets List

Sockets = [
'[  EMPTY SLOT  ]',
'[Fire]IncreaseSkillAttackPower',
'[Fire]IncreaseAttackSpeed',
'[Fire]IncreaseMaximumDamageskill',
'[Fire]IncreaseMinimumDamageskill',
'[Fire]DamageSkillPower',
'[Fire]DecreaseAGuse',


'[Water]IncreaseDefenseSRate',
'[Water]IncreaseDefense',
'[Water]IncreaseDefenseShield',
'[Water]DamageReduction',
'[Water]DamageReflection',


'[Ice]MonsterDieGetLife',
'[Ice]MonsterDieGetMana',
'[Ice]IncreaseSkillAttackPower',
'[Ice]IncreaseAttackSRate',
'[Ice]IncreaseDurability',


'[Wind]IncreaseLifeRecovery',
'[Wind]IncreaseMaximumLife',
'[Wind]IncreaseMaximumMana',
'[Wind]IncreaseManaAutoRecovery',
'[Wind]IncreaseMaximumAG',
'[Wind]IncreaseAGAmount',


'[Lightning]IncreaseExcellentDamage',
'[Lightning]IncreaseExcellentDamageII',
'[Lightning]IncreaseCriticalDamage',
'[Lightning]IncreaseCriticalDamageII',

'[Earth]IncreaseStamina',
'NO SOCKET',

]