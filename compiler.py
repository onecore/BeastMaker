__author__ = 'Mark Anthony Pequeras'
__software__ = 'Invictuz Online Game'
__year__ = '2013'
__python__ = '2.7'
__developers__ = 'CoreSEC Software Development Group'

from distutils.core import setup
import py2exe, sys, os, glob

# Now you need to pass arguments to setup
# windows is a list of scripts that have their own UI and
# thus don't need to run in a console.


origIsSystemDLL = py2exe.build_exe.isSystemDLL
def isSystemDLL(pathname):
        if os.path.basename(pathname).lower() in ("msvcp71.dll", "dwmapi.dll"):
                return 0
        return origIsSystemDLL(pathname)
py2exe.build_exe.isSystemDLL = isSystemDLL


version = '4.4.1b'

setup(windows=[{'script':'BeastMaker.py','uac_info': 'requireAdministrator','icon_resources':[(0,'bmu.ico')]}],

        name = 'BeastMaker',
        version = version,
        description = 'CoreSEC Softwares, Ca',
 

        options={

# And now, configure py2exe by passing more options;

          'py2exe': {

# This is magic: if you don't add these, your .exe may
# or may not work on older\newer versions of windows.

              "dll_excludes": [
                  "MSVCP90.dll",
                  "MSWSOCK.dll",
                  "mswsock.dll",
                  "powrprof.dll",
                  "QtCore4.dll","QtGui4.dll","QtNetwork4.dll"
                  ],

# Py2exe will not figure out that you need these on its own.
# You may need one, the other, or both.

              'includes': [
                  'sip',
                  'decimal',
                  'datetime',
                  'PyQt4.QtNetwork',
                  ],

# Optional: make one big exe with everything in it, or
# a folder with many things in it. Your choice
            "dist_dir":"BeastMaker-"+str(version),
            "bundle_files": 1,
            "compressed":True,
            "optimize":2,
            "xref":False, 
          }
      },

# Qt's dynamically loaded plugins and py2exe really don't
# get along.



data_files = [

#            ('phonon_backend', [   #phonon_backend
#                'C:\Python27\App/Lib\site-packages\PyQt4\plugins\phonon_backend\phonon_ds94.dll'
#                ]),

 #           ('imageplugins', [   #imageplugins
 #           'C:\Python27\App/lib\site-packages\PyQt4\plugins\imageformats\qgif4.dll',
#            'C:\Python27\App/lib\site-packages\PyQt4\plugins\imageformats\qjpeg4.dll',
#            'C:\Python27\App/lib\site-packages\PyQt4\plugins\imageformats\qsvg4.dll',
#            ]),

            ('coresec/imageformats', [   #imageformats
            'C:\Python27\App/lib\site-packages\PyQt4\plugins\imageformats\qgif4.dll',
            'C:\Python27\App/lib\site-packages\PyQt4\plugins\imageformats\qjpeg4.dll',
            'C:\Python27\App/lib\site-packages\PyQt4\plugins\imageformats\qsvg4.dll',
            'C:\Python27\App/lib\site-packages\PyQt4\plugins\imageformats\qico4.dll',
            ]),

            ],

# If you choose the bundle above, you may want to use this, too.
    zipfile='coresec.dll',
)