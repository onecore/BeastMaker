__author__ = [ 'Mark Anthony R. Pequeras' ]
__language__ = [ 'Python', 'Kivy', 'Cython', 'C' ]
__version__ = 1.0
__distributor__ = [ 'CoreSEC Softwares, CA' ]
__license__ = [ 'MIT' ]
__website__ = 'http://www.coresecsoftware.com/markie'

# Database Simula

# 'Price','Discount','Ancient','Options','Plus','Parts','Weapons','Pet','Bonus','pic1','pic2','pic3','pic4','pic5'

# Blade Knight
BKTier = {

    "Bragi Dark Phoenix": [ '45', '6%', 'Bragi', 'Full', '15', 'Ancients with Excellents', 'Upon Request or Same Level Weapons', 'Gold Pet (1)',
                            '12 Months (Forum VIP Tag)',
                            'images/BK/T2/bragi/brag1.png',
                            'images/BK/T2/bragi/brag2.png',
                            'images/BK/T2/bragi/brag3.png',
                            'images/BK/T2/bragi/brag3.png',
                            'images/BK/T2/bragi/brag4.png' ]

    , "Maahes Black Dragon": [ '45', '2%', 'Maahes', 'Full', '15', 'Ancients with Excellents', 'Upon Request or Same Level Weapons', 'Gold Pet (1)',
                               '12 Months (Forum VIP Tag)',
                            'images/BK/T2/maahes/Maahes1.png',
                            'images/BK/T2/maahes/Maahes2.jpg',
                            'images/BK/T2/maahes/Maahes3.jpg',
                            'images/BK/T2/maahes/Maahes4.jpg',
                            'images/BK/T2/maahes/Maahes4.jpg' ]

    , "Moro Great Dragon": [ '45', '2%', 'Moro', 'Full', '15', 'Ancients with Excellents', 'Upon Request or Same Level Weapons', 'Gold Pet (1)',
                               '12 Months (Forum VIP Tag)',
                            'images/BK/T2/moro/Moros1.png',
                            'images/BK/T2/moro/Moros2.jpg',
                            'images/BK/T2/moro/Moros3.jpg',
                            'images/BK/T2/moro/Moros4.jpg',
                            'images/BK/T2/moro/Moros4.jpg' ]

    , "Hegatonui Dragon Knight": [ '55', '2%', 'Hegatonui', 'Full', '15', 'Ancients with Excellents', 'Upon Request or Same Level Weapons', 'Gold Pet (1)',
                                   '12 Months (Forum VIP Tag) + FO 2nd and 3rd Job Wing' ,
                            'images/BK/T2/hegaton/Hegatonui1.png',
                            'images/BK/T2/hegaton/Hegatonui2.jpg',
                            'images/BK/T2/hegaton/Hegatonui3.png',
                            'images/BK/T2/hegaton/Hegatonui3.png',
                            'images/BK/T2/hegaton/Hegatonui4.jpg' ]

}

BKUber = {
    "Warrior Leather": [ '10', '0%', 'Warrior', 'Full', '15', 'Ancients with Excellents', 'Upon Request or Same Level Weapons', 'Gold Pet (1)',
                         '3 Months (Forum VIP Tag) + 3 Accessories of your choice',
                            'images/BK/T1/warrior/Warrior1.png',
                            'images/BK/T1/warrior/Warrior2.jpg',
                            'images/BK/T1/warrior/Warrior3.jpg',
                            'images/BK/T1/warrior/Warrior4.jpg',
                            'images/BK/T1/warrior/Warrior4.jpg' ]

    , "Anonymous Leather": [ '10', '0%', 'Anonymous', 'Full', '15', 'Ancients with Excellents', 'Upon Request or Same Level Weapons', 'Gold Pet (1)',
                             '3 Months (Forum VIP Tag) + 3 Accessories of your choice',
                            'images/BK/T2/anon/Anonymous1.png',
                            'images/BK/T2/anon/Anonymous2.jpg',
                            'images/BK/T2/anon/Anonymous3.jpg',
                            'images/BK/T2/anon/Anonymous4.jpg',
                            'images/BK/T2/anon/Anonymous4.png' ]

    , "Hyperion Bronze": [ '10', '0%', 'Hyperion', 'Full', '15', 'Ancients with Excellents', 'Upon Request or Same Level Weapons', 'Gold Pet (1)',
                           '3 Months (Forum VIP Tag) + 3 Accessories of your choice',
                           'images/BK/T1/hyperion/Hyperion1.png',
                            'images/BK/T1/hyperion/Hyperion2.jpg',
                            'images/BK/T1/hyperion/Hyperion3.jpg',
                            'images/BK/T1/hyperion/Hyperion4.jpg',
                            'images/BK/T1/hyperion/Hyperion4.jpg' ]
    ,
"Mist Bronze": [ '10', '0%', 'Mist', 'Full', '15', 'Ancients with Excellents', 'Upon Request or Same Level Weapons', 'Gold Pet (1)', '3 Months (Forum VIP Tag) + 3 Accessories of your choice',
                            'images/BK/T2/mist/Mist1.png',
                            'images/BK/T2/mist/Mist2.jpg',
                            'images/BK/T2/mist/Mist3.jpg',
                            'images/BK/T2/mist/Mist4.jpg',
                            'images/BK/T2/mist/Mist4.jpg', ]
    ,
"Garuda Brass": [ '13', '10%', 'Garuda', 'Full', '15', 'Ancients with Excellents', 'Upon Request or Same Level Weapons', 'Gold Pet (1)', '3 Months (Forum VIP Tag) + 3 Accessories of your choice',
                            'images/BK/T1/garuda/Garuda1.png',
                            'images/BK/T1/garuda/Garuda2.jpg',
                            'images/BK/T1/garuda/Garuda3.jpg',
                            'images/BK/T1/garuda/Garuda4.jpg',
                            'images/BK/T1/garuda/Garuda4.jpg' ]
    ,
"Cloud Brass": [ '13', '10%', 'Cloud', 'Full', '15', 'Ancients with Excellents', 'Upon Request or Same Level Weapons', 'Gold Pet (1)', '3 Months (Forum VIP Tag) + 3 Accessories of your choice',
                            'images/BK/T2/cloud/Cloud1.png',
                            'images/BK/T2/cloud/Cloud2.jpg',
                            'images/BK/T2/cloud/Cloud3.jpg',
                            'images/BK/T2/cloud/Cloud4.jpg',
                            'images/BK/T2/cloud/Cloud4.jpg' ]
    ,
"Eplete Scale": [ '20', '10%', 'Eplete', 'Full', '15', 'Ancients with Excellents', 'Upon Request or Same Level Weapons', 'Gold Pet (1)', '3 Months (Forum VIP Tag) + 3 Accessories of your choice',
                            'images/BK/T1/eplete/Eplete1.png',
                            'images/BK/T1/eplete/Eplete2.jpg',
                            'images/BK/T1/eplete/Eplete3.jpg',
                            'images/BK/T1/eplete/Eplete4.jpg',
                            'images/BK/T1/eplete/Eplete4.jpg' ]

    , "Berserker Scale": [ '27', '10%', 'Berserker', 'Full', '15', 'Ancients with Excellents', 'Upon Request or Same Level Weapons', 'Gold Pet (1)',
                           '3 Months (Forum VIP Tag) + 3 Accessories of your choice',
                            'images/BK/T2/berserk/Berserker1.png',
                            'images/BK/T2/berserk/Berserker2.jpg',
                            'images/BK/T2/berserk/Berserker3.jpg',
                            'images/BK/T2/berserk/Berserker4.jpg',
                            'images/BK/T2/berserk/Berserker4.jpg' ]

    , "Kantata Plate": [ '30', '10%', 'Kantata', 'Full', '15', 'Ancients with Excellents', 'Upon Request or Same Level Weapons', 'Gold Pet (1)',
                         '3 Months (Forum VIP Tag) + 3 Accessories of your choice',
                            'images/BK/T1/kantata/Kantata1.png',
                            'images/BK/T1/kantata/Kantata2.jpg',
                            'images/BK/T1/kantata/Kantata3.jpg',
                            'images/BK/T1/kantata/Kantata4.jpg',
                            'images/BK/T1/kantata/Kantata4.jpg' ]

,
"Rave Plate": [ '30', '10%', 'Rave', 'Full', '15', 'Ancients with Excellents', 'Upon Request or Same Level Weapons', 'Gold Pet (1)', '3 Months (Forum VIP Tag) + 3 Accessories of your choice',
                            'images/BK/T2/rave/Rave1.png',
                            'images/BK/T2/rave/Rave2.jpg',
                            'images/BK/T2/rave/Rave3.jpg',
                            'images/BK/T2/rave/Rave4.jpg',
                            'images/BK/T2/rave/Rave4.jpg' ]
    ,
"Hyon Dragon": [ '35', '10%', 'Hyon', 'Full', '15', 'Ancients with Excellents', 'Upon Request or Same Level Weapons', 'Gold Pet (1)', '3 Months (Forum VIP Tag) + 3 Accessories of your choice',
                            'images/BK/T1/hyon/Hyon1.png',
                            'images/BK/T1/hyon/Hyon2.jpg',
                            'images/BK/T1/hyon/Hyon3.jpg',
                            'images/BK/T1/hyon/Hyon4.jpg',
                            'images/BK/T1/hyon/Hyon4.jpg' ]

    , "Vicious Dragon": [ '35', '10%', 'Vicious', 'Full', '15', 'Ancients with Excellents', 'Upon Request or Same Level Weapons', 'Gold Pet (1)',
                          '3 Months (Forum VIP Tag) + 3 Accessories of your choice',

                            'images/BK/T2/vicious/Vicious1.png',
                            'images/BK/T2/vicious/Vicious2.jpg',
                            'images/BK/T2/vicious/Vicious3.jpg',
                            'images/BK/T2/vicious/Vicious4.jpg',
                            'images/BK/T2/vicious/Vicious4.jpg' ]


}
BKS4 = {}
BKWeap = {}
BKMisc = {""}



# Muse Elf
METier = {
"Serket Iris": [ '25', '10%', 'Serket', 'Full', '15', 'Ancients with Excellents', 'Upon Request or Same Level Weapons', 'Gold Pet (1)', '3 Months (Forum VIP Tag) + 3 Accessories of your choice',
                 'images/none.jpg', 'images/none.jpg', 'images/none.jpg', 'images/none.jpg', 'images/none.jpg' ],

"Castol Sylphid Ray": [ '27', '10%', 'Castol', 'Full', '15', 'Ancients with Excellents', 'Upon Request or Same Level Weapons', 'Gold Pet (1)', '3 Months (Forum VIP Tag) + 3 Accessories of your choice',
                 'images/none.jpg', 'images/none.jpg', 'images/none.jpg', 'images/none.jpg', 'images/none.jpg' ]
    , "Frigg Divine": [ '25', '10%', 'Frigg', 'Full', '15', 'Ancients with Excellents', 'Upon Request or Same Level Weapons', 'Gold Pet (1)',
                             '3 Months (Forum VIP Tag) + 3 Accessories of your choice', 'images/none.jpg', 'images/none.jpg', 'images/none.jpg',
                             'images/none.jpg', 'images/none.jpg' ],

"Diones Red Spirit": [ '30', '0%', 'Diones', 'Full', '15', 'Ancients with Excellents', 'Upon Request or Same Level Weapons', 'Gold Pet (1)',
                             '3 Months (Forum VIP Tag) + 3 Accessories of your choice', 'images/none.jpg', 'images/none.jpg', 'images/none.jpg',
                             'images/none.jpg', 'images/none.jpg' ],
}
MEUber = {
"Drake Vine": [ '10', '10%', 'Drake', 'Full', '15', 'Ancients with Excellents', 'Upon Request or Same Level Weapons', 'Gold Pet (1)', '3 Months (Forum VIP Tag) + 3 Accessories of your choice',
                'images/none.jpg', 'images/none.jpg', 'images/none.jpg', 'images/none.jpg', 'images/none.jpg' ]
    ,
"Fase Silk": [ '12', '10%', 'Fase', 'Full', '15', 'Ancients with Excellents', 'Upon Request or Same Level Weapons', 'Gold Pet (1)', '3 Months (Forum VIP Tag) + 3 Accessories of your choice',
               'images/none.jpg', 'images/none.jpg', 'images/none.jpg', 'images/none.jpg', 'images/none.jpg' ]
    ,
"Gaia Silk": [ '12', '10%', 'Gaia', 'Full', '15', 'Ancients with Excellents', 'Upon Request or Same Level Weapons', 'Gold Pet (1)', '3 Months (Forum VIP Tag) + 3 Accessories of your choice',
               'images/none.jpg', 'images/none.jpg', 'images/none.jpg', 'images/none.jpg', 'images/none.jpg' ]
    ,
"Odin Wind": [ '12', '10%', 'Odin', 'Full', '15', 'Ancients with Excellents', 'Upon Request or Same Level Weapons', 'Gold Pet (1)', '3 Months (Forum VIP Tag) + 3 Accessories of your choice',
               'images/none.jpg', 'images/none.jpg', 'images/none.jpg', 'images/none.jpg', 'images/none.jpg' ]
    ,
"Elvian Wind": [ '13', '10%', 'Elvian', 'Full', '15', 'Ancients with Excellents', 'Upon Request or Same Level Weapons', 'Gold Pet (1)', '3 Months (Forum VIP Tag) + 3 Accessories of your choice',
                 'images/none.jpg', 'images/none.jpg', 'images/none.jpg', 'images/none.jpg', 'images/none.jpg' ]
    ,
"Argo Spirit": [ '13', '10%', 'Argos', 'Full', '15', 'Ancients with Excellents', 'Upon Request or Same Level Weapons', 'Gold Pet (1)', '3 Months (Forum VIP Tag) + 3 Accessories of your choice',
                 'images/none.jpg', 'images/none.jpg', 'images/none.jpg', 'images/none.jpg', 'images/none.jpg' ]
    ,
"Karis Spirit": [ '15', '10%', 'Karis', 'Full', '15', 'Ancients with Excellents', 'Upon Request or Same Level Weapons', 'Gold Pet (1)', '3 Months (Forum VIP Tag) + 3 Accessories of your choice',
                  'images/none.jpg', 'images/none.jpg', 'images/none.jpg', 'images/none.jpg', 'images/none.jpg' ]
    , "Aruan Guardian": [ '15', '10%', 'Aruan', 'Full', '15', 'Ancients with Excellents', 'Upon Request or Same Level Weapons', 'Gold Pet (1)',
                          '3 Months (Forum VIP Tag) + 3 Accessories of your choice', 'images/none.jpg', 'images/none.jpg', 'images/none.jpg',
                          'images/none.jpg', 'images/none.jpg' ]
    , "Gywen Guardian": [ '15', '10%', 'Gywen', 'Full', '15', 'Ancients with Excellents', 'Upon Request or Same Level Weapons', 'Gold Pet (1)',
                          '3 Months (Forum VIP Tag) + 3 Accessories of your choice', 'images/none.jpg', 'images/none.jpg', 'images/none.jpg',
                          'images/none.jpg', 'images/none.jpg' ]
}
MES4 = {"Drake Vine": [ '10', '10%', 'Drake', 'Full', '15', 'Ancients with Excellents', 'Upon Request or Same Level Weapons', 'Gold Pet (1)',
                        '3 Months (Forum VIP Tag) + 3 Accessories of your choice''images/none.jpg', 'images/none.jpg', 'images/none.jpg',
                        'images/none.jpg', 'images/none.jpg' ]}
MEBow = {}
MEMisc = {}



# Soul Master
SMTier = {"Alvitr Grand Soul": [ '10', '10%', 'Alvitr', 'Full', '15', 'Ancients with Excellents', 'Upon Request or Same Level Weapons', 'Gold Pet (1)',
                          '3 Months (Forum VIP Tag) + 3 Accessories of your choice''images/none.jpg', 'images/none.jpg', 'images/none.jpg',
                          'images/none.jpg', 'images/none.jpg' ],

"Taros Dark Soul": [ '10', '10%', 'Taros', 'Full', '15', 'Ancients with Excellents', 'Upon Request or Same Level Weapons', 'Gold Pet (1)',
                          '3 Months (Forum VIP Tag) + 3 Accessories of your choice''images/none.jpg', 'images/none.jpg', 'images/none.jpg',
                          'images/none.jpg', 'images/none.jpg' ],

"Bes Eclipse": [ '10', '10%', 'Bes', 'Full', '15', 'Ancients with Excellents', 'Upon Request or Same Level Weapons', 'Gold Pet (1)',
                          '3 Months (Forum VIP Tag) + 3 Accessories of your choice''images/none.jpg', 'images/none.jpg', 'images/none.jpg',
                          'images/none.jpg', 'images/none.jpg' ],

"Hades Venom Mist": [ '10', '10%', 'Hades', 'Full', '15', 'Ancients with Excellents', 'Upon Request or Same Level Weapons', 'Gold Pet (1)',
                          '3 Months (Forum VIP Tag) + 3 Accessories of your choice''images/none.jpg', 'images/none.jpg', 'images/none.jpg',
                          'images/none.jpg', 'images/none.jpg' ],



          }


SMUber = {"Apollo Pad": [ '10', '10%', 'Apollo', 'Full', '15', 'Ancients with Excellents', 'Upon Request or Same Level Weapons', 'Gold Pet (1)',
                          '3 Months (Forum VIP Tag) + 3 Accessories of your choice''images/none.jpg', 'images/none.jpg', 'images/none.jpg',
                          'images/none.jpg', 'images/none.jpg' ]
    , "Bernake Pad": [ '7', '10%', 'Bernake', 'Full', '15', 'Ancients with Excellents', 'Upon Request or Same Level Weapons', 'Gold Pet (1)',
                       '3 Months (Forum VIP Tag) + 3 Accessories of your choice''images/none.jpg', 'images/none.jpg', 'images/none.jpg',
                       'images/none.jpg', 'images/none.jpg' ]
    , "Evis Bone": [ '7', '10%', 'Evis', 'Full', '15', 'Ancients with Excellents', 'Upon Request or Same Level Weapons', 'Gold Pet (1)',
                     '3 Months (Forum VIP Tag) + 3 Accessories of your choice''images/none.jpg', 'images/none.jpg', 'images/none.jpg',
                     'images/none.jpg', 'images/none.jpg' ]
    , "Sylion Bone": [ '7', '10%', 'Sylion', 'Full', '15', 'Ancients with Excellents', 'Upon Request or Same Level Weapons', 'Gold Pet (1)',
                       '3 Months (Forum VIP Tag) + 3 Accessories of your choice''images/none.jpg', 'images/none.jpg', 'images/none.jpg',
                       'images/none.jpg', 'images/none.jpg' ]
    , "Minet Sphinx": [ '14', '10%', 'Minet', 'Full', '15', 'Ancients with Excellents', 'Upon Request or Same Level Weapons', 'Gold Pet (1)',
                        '3 Months (Forum VIP Tag) + 3 Accessories of your choice''images/none.jpg', 'images/none.jpg', 'images/none.jpg',
                        'images/none.jpg', 'images/none.jpg' ]
    , "Heras Sphinx": [ '15', '10%', 'Heras', 'Full', '15', 'Ancients with Excellents', 'Upon Request or Same Level Weapons', 'Gold Pet (1)',
                        '3 Months (Forum VIP Tag) + 3 Accessories of your choice''images/none.jpg', 'images/none.jpg', 'images/none.jpg',
                        'images/none.jpg', 'images/none.jpg' ]
    , "Enis Legendary": [ '25', '10%', 'Enis', 'Full', '15', 'Ancients with Excellents', 'Upon Request or Same Level Weapons', 'Gold Pet (1)',
                          '3 Months (Forum VIP Tag) + 3 Accessories of your choice''images/none.jpg', 'images/none.jpg', 'images/none.jpg',
                          'images/none.jpg', 'images/none.jpg' ]
    , "Anubis Legendary": [ '25', '10%', 'Anubis', 'Full', '15', 'Ancients with Excellents', 'Upon Request or Same Level Weapons', 'Gold Pet (1)',
                            '3 Months (Forum VIP Tag) + 3 Accessories of your choice''images/none.jpg', 'images/none.jpg', 'images/none.jpg',
                            'images/none.jpg', 'images/none.jpg' ]
          }
SMS4 = {}
SMStaffs = {}
SMMisc = {}


# Rage Fighter
RFTier = {"Trites Phoenix Soul": [ '45', '10%', 'Trites', 'Full', '15', 'Ancients with Excellents', 'Upon Request or Same Level Weapons', 'Gold Pet (1)',
                             '3 Months (Forum VIP Tag) + 3 Accessories of your choice', 'images/none.jpg', 'images/none.jpg', 'images/none.jpg',
                             'images/none.jpg', 'images/none.jpg' ],

"Magni Piercing": [ '45', '10%', 'Magni', 'Full', '15', 'Ancients with Excellents', 'Upon Request or Same Level Weapons', 'Gold Pet (1)',
                             '3 Months (Forum VIP Tag) + 3 Accessories of your choice', 'images/none.jpg', 'images/none.jpg', 'images/none.jpg',
                             'images/none.jpg', 'images/none.jpg' ],
"Horus Holy Storm": [ '45', '10%', 'Horus', 'Full', '15', 'Ancients with Excellents', 'Upon Request or Same Level Weapons', 'Gold Pet (1)',
                             '3 Months (Forum VIP Tag) + 3 Accessories of your choice', 'images/none.jpg', 'images/none.jpg', 'images/none.jpg',
                             'images/none.jpg', 'images/none.jpg' ],

          }
RFUber = {
    "Warrior Leather": [ '10', '0%', 'Warrior', 'Full', '15', 'Ancients with Excellents', 'Upon Request or Same Level Weapons', 'Gold Pet (1)',
                         '3 Months (Forum VIP Tag) + 3 Accessories of your choice',
                            'images/BK/T1/warrior/Warrior1.png',
                            'images/BK/T1/warrior/Warrior2.jpg',
                            'images/BK/T1/warrior/Warrior3.jpg',
                            'images/BK/T1/warrior/Warrior4.jpg',
                            'images/BK/T1/warrior/Warrior4.jpg' ]

    , "Anonymous Leather": [ '10', '0%', 'Anonymous', 'Full', '15', 'Ancients with Excellents', 'Upon Request or Same Level Weapons', 'Gold Pet (1)',
                             '3 Months (Forum VIP Tag) + 3 Accessories of your choice',
                            'images/BK/T2/anon/Anonymous1.png',
                            'images/BK/T2/anon/Anonymous2.jpg',
                            'images/BK/T2/anon/Anonymous3.jpg',
                            'images/BK/T2/anon/Anonymous4.jpg',
                            'images/BK/T2/anon/Anonymous4.png' ]

    ,
"Garuda Brass": [ '13', '10%', 'Garuda', 'Full', '15', 'Ancients with Excellents', 'Upon Request or Same Level Weapons', 'Gold Pet (1)', '3 Months (Forum VIP Tag) + 3 Accessories of your choice',
                            'images/BK/T1/garuda/Garuda1.png',
                            'images/BK/T1/garuda/Garuda2.jpg',
                            'images/BK/T1/garuda/Garuda3.jpg',
                            'images/BK/T1/garuda/Garuda4.jpg',
                            'images/BK/T1/garuda/Garuda4.jpg' ]
    ,
"Cloud Brass": [ '13', '10%', 'Cloud', 'Full', '15', 'Ancients with Excellents', 'Upon Request or Same Level Weapons', 'Gold Pet (1)', '3 Months (Forum VIP Tag) + 3 Accessories of your choice',
                            'images/BK/T2/cloud/Cloud1.png',
                            'images/BK/T2/cloud/Cloud2.jpg',
                            'images/BK/T2/cloud/Cloud3.jpg',
                            'images/BK/T2/cloud/Cloud4.jpg',
                            'images/BK/T2/cloud/Cloud4.jpg' ]
    ,
"Eplete Scale": [ '20', '10%', 'Eplete', 'Full', '15', 'Ancients with Excellents', 'Upon Request or Same Level Weapons', 'Gold Pet (1)', '3 Months (Forum VIP Tag) + 3 Accessories of your choice',
                            'images/BK/T1/eplete/Eplete1.png',
                            'images/BK/T1/eplete/Eplete2.jpg',
                            'images/BK/T1/eplete/Eplete3.jpg',
                            'images/BK/T1/eplete/Eplete4.jpg',
                            'images/BK/T1/eplete/Eplete4.jpg' ]

    , "Berserker Scale": [ '27', '10%', 'Berserker', 'Full', '15', 'Ancients with Excellents', 'Upon Request or Same Level Weapons', 'Gold Pet (1)',
                           '3 Months (Forum VIP Tag) + 3 Accessories of your choice',
                            'images/BK/T2/berserk/Berserker1.png',
                            'images/BK/T2/berserk/Berserker2.jpg',
                            'images/BK/T2/berserk/Berserker3.jpg',
                            'images/BK/T2/berserk/Berserker4.jpg',
                            'images/BK/T2/berserk/Berserker4.jpg' ]

    ,

"Vegas Sacred Fire": [ '37', '10%', 'Vegas', 'Full', '15', 'Ancients with Excellents', 'Upon Request or Same Level Weapons', 'Gold Pet (1)',
                             '3 Months (Forum VIP Tag) + 3 Accessories of your choice', 'images/none.jpg', 'images/none.jpg', 'images/none.jpg',
                             'images/none.jpg', 'images/none.jpg' ],
"Chamers Sacred Fire": [ '37', '10%', 'Chamers', 'Full', '15', 'Ancients with Excellents', 'Upon Request or Same Level Weapons', 'Gold Pet (1)',
                             '3 Months (Forum VIP Tag) + 3 Accessories of your choice', 'images/none.jpg', 'images/none.jpg', 'images/none.jpg',
                             'images/none.jpg', 'images/none.jpg' ],
          }
RFS4 = {}
RFWeap = {}
RFMisc = {}


# Magic Gladiator
MGTier = {"Apis Valiant": [ '30', '10%', 'Apis', 'Full', '15', 'Ancients with Excellents', 'Upon Request or Same Level Weapons', 'Gold Pet (1)',
                             '3 Months (Forum VIP Tag) + 3 Accessories of your choice', 'images/none.jpg', 'images/none.jpg', 'images/none.jpg',
                             'images/none.jpg', 'images/none.jpg' ],

"Tyr Thunder Hawk": [ '36', '10%', 'Tyr', 'Full', '15', 'Ancients with Excellents', 'Upon Request or Same Level Weapons', 'Gold Pet (1)',
                             '3 Months (Forum VIP Tag) + 3 Accessories of your choice', 'images/none.jpg', 'images/none.jpg', 'images/none.jpg',
                             'images/none.jpg', 'images/none.jpg' ],

"Amis Huricane": [ '38', '10%', 'Amis', 'Full', '15', 'Ancients with Excellents', 'Upon Request or Same Level Weapons', 'Gold Pet (1)',
                             '3 Months (Forum VIP Tag) + 3 Accessories of your choice', 'images/none.jpg', 'images/none.jpg', 'images/none.jpg',
                             'images/none.jpg', 'images/none.jpg' ],
}

MGUber = {
    "Warrior Leather": [ '10', '0%', 'Warrior', 'Full', '15', 'Ancients with Excellents', 'Upon Request or Same Level Weapons', 'Gold Pet (1)',
                         '3 Months (Forum VIP Tag) + 3 Accessories of your choice',
                            'images/BK/T1/warrior/Warrior1.png',
                            'images/BK/T1/warrior/Warrior2.jpg',
                            'images/BK/T1/warrior/Warrior3.jpg',
                            'images/BK/T1/warrior/Warrior4.jpg',
                            'images/BK/T1/warrior/Warrior4.jpg' ]

    , "Anonymous Leather": [ '10', '0%', 'Anonymous', 'Full', '15', 'Ancients with Excellents', 'Upon Request or Same Level Weapons', 'Gold Pet (1)',
                             '3 Months (Forum VIP Tag) + 3 Accessories of your choice',
                            'images/BK/T2/anon/Anonymous1.png',
                            'images/BK/T2/anon/Anonymous2.jpg',
                            'images/BK/T2/anon/Anonymous3.jpg',
                            'images/BK/T2/anon/Anonymous4.jpg',
                            'images/BK/T2/anon/Anonymous4.png' ]

    , "Hyperion Bronze": [ '10', '0%', 'Hyperion', 'Full', '15', 'Ancients with Excellents', 'Upon Request or Same Level Weapons', 'Gold Pet (1)',
                           '3 Months (Forum VIP Tag) + 3 Accessories of your choice',
                           'images/BK/T1/hyperion/Hyperion1.png',
                            'images/BK/T1/hyperion/Hyperion2.jpg',
                            'images/BK/T1/hyperion/Hyperion3.jpg',
                            'images/BK/T1/hyperion/Hyperion4.jpg',
                            'images/BK/T1/hyperion/Hyperion4.jpg' ]
    ,
"Mist Bronze": [ '10', '0%', 'Mist', 'Full', '15', 'Ancients with Excellents', 'Upon Request or Same Level Weapons', 'Gold Pet (1)', '3 Months (Forum VIP Tag) + 3 Accessories of your choice',
                            'images/BK/T2/mist/Mist1.png',
                            'images/BK/T2/mist/Mist2.jpg',
                            'images/BK/T2/mist/Mist3.jpg',
                            'images/BK/T2/mist/Mist4.jpg',
                            'images/BK/T2/mist/Mist4.jpg', ]
    ,
"Garuda Brass": [ '13', '10%', 'Garuda', 'Full', '15', 'Ancients with Excellents', 'Upon Request or Same Level Weapons', 'Gold Pet (1)', '3 Months (Forum VIP Tag) + 3 Accessories of your choice',
                            'images/BK/T1/garuda/Garuda1.png',
                            'images/BK/T1/garuda/Garuda2.jpg',
                            'images/BK/T1/garuda/Garuda3.jpg',
                            'images/BK/T1/garuda/Garuda4.jpg',
                            'images/BK/T1/garuda/Garuda4.jpg' ]
    ,
"Cloud Brass": [ '13', '10%', 'Cloud', 'Full', '15', 'Ancients with Excellents', 'Upon Request or Same Level Weapons', 'Gold Pet (1)', '3 Months (Forum VIP Tag) + 3 Accessories of your choice',
                            'images/BK/T2/cloud/Cloud1.png',
                            'images/BK/T2/cloud/Cloud2.jpg',
                            'images/BK/T2/cloud/Cloud3.jpg',
                            'images/BK/T2/cloud/Cloud4.jpg',
                            'images/BK/T2/cloud/Cloud4.jpg' ]
    ,
"Eplete Scale": [ '20', '10%', 'Eplete', 'Full', '15', 'Ancients with Excellents', 'Upon Request or Same Level Weapons', 'Gold Pet (1)', '3 Months (Forum VIP Tag) + 3 Accessories of your choice',
                            'images/BK/T1/eplete/Eplete1.png',
                            'images/BK/T1/eplete/Eplete2.jpg',
                            'images/BK/T1/eplete/Eplete3.jpg',
                            'images/BK/T1/eplete/Eplete4.jpg',
                            'images/BK/T1/eplete/Eplete4.jpg' ]

    , "Berserker Scale": [ '27', '10%', 'Berserker', 'Full', '15', 'Ancients with Excellents', 'Upon Request or Same Level Weapons', 'Gold Pet (1)',
                           '3 Months (Forum VIP Tag) + 3 Accessories of your choice',
                            'images/BK/T2/berserk/Berserker1.png',
                            'images/BK/T2/berserk/Berserker2.jpg',
                            'images/BK/T2/berserk/Berserker3.jpg',
                            'images/BK/T2/berserk/Berserker4.jpg',
                            'images/BK/T2/berserk/Berserker4.jpg' ]

    , "Kantata Plate": [ '30', '10%', 'Kantata', 'Full', '15', 'Ancients with Excellents', 'Upon Request or Same Level Weapons', 'Gold Pet (1)',
                         '3 Months (Forum VIP Tag) + 3 Accessories of your choice',
                            'images/BK/T1/kantata/Kantata1.png',
                            'images/BK/T1/kantata/Kantata2.jpg',
                            'images/BK/T1/kantata/Kantata3.jpg',
                            'images/BK/T1/kantata/Kantata4.jpg',
                            'images/BK/T1/kantata/Kantata4.jpg' ]

,
"Rave Plate": [ '30', '10%', 'Rave', 'Full', '15', 'Ancients with Excellents', 'Upon Request or Same Level Weapons', 'Gold Pet (1)', '3 Months (Forum VIP Tag) + 3 Accessories of your choice',
                            'images/BK/T2/rave/Rave1.png',
                            'images/BK/T2/rave/Rave2.jpg',
                            'images/BK/T2/rave/Rave3.jpg',
                            'images/BK/T2/rave/Rave4.jpg',
                            'images/BK/T2/rave/Rave4.jpg' ]
    ,
"Hyon Dragon": [ '35', '10%', 'Hyon', 'Full', '15', 'Ancients with Excellents', 'Upon Request or Same Level Weapons', 'Gold Pet (1)', '3 Months (Forum VIP Tag) + 3 Accessories of your choice',
                            'images/BK/T1/hyon/Hyon1.png',
                            'images/BK/T1/hyon/Hyon2.jpg',
                            'images/BK/T1/hyon/Hyon3.jpg',
                            'images/BK/T1/hyon/Hyon4.jpg',
                            'images/BK/T1/hyon/Hyon4.jpg' ]

    , "Vicious Dragon": [ '35', '10%', 'Vicious', 'Full', '15', 'Ancients with Excellents', 'Upon Request or Same Level Weapons', 'Gold Pet (1)',
                          '3 Months (Forum VIP Tag) + 3 Accessories of your choice',

                            'images/BK/T2/vicious/Vicious1.png',
                            'images/BK/T2/vicious/Vicious2.jpg',
                            'images/BK/T2/vicious/Vicious3.jpg',
                            'images/BK/T2/vicious/Vicious4.jpg',
                            'images/BK/T2/vicious/Vicious4.jpg' ],

"Gaion Storm Crow": [ '40', '10%', 'Gaion', 'Full', '15', 'Ancients with Excellents', 'Upon Request or Same Level Weapons', 'Gold Pet (1)',
                             '3 Months (Forum VIP Tag) + 3 Accessories of your choice', 'images/none.jpg', 'images/none.jpg', 'images/none.jpg',
                             'images/none.jpg', 'images/none.jpg' ],
"Muren Storm Crow": [ '47', '10%', 'Muren', 'Full', '15', 'Ancients with Excellents', 'Upon Request or Same Level Weapons', 'Gold Pet (1)',
                             '3 Months (Forum VIP Tag) + 3 Accessories of your choice', 'images/none.jpg', 'images/none.jpg', 'images/none.jpg',
                             'images/none.jpg', 'images/none.jpg' ],

}
MGS4 = {}
MGWeap = {}
MGMisc = {}


# Dark Lord
DLTier = {"Sutr Glorious": [ '45', '10%', 'Meter', 'Full', '15', 'Ancients with Excellents', 'Upon Request or Same Level Weapons', 'Gold Pet (1)',
                             '3 Months (Forum VIP Tag) + 3 Accessories of your choice', 'images/none.jpg', 'images/none.jpg', 'images/none.jpg',
                             'images/none.jpg', 'images/none.jpg' ],

"Meter Dark Master": [ '47', '10%', 'Meter', 'Full', '15', 'Ancients with Excellents', 'Upon Request or Same Level Weapons', 'Gold Pet (1)',
                             '3 Months (Forum VIP Tag) + 3 Accessories of your choice', 'images/none.jpg', 'images/none.jpg', 'images/none.jpg',
                             'images/none.jpg', 'images/none.jpg' ],
"Choms Dark Steel": [ '47', '10%', 'Apis', 'Full', '15', 'Ancients with Excellents', 'Upon Request or Same Level Weapons', 'Gold Pet (1)',
                             '3 Months (Forum VIP Tag) + 3 Accessories of your choice', 'images/none.jpg', 'images/none.jpg', 'images/none.jpg',
                             'images/none.jpg', 'images/none.jpg' ],

}

DLUber = {
    "Warrior Leather": [ '10', '10%', 'Warrior', 'Full', '15', 'Ancients with Excellents', 'Upon Request or Same Level Weapons', 'Gold Pet (1)',
                         '3 Months (Forum VIP Tag) + 3 Accessories of your choice', 'images/none.jpg', 'images/none.jpg', 'images/none.jpg',
                         'images/none.jpg', 'images/none.jpg' ]
    , "Anonymous Leather": [ '10', '10%', 'Anonymous', 'Full', '15', 'Ancients with Excellents', 'Upon Request or Same Level Weapons', 'Gold Pet (1)',
                             '3 Months (Forum VIP Tag) + 3 Accessories of your choice', 'images/none.jpg', 'images/none.jpg', 'images/none.jpg',
                             'images/none.jpg', 'images/none.jpg' ]
    , "Hyperion Bronze": [ '10', '10%', 'Hyperion', 'Full', '15', 'Ancients with Excellents', 'Upon Request or Same Level Weapons', 'Gold Pet (1)',
                           '3 Months (Forum VIP Tag) + 3 Accessories of your choice', 'images/none.jpg', 'images/none.jpg', 'images/none.jpg',
                           'images/none.jpg', 'images/none.jpg' ]
    ,
"Mist Bronze": [ '10', '10%', 'Bragi', 'Mist', '15', 'Ancients with Excellents', 'Upon Request or Same Level Weapons', 'Gold Pet (1)', '3 Months (Forum VIP Tag) + 3 Accessories of your choice',
                 'images/none.jpg', 'images/none.jpg', 'images/none.jpg', 'images/none.jpg', 'images/none.jpg' ]
    ,
"Garuda Brass": [ '13', '10%', 'Garuda', 'Full', '15', 'Ancients with Excellents', 'Upon Request or Same Level Weapons', 'Gold Pet (1)', '3 Months (Forum VIP Tag) + 3 Accessories of your choice',
                  'images/none.jpg', 'images/none.jpg', 'images/none.jpg', 'images/none.jpg', 'images/none.jpg' ]
    ,
"Cloud Brass": [ '13', '10%', 'Cloud', 'Full', '15', 'Ancients with Excellents', 'Upon Request or Same Level Weapons', 'Gold Pet (1)', '3 Months (Forum VIP Tag) + 3 Accessories of your choice',
                 'images/none.jpg', 'images/none.jpg', 'images/none.jpg', 'images/none.jpg', 'images/none.jpg' ]
    ,
"Eplete Scale": [ '20', '10%', 'Eplete', 'Full', '15', 'Ancients with Excellents', 'Upon Request or Same Level Weapons', 'Gold Pet (1)', '3 Months (Forum VIP Tag) + 3 Accessories of your choice',
                  'images/none.jpg', 'images/none.jpg', 'images/none.jpg', 'images/none.jpg', 'images/none.jpg' ]
    , "Berserker Scale": [ '27', '10%', 'Berserker', 'Full', '15', 'Ancients with Excellents', 'Upon Request or Same Level Weapons', 'Gold Pet (1)',
                           '3 Months (Forum VIP Tag) + 3 Accessories of your choice', 'images/none.jpg', 'images/none.jpg', 'images/none.jpg',
                           'images/none.jpg', 'images/none.jpg' ]
    , "Agnis Adamantine": [ '37', '10%', 'Agnis', 'Full', '15', 'Ancients with Excellents', 'Upon Request or Same Level Weapons', 'Gold Pet (1)',
                            '3 Months (Forum VIP Tag) + 3 Accessories of your choice', 'images/none.jpg', 'images/none.jpg', 'images/none.jpg',
                            'images/none.jpg', 'images/none.jpg' ]
    , "Broy Adamantine": [ '39', '10%', 'Broy\'s', 'Full', '15', 'Ancients with Excellents', 'Upon Request or Same Level Weapons', 'Gold Pet (1)',
                           '3 Months (Forum VIP Tag) + 3 Accessories of your choice', 'images/none.jpg', 'images/none.jpg', 'images/none.jpg',
                           'images/none.jpg', 'images/none.jpg' ]
}
DLS4 = {}
DLWeap = {}
DLMisc = {}



# Summoner
SUTier = {"Hapi Ancient": [ '40', '10%', 'Hapi', 'Full', '15', 'Ancients with Excellents', 'Upon Request or Same Level Weapons', 'Gold Pet (1)',
                             '3 Months (Forum VIP Tag) + 3 Accessories of your choice', 'images/none.jpg', 'images/none.jpg', 'images/none.jpg',
                             'images/none.jpg', 'images/none.jpg' ],

"Olrum Demonic": [ '40', '10%', 'Olrum', 'Full', '15', 'Ancients with Excellents', 'Upon Request or Same Level Weapons', 'Gold Pet (1)',
                             '3 Months (Forum VIP Tag) + 3 Accessories of your choice', 'images/none.jpg', 'images/none.jpg', 'images/none.jpg',
                             'images/none.jpg', 'images/none.jpg' ],
"Ophions Mistery": [ '40', '10%', 'Ophions', 'Full', '15', 'Ancients with Excellents', 'Upon Request or Same Level Weapons', 'Gold Pet (1)',
                             '3 Months (Forum VIP Tag) + 3 Accessories of your choice', 'images/none.jpg', 'images/none.jpg', 'images/none.jpg',
                             'images/none.jpg', 'images/none.jpg' ],

"Nemesis Aura": [ '40', '10%', 'Nemesis', 'Full', '15', 'Ancients with Excellents', 'Upon Request or Same Level Weapons', 'Gold Pet (1)',
                             '3 Months (Forum VIP Tag) + 3 Accessories of your choice', 'images/none.jpg', 'images/none.jpg', 'images/none.jpg',
                             'images/none.jpg', 'images/none.jpg' ],

}

SUUber = {
    "Chrono Red Wing": [ '10', '10%', 'Chrono', 'Full', '15', 'Ancients with Excellents', 'Upon Request or Same Level Weapons', 'Gold Pet (1)',
                         '3 Months (Forum VIP Tag) + 3 Accessories of your choice', 'images/none.jpg', 'images/none.jpg', 'images/none.jpg',
                         'images/none.jpg', 'images/none.jpg' ]
    , "Semeden Red Wing": [ '10', '10%', 'Semeden', 'Full', '15', 'Ancients with Excellents', 'Upon Request or Same Level Weapons', 'Gold Pet (1)',
                            '3 Months (Forum VIP Tag) + 3 Accessories of your choice', 'images/none.jpg', 'images/none.jpg', 'images/none.jpg',
                            'images/none.jpg', 'images/none.jpg' ]}
SUS4 = {}
SUWeap = {}
SUMisc = {}

Datas = {
    "Choose Class": 0,

    "Blade Master": {"Tier": BKTier, "Uber": BKUber, "Season 4": BKS4, "Weapons": BKWeap, "Miscellaneous": BKMisc},

    "Muse Elf": {"Tier": METier, "Uber": MEUber, "Season 4": MES4, "Weapons": MEBow, "Miscellaneous": MEMisc},

    "Soul Master": {"Tier": SMTier, "Uber": SMUber, "Season 4": SMS4, "Weapons": SMStaffs, "Miscellaneous": SMMisc},

    "Rage Fighter": {"Tier": RFTier, "Uber": RFUber, "Season 4": RFS4, "Weapons": RFWeap, "Miscellaneous": RFMisc},

    "Magic Gladiator": {"Tier": MGTier, "Uber": MGUber, "Season 4": MGS4, "Weapons": MGWeap, "Miscellaneous": MGMisc},

    "Dark Lord": {"Tier": DLTier, "Uber": DLUber, "Season 4": DLS4, "Weapons": DLWeap, "Miscellaneous": DLMisc},

    "Summoner": {"Tier": SUTier, "Uber": SUUber, "Season 4": SUS4, "Weapons": SUWeap, "Miscellaneous": SUMisc},
}


def GetAllItems(Class, types):
    _Classes = Datas
    try:
        _Clean = _Classes[ Class ][ types ]
        return dict(_Clean).keys()

    except:
        return ["None"]


def GetItemImages(Class, types, name):
    _Classes = Datas
    _Clean = _Classes[ Class ][ types ][ name ]
    return _Clean[ 9: ]


def GetItemInfo(Class, types, name, which=0):
    _Classes = Datas
    try:
        if which:
            Get = _Classes[ Class ][ types ][ name ]
            return Get
        else:
            Get = _Classes[ Class ][ types ][ name ]
            return Get


    except:
        return "KeyError (CSECPENGINE)"

        # GetItemImages("Blade Master","Tier","Bragi Dark Phoenix")
