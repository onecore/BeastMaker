# -*- coding: utf-8 -*-
# Author: Mark Anthony R. Pequeras
# License: GPL / PSF
# Project: Database and Item Parser (BeastMU Online)
# Created: Mon May 04 22:16:04 2015
__CoreSEC__ = "CoreSEC Softwares, Ca"
__Version__ = 4.7 # Stable Option Parser
__Developers__ = [ "Mark Anthony R. Pequeras", "Alman Alibata", "Ken Sham", "Winszy Langueras" ]

from PyQt4 import QtCore, QtGui
import sys

win32 = sys.platform.startswith("win")
compiled = False
if win32:
    compiled = True

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8

    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)


class Ui_BeastMaker(object):
    def setupUi(self, BeastMaker):
        BeastMaker.setObjectName(_fromUtf8("BeastMaker"))
        BeastMaker.resize(801, 678)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(BeastMaker.sizePolicy().hasHeightForWidth())
        BeastMaker.setSizePolicy(sizePolicy)
        BeastMaker.setMinimumSize(QtCore.QSize(801, 678))
        BeastMaker.setMaximumSize(QtCore.QSize(801, 678))
        font = QtGui.QFont()
        font.setKerning(True)
        font.setStyleStrategy(QtGui.QFont.PreferAntialias)
        BeastMaker.setFont(font)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(_fromUtf8(":/coresec/im/bmu.ico")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        BeastMaker.setWindowIcon(icon)
        BeastMaker.setStyleSheet(_fromUtf8("QWidget {\n"
                                           "border: none;\n"
                                           "background-image: url(:/coresec/im/bg.png)\n"
                                           "}"))
        self.BGMenu = QtGui.QPushButton(BeastMaker)
        self.BGMenu.setGeometry(QtCore.QRect(-20, 200, 821, 61))
        self.BGMenu.setStyleSheet(_fromUtf8("QPushButton{\n"
                                            "border: none;\n"
                                            "background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
                                            "stop: 0 #a6a6a6, stop: 0.08 #7f7f7f,\n"
                                            "stop: 0.39999 #717171, stop: 0.4 #626262,\n"
                                            "stop: 0.9 #4c4c4c, stop: 1 #333333);\n"
                                            "}"))
        self.BGMenu.setText(_fromUtf8(""))
        self.BGMenu.setObjectName(_fromUtf8("BGMenu"))
        self.ItemMakerButton = QtGui.QPushButton(BeastMaker)
        self.ItemMakerButton.setGeometry(QtCore.QRect(20, 211, 121, 41))
        self.ItemMakerButton.setStyleSheet(_fromUtf8("QPushButton {\n"
                                                     "color: #333;\n"
                                                     "\n"
                                                     "border: 2px solid #555;\n"
                                                     "border-radius: 11px;\n"
                                                     "padding: 5px;\n"
                                                     "background: qradialgradient(cx: 0.3, cy: -0.4,\n"
                                                     "fx: 0.3, fy: -0.4,\n"
                                                     "radius: 1.35, stop: 0 #fff, stop: 1 #888);\n"
                                                     "min-width: 80px;\n"
                                                     "}\n"
                                                     "\n"
                                                     "\n"
                                                     "QPushButton:hover {\n"
                                                     "background: qradialgradient(cx: 0.3, cy: -0.4,\n"
                                                     "fx: 0.3, fy: -0.4,\n"
                                                     "radius: 1.35, stop: 0 #fff, stop: 1 #bbb);\n"
                                                     "}\n"
                                                     "\n"
                                                     "QPushButton:pressed {\n"
                                                     "background: qradialgradient(cx: 0.4, cy: -0.1,\n"
                                                     "fx: 0.4, fy: -0.1,\n"
                                                     "radius: 1.35, stop: 0 #fff, stop: 1 #ddd);\n"
                                                     "}"))
        self.ItemMakerButton.setObjectName(_fromUtf8("ItemMakerButton"))
        self.PackageMakerButton = QtGui.QPushButton(BeastMaker)
        self.PackageMakerButton.setGeometry(QtCore.QRect(146, 211, 121, 41))
        self.PackageMakerButton.setStyleSheet(_fromUtf8("QPushButton {\n"
                                                        "color: #333;\n"
                                                        "\n"
                                                        "border: 2px solid #555;\n"
                                                        "border-radius: 11px;\n"
                                                        "padding: 5px;\n"
                                                        "background: qradialgradient(cx: 0.3, cy: -0.4,\n"
                                                        "fx: 0.3, fy: -0.4,\n"
                                                        "radius: 1.35, stop: 0 #fff, stop: 1 #888);\n"
                                                        "min-width: 80px;\n"
                                                        "}\n"
                                                        "\n"
                                                        "\n"
                                                        "QPushButton:hover {\n"
                                                        "background: qradialgradient(cx: 0.3, cy: -0.4,\n"
                                                        "fx: 0.3, fy: -0.4,\n"
                                                        "radius: 1.35, stop: 0 #fff, stop: 1 #bbb);\n"
                                                        "}\n"
                                                        "\n"
                                                        "QPushButton:pressed {\n"
                                                        "background: qradialgradient(cx: 0.4, cy: -0.1,\n"
                                                        "fx: 0.4, fy: -0.1,\n"
                                                        "radius: 1.35, stop: 0 #fff, stop: 1 #ddd);\n"
                                                        "}qlineargradient(spread:pad, x1:0, y1:0, x2:1, y2:0, stop:0 rgba(0, 0, 0, 255), stop:1 rgba(255, 255, 255, 255))"))
        self.PackageMakerButton.setObjectName(_fromUtf8("PackageMakerButton"))
        self.AccountManagerButton = QtGui.QPushButton(BeastMaker)
        self.AccountManagerButton.setGeometry(QtCore.QRect(273, 212, 121, 41))
        self.AccountManagerButton.setStyleSheet(_fromUtf8("QPushButton {\n"
                                                          "color: #333;\n"
                                                          "\n"
                                                          "border: 2px solid #555;\n"
                                                          "border-radius: 11px;\n"
                                                          "padding: 5px;\n"
                                                          "background: qradialgradient(cx: 0.3, cy: -0.4,\n"
                                                          "fx: 0.3, fy: -0.4,\n"
                                                          "radius: 1.35, stop: 0 #fff, stop: 1 #888);\n"
                                                          "min-width: 80px;\n"
                                                          "}\n"
                                                          "\n"
                                                          "\n"
                                                          "QPushButton:hover {\n"
                                                          "background: qradialgradient(cx: 0.3, cy: -0.4,\n"
                                                          "fx: 0.3, fy: -0.4,\n"
                                                          "radius: 1.35, stop: 0 #fff, stop: 1 #bbb);\n"
                                                          "}\n"
                                                          "\n"
                                                          "QPushButton:pressed {\n"
                                                          "background: qradialgradient(cx: 0.4, cy: -0.1,\n"
                                                          "fx: 0.4, fy: -0.1,\n"
                                                          "radius: 1.35, stop: 0 #fff, stop: 1 #ddd);\n"
                                                          "}"))
        self.AccountManagerButton.setObjectName(_fromUtf8("AccountManagerButton"))
        self.ConnectionButton = QtGui.QPushButton(BeastMaker)
        self.ConnectionButton.setGeometry(QtCore.QRect(399, 212, 121, 41))
        self.ConnectionButton.setStyleSheet(_fromUtf8("QPushButton {\n"
                                                      "color: #333;\n"
                                                      "\n"
                                                      "border: 2px solid #555;\n"
                                                      "border-radius: 11px;\n"
                                                      "padding: 5px;\n"
                                                      "background: qradialgradient(cx: 0.3, cy: -0.4,\n"
                                                      "fx: 0.3, fy: -0.4,\n"
                                                      "radius: 1.35, stop: 0 #fff, stop: 1 #888);\n"
                                                      "min-width: 80px;\n"
                                                      "}\n"
                                                      "\n"
                                                      "\n"
                                                      "QPushButton:hover {\n"
                                                      "background: qradialgradient(cx: 0.3, cy: -0.4,\n"
                                                      "fx: 0.3, fy: -0.4,\n"
                                                      "radius: 1.35, stop: 0 #fff, stop: 1 #bbb);\n"
                                                      "}\n"
                                                      "\n"
                                                      "QPushButton:pressed {\n"
                                                      "background: qradialgradient(cx: 0.4, cy: -0.1,\n"
                                                      "fx: 0.4, fy: -0.1,\n"
                                                      "radius: 1.35, stop: 0 #fff, stop: 1 #ddd);\n"
                                                      "}"))
        self.ConnectionButton.setObjectName(_fromUtf8("ConnectionButton"))
        self.headerim = QtGui.QLabel(BeastMaker)
        self.headerim.setGeometry(QtCore.QRect(0, 20, 730, 180))
        self.headerim.setStyleSheet(_fromUtf8("background: transparent;"))
        self.headerim.setText(_fromUtf8(""))
        self.headerim.setPixmap(QtGui.QPixmap(_fromUtf8(":/coresec/im/header.png")))
        self.headerim.setScaledContents(True)
        self.headerim.setObjectName(_fromUtf8("headerim"))
        self.ResourcesButton = QtGui.QPushButton(BeastMaker)
        self.ResourcesButton.setGeometry(QtCore.QRect(526, 212, 122, 40))
        self.ResourcesButton.setStyleSheet(_fromUtf8("QPushButton {\n"
                                                     "color: #333;\n"
                                                     "\n"
                                                     "border: 2px solid #555;\n"
                                                     "border-radius: 11px;\n"
                                                     "padding: 5px;\n"
                                                     "background: qradialgradient(cx: 0.3, cy: -0.4,\n"
                                                     "fx: 0.3, fy: -0.4,\n"
                                                     "radius: 1.35, stop: 0 #fff, stop: 1 #888);\n"
                                                     "min-width: 80px;\n"
                                                     "}\n"
                                                     "\n"
                                                     "\n"
                                                     "QPushButton:hover {\n"
                                                     "background: qradialgradient(cx: 0.3, cy: -0.4,\n"
                                                     "fx: 0.3, fy: -0.4,\n"
                                                     "radius: 1.35, stop: 0 #fff, stop: 1 #bbb);\n"
                                                     "}\n"
                                                     "\n"
                                                     "QPushButton:pressed {\n"
                                                     "background: qradialgradient(cx: 0.4, cy: -0.1,\n"
                                                     "fx: 0.4, fy: -0.1,\n"
                                                     "radius: 1.35, stop: 0 #fff, stop: 1 #ddd);\n"
                                                     "}"))
        self.ResourcesButton.setObjectName(_fromUtf8("ResourcesButton"))
        self.pushButton = QtGui.QPushButton(BeastMaker)
        self.pushButton.setGeometry(QtCore.QRect(654, 212, 127, 40))
        self.pushButton.setStyleSheet(_fromUtf8("QPushButton {\n"
                                                "color: #333;\n"
                                                "\n"
                                                "border: 2px solid #555;\n"
                                                "border-radius: 11px;\n"
                                                "padding: 5px;\n"
                                                "background: qradialgradient(cx: 0.3, cy: -0.4,\n"
                                                "fx: 0.3, fy: -0.4,\n"
                                                "radius: 1.35, stop: 0 #fff, stop: 1 #888);\n"
                                                "min-width: 80px;\n"
                                                "}\n"
                                                "\n"
                                                "\n"
                                                "QPushButton:hover {\n"
                                                "background: qradialgradient(cx: 0.3, cy: -0.4,\n"
                                                "fx: 0.3, fy: -0.4,\n"
                                                "radius: 1.35, stop: 0 #fff, stop: 1 #bbb);\n"
                                                "}\n"
                                                "\n"
                                                "QPushButton:pressed {\n"
                                                "background: qradialgradient(cx: 0.4, cy: -0.1,\n"
                                                "fx: 0.4, fy: -0.1,\n"
                                                "radius: 1.35, stop: 0 #fff, stop: 1 #ddd);\n"
                                                "}"))
        self.pushButton.setObjectName(_fromUtf8("pushButton"))
        self.bmuPager = QtGui.QStackedWidget(BeastMaker)
        self.bmuPager.setGeometry(QtCore.QRect(4, 265, 799, 419))
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.bmuPager.sizePolicy().hasHeightForWidth())
        self.bmuPager.setSizePolicy(sizePolicy)
        font = QtGui.QFont()
        font.setBold(False)
        font.setWeight(50)
        font.setStyleStrategy(QtGui.QFont.PreferAntialias)
        self.bmuPager.setFont(font)
        self.bmuPager.setMouseTracking(True)
        self.bmuPager.setAcceptDrops(False)
        self.bmuPager.setFrameShape(QtGui.QFrame.NoFrame)
        self.bmuPager.setFrameShadow(QtGui.QFrame.Plain)
        self.bmuPager.setLineWidth(1)
        self.bmuPager.setMidLineWidth(0)
        self.bmuPager.setObjectName(_fromUtf8("bmuPager"))
        self.itemMakerPage = QtGui.QWidget()
        self.itemMakerPage.setObjectName(_fromUtf8("itemMakerPage"))
        self.imtitle = QtGui.QLabel(self.itemMakerPage)
        self.imtitle.setGeometry(QtCore.QRect(708, -3, 66, 16))
        self.imtitle.setStyleSheet(_fromUtf8("color: rgb(255, 73, 12)"))
        self.imtitle.setObjectName(_fromUtf8("imtitle"))
        self.vaultBG = QtGui.QPushButton(self.itemMakerPage)
        self.vaultBG.setGeometry(QtCore.QRect(3, -1, 282, 407))
        self.vaultBG.setStyleSheet(_fromUtf8("QPushButton {\n"
                                             "color: #333;\n"
                                             "\n"
                                             "border: 2px solid #555;\n"
                                             "border-radius: 11px;\n"
                                             "padding: 5px;\n"
                                             "background: qradialgradient(cx: 0.3, cy: -0.4,\n"
                                             "fx: 0.3, fy: -0.4,\n"
                                             "radius: 1.35, stop: 0 #fff, stop: 1 #888);\n"
                                             "min-width: 80px;\n"
                                             "}"))
        self.vaultBG.setText(_fromUtf8(""))
        self.vaultBG.setObjectName(_fromUtf8("vaultBG"))
        self.vaultCat = QtGui.QToolBox(self.itemMakerPage)
        self.vaultCat.setGeometry(QtCore.QRect(8, 28, 273, 369))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Miriam"))
        font.setPointSize(8)
        font.setBold(True)
        font.setWeight(75)
        self.vaultCat.setFont(font)
        self.vaultCat.setContextMenuPolicy(QtCore.Qt.NoContextMenu)
        self.vaultCat.setStyleSheet(_fromUtf8(""))
        self.vaultCat.setFrameShape(QtGui.QFrame.NoFrame)
        self.vaultCat.setObjectName(_fromUtf8("vaultCat"))
        self.wareMode = QtGui.QWidget()
        self.wareMode.setGeometry(QtCore.QRect(0, 0, 273, 312))
        self.wareMode.setCursor(QtGui.QCursor(QtCore.Qt.OpenHandCursor))
        self.wareMode.setFocusPolicy(QtCore.Qt.NoFocus)
        self.wareMode.setObjectName(_fromUtf8("wareMode"))
        self.viewFloat = QtGui.QDockWidget(self.wareMode)
        self.viewFloat.setWindowTitle("  BeastMaker: Optionizer")
        self.viewFloat.setStyleSheet("color: grey")
        self.viewFloat.setGeometry(QtCore.QRect(-1, -3, 274, 317))
        font = QtGui.QFont()
        font.setStyleStrategy(QtGui.QFont.PreferAntialias)
        self.viewFloat.setFont(font)
        self.viewFloat.setCursor(QtGui.QCursor(QtCore.Qt.OpenHandCursor))
        self.viewFloat.setFloating(False)
        self.viewFloat.setObjectName(_fromUtf8("viewFloat"))
        self.dockWidgetContents = QtGui.QWidget()
        self.dockWidgetContents.setObjectName(_fromUtf8("dockWidgetContents"))
        self.Details_Name = QtGui.QLabel(self.dockWidgetContents)
        self.Details_Name.setGeometry(QtCore.QRect(-1, 0, 277, 22))
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        self.Details_Name.setFont(font)
        self.Details_Name.setStyleSheet(_fromUtf8("background:rgb(48, 48, 48);\n"
                                                  "color: rgb(33, 166, 42);"))
        self.Details_Name.setObjectName(_fromUtf8("Details_Name"))
        self.AppendOnOff = QtGui.QPushButton(self.dockWidgetContents)
        self.AppendOnOff.setGeometry(QtCore.QRect(4, 38, 92, 40))
        font = QtGui.QFont()
        font.setPointSize(8)
        font.setBold(False)
        font.setWeight(50)
        font.setStyleStrategy(QtGui.QFont.PreferAntialias)
        self.AppendOnOff.setFont(font)
        self.AppendOnOff.setStyleSheet(_fromUtf8("QPushButton {\n"
                                                 "color: #333;\n"
                                                 "\n"
                                                 "border: 1px solid #555;\n"
                                                 "border-radius: 11px;\n"
                                                 "padding: 5px;\n"
                                                 "background: qradialgradient(cx: 0.3, cy: -0.4,\n"
                                                 "fx: 0.3, fy: -0.4,\n"
                                                 "radius: 1.35, stop: 0 #fff, stop: 1 #888);\n"
                                                 "min-width: 80px;\n"
                                                 "}\n"
                                                 "\n"
                                                 "\n"
                                                 "QPushButton:hover {\n"
                                                 "background: qradialgradient(cx: 0.3, cy: -0.4,\n"
                                                 "fx: 0.3, fy: -0.4,\n"
                                                 "radius: 1.35, stop: 0 #fff, stop: 1 #bbb);\n"
                                                 "}\n"
                                                 "\n"
                                                 "QPushButton:pressed {\n"
                                                 "background: qradialgradient(cx: 0.4, cy: -0.1,\n"
                                                 "fx: 0.4, fy: -0.1,\n"
                                                 "radius: 1.35, stop: 0 #fff, stop: 1 #ddd);\n"
                                                 "}"))
        self.AppendOnOff.setObjectName(_fromUtf8("AppendOnOff"))
        self.SafetyOnOff = QtGui.QPushButton(self.dockWidgetContents)
        self.SafetyOnOff.setGeometry(QtCore.QRect(4, 81, 92, 40))
        font = QtGui.QFont()
        font.setPointSize(8)
        font.setBold(False)
        font.setWeight(50)
        font.setStyleStrategy(QtGui.QFont.PreferAntialias)
        self.SafetyOnOff.setFont(font)
        self.SafetyOnOff.setStyleSheet(_fromUtf8("QPushButton {\n"
                                                 "color: #333;\n"
                                                 "\n"
                                                 "border: 1px solid #555;\n"
                                                 "border-radius: 11px;\n"
                                                 "padding: 5px;\n"
                                                 "background: qradialgradient(cx: 0.3, cy: -0.4,\n"
                                                 "fx: 0.3, fy: -0.4,\n"
                                                 "radius: 1.35, stop: 0 #fff, stop: 1 #888);\n"
                                                 "min-width: 80px;\n"
                                                 "}\n"
                                                 "\n"
                                                 "\n"
                                                 "QPushButton:hover {\n"
                                                 "background: qradialgradient(cx: 0.3, cy: -0.4,\n"
                                                 "fx: 0.3, fy: -0.4,\n"
                                                 "radius: 1.35, stop: 0 #fff, stop: 1 #bbb);\n"
                                                 "}\n"
                                                 "\n"
                                                 "QPushButton:pressed {\n"
                                                 "background: qradialgradient(cx: 0.4, cy: -0.1,\n"
                                                 "fx: 0.4, fy: -0.1,\n"
                                                 "radius: 1.35, stop: 0 #fff, stop: 1 #ddd);\n"
                                                 "}"))
        self.SafetyOnOff.setObjectName(_fromUtf8("SafetyOnOff"))
        self.ItemView_2 = QtGui.QPushButton(self.dockWidgetContents)
        self.ItemView_2.setGeometry(QtCore.QRect(101, 25, 166, 115))
        self.ItemView_2.setStyleSheet(_fromUtf8("background: rgb(94, 94, 94);\n"
                                                "border: 2px solid #555;\n"
                                                "border-radius: 9px;\n"
                                                "opacity: 39;"))
        icon1 = QtGui.QIcon()
        icon1.addPixmap(QtGui.QPixmap(_fromUtf8(":/Items/Data/Items/views/14/73.gif")), QtGui.QIcon.Normal,
                        QtGui.QIcon.Off)
        self.ItemView_2.setIcon(icon1)
        self.ItemView_2.setIconSize(QtCore.QSize(100, 100))
        self.ItemView_2.setObjectName(_fromUtf8("ItemView_2"))
        self.Details_Options = QtGui.QTextEdit(self.dockWidgetContents)
        self.Details_Options.setGeometry(QtCore.QRect(16, 225, 246, 45))
        font = QtGui.QFont()
        font.setPointSize(7)
        self.Details_Options.setFont(font)
        self.Details_Options.setStyleSheet(_fromUtf8("background: transparent;"))
        self.Details_Options.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.Details_Options.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.Details_Options.setTextInteractionFlags(
            QtCore.Qt.TextSelectableByKeyboard | QtCore.Qt.TextSelectableByMouse)
        self.Details_Options.setObjectName(_fromUtf8("Details_Options"))
        self.Details_OptionsBG = QtGui.QPushButton(self.dockWidgetContents)
        self.Details_OptionsBG.setGeometry(QtCore.QRect(0, 143, 276, 150))
        self.Details_OptionsBG.setStyleSheet(_fromUtf8("background: rgb(48, 48, 48);\n"
                                                       "border: 1px solid #555;\n"
                                                       "border-radius: 2px;\n"
                                                       "opacity: 39;"))
        self.Details_OptionsBG.setIconSize(QtCore.QSize(100, 100))
        self.Details_OptionsBG.setObjectName(_fromUtf8("Details_OptionsBG"))
        self.SocketOptions_Val2 = QtGui.QComboBox(self.dockWidgetContents)
        self.SocketOptions_Val2.setGeometry(QtCore.QRect(13, 189, 251, 17))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Narkisim"))
        font.setPointSize(10)
        font.setBold(False)
        font.setWeight(50)
        font.setStyleStrategy(QtGui.QFont.PreferAntialias)
        self.SocketOptions_Val2.setFont(font)
        self.SocketOptions_Val2.setStyleSheet(_fromUtf8(" QComboBox {\n"
                                                        "     border: 1px solid gray;\n"
                                                        "     border-radius: 3px;\n"
                                                        "     padding: 1px 18px 1px 3px;\n"
                                                        "     min-width: 6em;\n"
                                                        "color:rgb(79, 79, 79);\n"
                                                        " }\n"
                                                        "\n"
                                                        " QComboBox:editable {\n"
                                                        "     background: white;\n"
                                                        " }\n"
                                                        "\n"
                                                        " QComboBox:!editable, QComboBox::drop-down:editable {\n"
                                                        "      background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
                                                        "                                  stop: 0 #E1E1E1, stop: 0.4 #DDDDDD,\n"
                                                        "                                  stop: 0.5 #D8D8D8, stop: 1.0 #D3D3D3);\n"
                                                        " }\n"
                                                        "\n"
                                                        " /* QComboBox gets the \"on\" state when the popup is open */\n"
                                                        " QComboBox:!editable:on, QComboBox::drop-down:editable:on {\n"
                                                        "     background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
                                                        "                                 stop: 0 #D3D3D3, stop: 0.4 #D8D8D8,\n"
                                                        "                                 stop: 0.5 #DDDDDD, stop: 1.0 #E1E1E1);\n"
                                                        " }\n"
                                                        "\n"
                                                        " QComboBox:on { /* shift the text when the popup opens */\n"
                                                        "     padding-top: 3px;\n"
                                                        "     padding-left: 4px;\n"
                                                        " }\n"
                                                        "\n"
                                                        " QComboBox::drop-down {\n"
                                                        "     subcontrol-origin: padding;\n"
                                                        "     subcontrol-position: top right;\n"
                                                        "     width: 15px;\n"
                                                        "     background: #555;\n"
                                                        "     border-left-width: 1px;\n"
                                                        "     border-left-color: darkgray;\n"
                                                        "     border-left-style: solid; /* just a single line */\n"
                                                        "     border-top-right-radius: 3px; /* same radius as the QComboBox */\n"
                                                        "     border-bottom-right-radius: 3px;\n"
                                                        " }\n"
                                                        "QComboBox QAbstractItemView {\n"
                                                        "    border: 2px solid darkgray;\n"
                                                        "    color: black;\n"
                                                        "background: yellow;\n"
                                                        "selection-color: black;\n"
                                                        "    selection-background-color: grey;\n"
                                                        "}\n"
                                                        "\n"
                                                        ""))
        self.SocketOptions_Val2.setObjectName(_fromUtf8("SocketOptions_Val2"))
        self.SocketOptions_Val2.addItem(_fromUtf8(""))
        self.SocketOptions = QtGui.QComboBox(self.dockWidgetContents)
        self.SocketOptions.setGeometry(QtCore.QRect(14, 147, 121, 17))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Narkisim"))
        font.setPointSize(10)
        font.setBold(False)
        font.setWeight(50)
        font.setStyleStrategy(QtGui.QFont.PreferAntialias)
        self.SocketOptions.setFont(font)
        self.SocketOptions.setStyleSheet(_fromUtf8(" QComboBox {\n"
                                                   "     border: 1px solid gray;\n"
                                                   "     border-radius: 3px;\n"
                                                   "     padding: 1px 18px 1px 3px;\n"
                                                   "     min-width: 6em;\n"
                                                   "color:rgb(79, 79, 79);\n"
                                                   " }\n"
                                                   "\n"
                                                   " QComboBox:editable {\n"
                                                   "     background: white;\n"
                                                   " }\n"
                                                   "\n"
                                                   " QComboBox:!editable, QComboBox::drop-down:editable {\n"
                                                   "      background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
                                                   "                                  stop: 0 #E1E1E1, stop: 0.4 #DDDDDD,\n"
                                                   "                                  stop: 0.5 #D8D8D8, stop: 1.0 #D3D3D3);\n"
                                                   " }\n"
                                                   "\n"
                                                   " /* QComboBox gets the \"on\" state when the popup is open */\n"
                                                   " QComboBox:!editable:on, QComboBox::drop-down:editable:on {\n"
                                                   "     background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
                                                   "                                 stop: 0 #D3D3D3, stop: 0.4 #D8D8D8,\n"
                                                   "                                 stop: 0.5 #DDDDDD, stop: 1.0 #E1E1E1);\n"
                                                   " }\n"
                                                   "\n"
                                                   " QComboBox:on { /* shift the text when the popup opens */\n"
                                                   "     padding-top: 3px;\n"
                                                   "     padding-left: 4px;\n"
                                                   " }\n"
                                                   "\n"
                                                   " QComboBox::drop-down {\n"
                                                   "     subcontrol-origin: padding;\n"
                                                   "     subcontrol-position: top right;\n"
                                                   "     width: 15px;\n"
                                                   "     background: #555;\n"
                                                   "     border-left-width: 1px;\n"
                                                   "     border-left-color: darkgray;\n"
                                                   "     border-left-style: solid; /* just a single line */\n"
                                                   "     border-top-right-radius: 3px; /* same radius as the QComboBox */\n"
                                                   "     border-bottom-right-radius: 3px;\n"
                                                   " }\n"
                                                   "QComboBox QAbstractItemView {\n"
                                                   "    border: 2px solid darkgray;\n"
                                                   "    color: black;\n"
                                                   "background: yellow;\n"
                                                   "selection-color: black;\n"
                                                   "    selection-background-color: grey;\n"
                                                   "}\n"
                                                   "\n"
                                                   ""))
        self.SocketOptions.setObjectName(_fromUtf8("SocketOptions"))
        self.SocketOptions.addItem(_fromUtf8(""))
        self.SocketOptions.addItem(_fromUtf8(""))
        self.AncientOptions = QtGui.QComboBox(self.dockWidgetContents)
        self.AncientOptions.setGeometry(QtCore.QRect(149, 147, 115, 17))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Narkisim"))
        font.setPointSize(10)
        font.setBold(False)
        font.setWeight(50)
        font.setStyleStrategy(QtGui.QFont.PreferAntialias)
        self.AncientOptions.setFont(font)
        self.AncientOptions.setStyleSheet(_fromUtf8(" QComboBox {\n"
                                                    "     border: 1px solid gray;\n"
                                                    "     border-radius: 3px;\n"
                                                    "     padding: 1px 18px 1px 3px;\n"
                                                    "     min-width: 6em;\n"
                                                    "color:rgb(79, 79, 79);\n"
                                                    " }\n"
                                                    "\n"
                                                    " QComboBox:editable {\n"
                                                    "     background: white;\n"
                                                    " }\n"
                                                    "\n"
                                                    " QComboBox:!editable, QComboBox::drop-down:editable {\n"
                                                    "      background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
                                                    "                                  stop: 0 #E1E1E1, stop: 0.4 #DDDDDD,\n"
                                                    "                                  stop: 0.5 #D8D8D8, stop: 1.0 #D3D3D3);\n"
                                                    " }\n"
                                                    "\n"
                                                    " /* QComboBox gets the \"on\" state when the popup is open */\n"
                                                    " QComboBox:!editable:on, QComboBox::drop-down:editable:on {\n"
                                                    "     background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
                                                    "                                 stop: 0 #D3D3D3, stop: 0.4 #D8D8D8,\n"
                                                    "                                 stop: 0.5 #DDDDDD, stop: 1.0 #E1E1E1);\n"
                                                    " }\n"
                                                    "\n"
                                                    " QComboBox:on { /* shift the text when the popup opens */\n"
                                                    "     padding-top: 3px;\n"
                                                    "     padding-left: 4px;\n"
                                                    " }\n"
                                                    "\n"
                                                    " QComboBox::drop-down {\n"
                                                    "     subcontrol-origin: padding;\n"
                                                    "     subcontrol-position: top right;\n"
                                                    "     width: 15px;\n"
                                                    "     background: #555;\n"
                                                    "     border-left-width: 1px;\n"
                                                    "     border-left-color: darkgray;\n"
                                                    "     border-left-style: solid; /* just a single line */\n"
                                                    "     border-top-right-radius: 3px; /* same radius as the QComboBox */\n"
                                                    "     border-bottom-right-radius: 3px;\n"
                                                    " }\n"
                                                    "QComboBox QAbstractItemView {\n"
                                                    "    border: 2px solid darkgray;\n"
                                                    "    color: black;\n"
                                                    "background: yellow;\n"
                                                    "selection-color: black;\n"
                                                    "    selection-background-color: grey;\n"
                                                    "}\n"
                                                    "\n"
                                                    ""))
        self.AncientOptions.setObjectName(_fromUtf8("AncientOptions"))
        self.AncientOptions.addItem(_fromUtf8(""))
        self.AncientOptions.addItem(_fromUtf8(""))
        self.SocketOptions_Val3 = QtGui.QComboBox(self.dockWidgetContents)
        self.SocketOptions_Val3.setGeometry(QtCore.QRect(13, 208, 251, 17))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Narkisim"))
        font.setPointSize(10)
        font.setBold(False)
        font.setWeight(50)
        font.setStyleStrategy(QtGui.QFont.PreferAntialias)
        self.SocketOptions_Val3.setFont(font)
        self.SocketOptions_Val3.setStyleSheet(_fromUtf8(" QComboBox {\n"
                                                        "     border: 1px solid gray;\n"
                                                        "     border-radius: 3px;\n"
                                                        "     padding: 1px 18px 1px 3px;\n"
                                                        "     min-width: 6em;\n"
                                                        "color:rgb(79, 79, 79);\n"
                                                        " }\n"
                                                        "\n"
                                                        " QComboBox:editable {\n"
                                                        "     background: white;\n"
                                                        " }\n"
                                                        "\n"
                                                        " QComboBox:!editable, QComboBox::drop-down:editable {\n"
                                                        "      background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
                                                        "                                  stop: 0 #E1E1E1, stop: 0.4 #DDDDDD,\n"
                                                        "                                  stop: 0.5 #D8D8D8, stop: 1.0 #D3D3D3);\n"
                                                        " }\n"
                                                        "\n"
                                                        " /* QComboBox gets the \"on\" state when the popup is open */\n"
                                                        " QComboBox:!editable:on, QComboBox::drop-down:editable:on {\n"
                                                        "     background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
                                                        "                                 stop: 0 #D3D3D3, stop: 0.4 #D8D8D8,\n"
                                                        "                                 stop: 0.5 #DDDDDD, stop: 1.0 #E1E1E1);\n"
                                                        " }\n"
                                                        "\n"
                                                        " QComboBox:on { /* shift the text when the popup opens */\n"
                                                        "     padding-top: 3px;\n"
                                                        "     padding-left: 4px;\n"
                                                        " }\n"
                                                        "\n"
                                                        " QComboBox::drop-down {\n"
                                                        "     subcontrol-origin: padding;\n"
                                                        "     subcontrol-position: top right;\n"
                                                        "     width: 15px;\n"
                                                        "     background: #555;\n"
                                                        "     border-left-width: 1px;\n"
                                                        "     border-left-color: darkgray;\n"
                                                        "     border-left-style: solid; /* just a single line */\n"
                                                        "     border-top-right-radius: 3px; /* same radius as the QComboBox */\n"
                                                        "     border-bottom-right-radius: 3px;\n"
                                                        " }\n"
                                                        "QComboBox QAbstractItemView {\n"
                                                        "    border: 2px solid darkgray;\n"
                                                        "    color: black;\n"
                                                        "background: yellow;\n"
                                                        "selection-color: black;\n"
                                                        "    selection-background-color: grey;\n"
                                                        "}\n"
                                                        "\n"
                                                        ""))
        self.SocketOptions_Val3.setObjectName(_fromUtf8("SocketOptions_Val3"))
        self.SocketOptions_Val3.addItem(_fromUtf8(""))
        self.SocketOptions_Val1 = QtGui.QComboBox(self.dockWidgetContents)
        self.SocketOptions_Val1.setGeometry(QtCore.QRect(13, 170, 251, 17))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Narkisim"))
        font.setPointSize(10)
        font.setBold(False)
        font.setWeight(50)
        font.setStyleStrategy(QtGui.QFont.PreferAntialias)
        self.SocketOptions_Val1.setFont(font)
        self.SocketOptions_Val1.setStyleSheet(_fromUtf8(" QComboBox {\n"
                                                        "     border: 1px solid gray;\n"
                                                        "     border-radius: 3px;\n"
                                                        "     padding: 1px 18px 1px 3px;\n"
                                                        "     min-width: 6em;\n"
                                                        "color:rgb(79, 79, 79);\n"
                                                        " }\n"
                                                        "\n"
                                                        " QComboBox:editable {\n"
                                                        "     background: white;\n"
                                                        " }\n"
                                                        "\n"
                                                        " QComboBox:!editable, QComboBox::drop-down:editable {\n"
                                                        "      background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
                                                        "                                  stop: 0 #E1E1E1, stop: 0.4 #DDDDDD,\n"
                                                        "                                  stop: 0.5 #D8D8D8, stop: 1.0 #D3D3D3);\n"
                                                        " }\n"
                                                        "\n"
                                                        " /* QComboBox gets the \"on\" state when the popup is open */\n"
                                                        " QComboBox:!editable:on, QComboBox::drop-down:editable:on {\n"
                                                        "     background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
                                                        "                                 stop: 0 #D3D3D3, stop: 0.4 #D8D8D8,\n"
                                                        "                                 stop: 0.5 #DDDDDD, stop: 1.0 #E1E1E1);\n"
                                                        " }\n"
                                                        "\n"
                                                        " QComboBox:on { /* shift the text when the popup opens */\n"
                                                        "     padding-top: 3px;\n"
                                                        "     padding-left: 4px;\n"
                                                        " }\n"
                                                        "\n"
                                                        " QComboBox::drop-down {\n"
                                                        "     subcontrol-origin: padding;\n"
                                                        "     subcontrol-position: top right;\n"
                                                        "     width: 15px;\n"
                                                        "     background: #555;\n"
                                                        "     border-left-width: 1px;\n"
                                                        "     border-left-color: darkgray;\n"
                                                        "     border-left-style: solid; /* just a single line */\n"
                                                        "     border-top-right-radius: 3px; /* same radius as the QComboBox */\n"
                                                        "     border-bottom-right-radius: 3px;\n"
                                                        " }\n"
                                                        "QComboBox QAbstractItemView {\n"
                                                        "    border: 2px solid darkgray;\n"
                                                        "    color: black;\n"
                                                        "background: yellow;\n"
                                                        "selection-color: black;\n"
                                                        "    selection-background-color: grey;\n"
                                                        "}\n"
                                                        "\n"
                                                        ""))
        self.SocketOptions_Val1.setObjectName(_fromUtf8("SocketOptions_Val1"))
        self.SocketOptions_Val1.addItem(_fromUtf8(""))
        self.Serialized = QtGui.QLabel(self.dockWidgetContents)
        self.Serialized.setGeometry(QtCore.QRect(2, 272, 271, 21))
        font = QtGui.QFont()
        font.setBold(False)
        font.setWeight(50)
        self.Serialized.setFont(font)
        self.Serialized.setStyleSheet(_fromUtf8("color:rgb(44, 44, 44);\n"
                                                "background: rgb(150, 150, 150);\n"
                                                "border: 2px solid #555;\n"
                                                "opacity: 39;"))
        self.Serialized.setObjectName(_fromUtf8("Serialized"))
        self.Details_Name.raise_()
        self.AppendOnOff.raise_()
        self.SafetyOnOff.raise_()
        self.ItemView_2.raise_()
        self.Details_OptionsBG.raise_()
        self.SocketOptions_Val2.raise_()
        self.SocketOptions.raise_()
        self.AncientOptions.raise_()
        self.SocketOptions_Val3.raise_()
        self.SocketOptions_Val1.raise_()
        self.Details_Options.raise_()
        self.Serialized.raise_()
        self.viewFloat.setWidget(self.dockWidgetContents)
        self.AppendOnOff_2 = QtGui.QPushButton(self.wareMode)
        self.AppendOnOff_2.setGeometry(QtCore.QRect(94, 127, 92, 40))
        font = QtGui.QFont()
        font.setPointSize(8)
        font.setBold(False)
        font.setWeight(50)
        font.setStyleStrategy(QtGui.QFont.PreferAntialias)
        self.AppendOnOff_2.setFont(font)
        self.AppendOnOff_2.setStyleSheet(_fromUtf8("QPushButton {\n"
                                                   "color: #333;\n"
                                                   "\n"
                                                   "border: 1px solid #555;\n"
                                                   "border-radius: 11px;\n"
                                                   "padding: 5px;\n"
                                                   "background: qradialgradient(cx: 0.3, cy: -0.4,\n"
                                                   "fx: 0.3, fy: -0.4,\n"
                                                   "radius: 1.35, stop: 0 #fff, stop: 1 #888);\n"
                                                   "min-width: 80px;\n"
                                                   "}\n"
                                                   "\n"
                                                   "\n"
                                                   "QPushButton:hover {\n"
                                                   "background: qradialgradient(cx: 0.3, cy: -0.4,\n"
                                                   "fx: 0.3, fy: -0.4,\n"
                                                   "radius: 1.35, stop: 0 #fff, stop: 1 #bbb);\n"
                                                   "}\n"
                                                   "\n"
                                                   "QPushButton:pressed {\n"
                                                   "background: qradialgradient(cx: 0.4, cy: -0.1,\n"
                                                   "fx: 0.4, fy: -0.1,\n"
                                                   "radius: 1.35, stop: 0 #fff, stop: 1 #ddd);\n"
                                                   "}"))
        self.AppendOnOff_2.setObjectName(_fromUtf8("AppendOnOff_2"))
        self.AppendOnOff_2.raise_()
        self.viewFloat.raise_()
        self.vaultCat.addItem(self.wareMode, _fromUtf8(""))
        self.inventMode = QtGui.QWidget()
        self.inventMode.setGeometry(QtCore.QRect(0, 0, 273, 312))
        self.inventMode.setObjectName(_fromUtf8("inventMode"))
        self.InventBGLabel = QtGui.QLabel(self.inventMode)
        self.InventBGLabel.setGeometry(QtCore.QRect(2, 1, 269, 309))
        self.InventBGLabel.setStyleSheet(_fromUtf8("background: #fff"))
        self.InventBGLabel.setText(_fromUtf8(""))
        self.InventBGLabel.setObjectName(_fromUtf8("InventBGLabel"))
        self.slot_1_1 = QtGui.QPushButton(self.inventMode)
        self.slot_1_1.setGeometry(QtCore.QRect(5, 5, 61, 62))
        self.slot_1_1.setStyleSheet(_fromUtf8("background: rgb(58, 6, 2);\n"
                                              "color: rgb(222, 222, 222)"))
        self.slot_1_1.setObjectName(_fromUtf8("slot_1_1"))
        self.slot_1_2 = QtGui.QPushButton(self.inventMode)
        self.slot_1_2.setGeometry(QtCore.QRect(70, 5, 61, 62))
        self.slot_1_2.setStyleSheet(_fromUtf8("background: rgb(58, 6, 2);\n"
                                              "color: rgb(222, 222, 222)"))
        self.slot_1_2.setObjectName(_fromUtf8("slot_1_2"))
        self.slot_1_3 = QtGui.QPushButton(self.inventMode)
        self.slot_1_3.setGeometry(QtCore.QRect(137, 5, 61, 62))
        self.slot_1_3.setStyleSheet(_fromUtf8("background: rgb(58, 6, 2);\n"
                                              "color: rgb(222, 222, 222)"))
        self.slot_1_3.setObjectName(_fromUtf8("slot_1_3"))
        self.slot_1_4 = QtGui.QPushButton(self.inventMode)
        self.slot_1_4.setGeometry(QtCore.QRect(204, 5, 61, 62))
        self.slot_1_4.setStyleSheet(_fromUtf8("background: rgb(58, 6, 2);\n"
                                              "color: rgb(222, 222, 222)"))
        self.slot_1_4.setObjectName(_fromUtf8("slot_1_4"))
        self.slot_2_3 = QtGui.QPushButton(self.inventMode)
        self.slot_2_3.setGeometry(QtCore.QRect(137, 74, 61, 62))
        self.slot_2_3.setStyleSheet(_fromUtf8("background: rgb(58, 6, 2);\n"
                                              "color: rgb(222, 222, 222)"))
        self.slot_2_3.setObjectName(_fromUtf8("slot_2_3"))
        self.slot_2_4 = QtGui.QPushButton(self.inventMode)
        self.slot_2_4.setGeometry(QtCore.QRect(204, 74, 61, 62))
        self.slot_2_4.setStyleSheet(_fromUtf8("background: rgb(58, 6, 2);\n"
                                              "color: rgb(222, 222, 222)"))
        self.slot_2_4.setObjectName(_fromUtf8("slot_2_4"))
        self.slot_2_1 = QtGui.QPushButton(self.inventMode)
        self.slot_2_1.setGeometry(QtCore.QRect(5, 74, 61, 62))
        self.slot_2_1.setStyleSheet(_fromUtf8("background: rgb(58, 6, 2);\n"
                                              "color: rgb(222, 222, 222)"))
        self.slot_2_1.setObjectName(_fromUtf8("slot_2_1"))
        self.slot_2_2 = QtGui.QPushButton(self.inventMode)
        self.slot_2_2.setGeometry(QtCore.QRect(70, 74, 61, 62))
        self.slot_2_2.setStyleSheet(_fromUtf8("background: rgb(58, 6, 2);\n"
                                              "color: rgb(222, 222, 222)"))
        self.slot_2_2.setObjectName(_fromUtf8("slot_2_2"))
        self.slot_3_3 = QtGui.QPushButton(self.inventMode)
        self.slot_3_3.setGeometry(QtCore.QRect(137, 141, 61, 62))
        self.slot_3_3.setStyleSheet(_fromUtf8("background: rgb(58, 6, 2);\n"
                                              "color: rgb(222, 222, 222)"))
        self.slot_3_3.setObjectName(_fromUtf8("slot_3_3"))
        self.slot_3_4 = QtGui.QPushButton(self.inventMode)
        self.slot_3_4.setGeometry(QtCore.QRect(204, 141, 61, 62))
        self.slot_3_4.setStyleSheet(_fromUtf8("background: rgb(58, 6, 2);\n"
                                              "color: rgb(222, 222, 222)"))
        self.slot_3_4.setObjectName(_fromUtf8("slot_3_4"))
        self.slot_3_1 = QtGui.QPushButton(self.inventMode)
        self.slot_3_1.setGeometry(QtCore.QRect(5, 141, 61, 62))
        self.slot_3_1.setStyleSheet(_fromUtf8("background: rgb(58, 6, 2);\n"
                                              "color: rgb(222, 222, 222)"))
        self.slot_3_1.setObjectName(_fromUtf8("slot_3_1"))
        self.slot_3_2 = QtGui.QPushButton(self.inventMode)
        self.slot_3_2.setGeometry(QtCore.QRect(70, 141, 61, 62))
        self.slot_3_2.setStyleSheet(_fromUtf8("background: rgb(58, 6, 2);\n"
                                              "color: rgb(222, 222, 222)"))
        self.slot_3_2.setObjectName(_fromUtf8("slot_3_2"))
        self.slot_4_3 = QtGui.QPushButton(self.inventMode)
        self.slot_4_3.setGeometry(QtCore.QRect(137, 210, 61, 62))
        self.slot_4_3.setStyleSheet(_fromUtf8("background: rgb(58, 6, 2);\n"
                                              "color: rgb(222, 222, 222)"))
        self.slot_4_3.setObjectName(_fromUtf8("slot_4_3"))
        self.slot_4_4 = QtGui.QPushButton(self.inventMode)
        self.slot_4_4.setGeometry(QtCore.QRect(204, 210, 61, 62))
        self.slot_4_4.setStyleSheet(_fromUtf8("background: rgb(58, 6, 2);\n"
                                              "color: rgb(222, 222, 222)"))
        self.slot_4_4.setObjectName(_fromUtf8("slot_4_4"))
        self.slot_4_1 = QtGui.QPushButton(self.inventMode)
        self.slot_4_1.setGeometry(QtCore.QRect(5, 210, 61, 62))
        self.slot_4_1.setStyleSheet(_fromUtf8("background: rgb(58, 6, 2);\n"
                                              "color: rgb(222, 222, 222)"))
        self.slot_4_1.setObjectName(_fromUtf8("slot_4_1"))
        self.slot_4_2 = QtGui.QPushButton(self.inventMode)
        self.slot_4_2.setGeometry(QtCore.QRect(70, 210, 61, 62))
        self.slot_4_2.setStyleSheet(_fromUtf8("background: rgb(58, 6, 2);\n"
                                              "color: rgb(222, 222, 222)"))
        self.slot_4_2.setObjectName(_fromUtf8("slot_4_2"))
        self.warninglabel = QtGui.QLabel(self.inventMode)
        self.warninglabel.setGeometry(QtCore.QRect(37, 284, 218, 16))
        self.warninglabel.setStyleSheet(_fromUtf8("background: transparent;"))
        self.warninglabel.setObjectName(_fromUtf8("warninglabel"))
        self.vaultCat.addItem(self.inventMode, _fromUtf8(""))
        self.charMode = QtGui.QWidget()
        self.charMode.setGeometry(QtCore.QRect(0, 0, 273, 312))
        self.charMode.setObjectName(_fromUtf8("charMode"))
        self.charMode_helm = QtGui.QPushButton(self.charMode)
        self.charMode_helm.setGeometry(QtCore.QRect(86, 3, 94, 68))
        self.charMode_helm.setStyleSheet(_fromUtf8("QPushButton{\n"
                                                   "\n"
                                                   "background:rgb(11, 11, 11);\n"
                                                   "color: rgb(93, 93, 93);\n"
                                                   "}\n"
                                                   "QPushButton::Pressed{\n"
                                                   "background: rgb(255, 107, 48)\n"
                                                   "}"))
        self.charMode_helm.setIconSize(QtCore.QSize(100, 70))
        self.charMode_helm.setObjectName(_fromUtf8("charMode_helm"))
        self.charMode_armor = QtGui.QPushButton(self.charMode)
        self.charMode_armor.setGeometry(QtCore.QRect(86, 72, 94, 104))
        self.charMode_armor.setStyleSheet(_fromUtf8("QPushButton{\n"
                                                    "\n"
                                                    "background:rgb(11, 11, 11);\n"
                                                    "color: rgb(93, 93, 93);\n"
                                                    "}\n"
                                                    "QPushButton::Pressed{\n"
                                                    "background: rgb(255, 107, 48)\n"
                                                    "}"))
        self.charMode_armor.setIconSize(QtCore.QSize(120, 120))
        self.charMode_armor.setObjectName(_fromUtf8("charMode_armor"))
        self.charMode_pants = QtGui.QPushButton(self.charMode)
        self.charMode_pants.setGeometry(QtCore.QRect(86, 175, 94, 66))
        self.charMode_pants.setStyleSheet(_fromUtf8("QPushButton{\n"
                                                    "\n"
                                                    "background:rgb(11, 11, 11);\n"
                                                    "color: rgb(93, 93, 93);\n"
                                                    "}\n"
                                                    "QPushButton::Pressed{\n"
                                                    "background: rgb(255, 107, 48)\n"
                                                    "}"))
        self.charMode_pants.setIconSize(QtCore.QSize(100, 100))
        self.charMode_pants.setObjectName(_fromUtf8("charMode_pants"))
        self.charMode_weap2 = QtGui.QPushButton(self.charMode)
        self.charMode_weap2.setGeometry(QtCore.QRect(181, 82, 88, 121))
        self.charMode_weap2.setStyleSheet(_fromUtf8("QPushButton{\n"
                                                    "\n"
                                                    "background:rgb(11, 11, 11);\n"
                                                    "color: rgb(93, 93, 93);\n"
                                                    "}\n"
                                                    "QPushButton::Pressed{\n"
                                                    "background: rgb(255, 107, 48)\n"
                                                    "}"))
        self.charMode_weap2.setIconSize(QtCore.QSize(120, 120))
        self.charMode_weap2.setObjectName(_fromUtf8("charMode_weap2"))
        self.charMode_weap1 = QtGui.QPushButton(self.charMode)
        self.charMode_weap1.setGeometry(QtCore.QRect(4, 81, 81, 122))
        self.charMode_weap1.setStyleSheet(_fromUtf8("QPushButton{\n"
                                                    "\n"
                                                    "background:rgb(11, 11, 11);\n"
                                                    "color: rgb(93, 93, 93);\n"
                                                    "}\n"
                                                    "QPushButton::Pressed{\n"
                                                    "background: rgb(255, 107, 48)\n"
                                                    "}"))
        self.charMode_weap1.setIconSize(QtCore.QSize(100, 100))
        self.charMode_weap1.setObjectName(_fromUtf8("charMode_weap1"))
        self.charMode_boot = QtGui.QPushButton(self.charMode)
        self.charMode_boot.setGeometry(QtCore.QRect(132, 242, 93, 66))
        self.charMode_boot.setStyleSheet(_fromUtf8("QPushButton{\n"
                                                   "\n"
                                                   "background:rgb(11, 11, 11);\n"
                                                   "color: rgb(93, 93, 93);\n"
                                                   "}\n"
                                                   "QPushButton::Pressed{\n"
                                                   "background: rgb(255, 107, 48)\n"
                                                   "}"))
        self.charMode_boot.setIconSize(QtCore.QSize(100, 100))
        self.charMode_boot.setObjectName(_fromUtf8("charMode_boot"))
        self.charMode_gloves = QtGui.QPushButton(self.charMode)
        self.charMode_gloves.setGeometry(QtCore.QRect(38, 242, 93, 66))
        self.charMode_gloves.setStyleSheet(_fromUtf8("QPushButton{\n"
                                                     "\n"
                                                     "background:rgb(11, 11, 11);\n"
                                                     "color: rgb(93, 93, 93);\n"
                                                     "}\n"
                                                     "QPushButton::Pressed{\n"
                                                     "background: rgb(255, 107, 48)\n"
                                                     "}"))
        self.charMode_gloves.setIconSize(QtCore.QSize(100, 100))
        self.charMode_gloves.setObjectName(_fromUtf8("charMode_gloves"))
        self.charMode_wing = QtGui.QPushButton(self.charMode)
        self.charMode_wing.setGeometry(QtCore.QRect(181, 13, 88, 68))
        self.charMode_wing.setStyleSheet(_fromUtf8("QPushButton{\n"
                                                   "\n"
                                                   "background:rgb(11, 11, 11);\n"
                                                   "color: rgb(93, 93, 93);\n"
                                                   "}\n"
                                                   "QPushButton::Pressed{\n"
                                                   "background: rgb(255, 107, 48)\n"
                                                   "}"))
        self.charMode_wing.setIconSize(QtCore.QSize(100, 70))
        self.charMode_wing.setObjectName(_fromUtf8("charMode_wing"))
        self.charMode_pet = QtGui.QPushButton(self.charMode)
        self.charMode_pet.setGeometry(QtCore.QRect(3, 13, 82, 67))
        self.charMode_pet.setStyleSheet(_fromUtf8("QPushButton{\n"
                                                  "\n"
                                                  "background:rgb(11, 11, 11);\n"
                                                  "color: rgb(93, 93, 93);\n"
                                                  "}\n"
                                                  "QPushButton::Pressed{\n"
                                                  "background: rgb(255, 107, 48)\n"
                                                  "}"))
        self.charMode_pet.setIconSize(QtCore.QSize(100, 70))
        self.charMode_pet.setCheckable(False)
        self.charMode_pet.setChecked(False)
        self.charMode_pet.setAutoDefault(False)
        self.charMode_pet.setObjectName(_fromUtf8("charMode_pet"))
        self.charMode_pendant = QtGui.QPushButton(self.charMode)
        self.charMode_pendant.setGeometry(QtCore.QRect(48, 205, 37, 32))
        self.charMode_pendant.setStyleSheet(_fromUtf8("QPushButton{\n"
                                                      "\n"
                                                      "background:rgb(11, 11, 11);\n"
                                                      "color: rgb(93, 93, 93);\n"
                                                      "}\n"
                                                      "QPushButton::Pressed{\n"
                                                      "background: rgb(255, 107, 48)\n"
                                                      "}"))
        self.charMode_pendant.setIconSize(QtCore.QSize(100, 100))
        self.charMode_pendant.setObjectName(_fromUtf8("charMode_pendant"))
        self.charMode_ring1 = QtGui.QPushButton(self.charMode)
        self.charMode_ring1.setGeometry(QtCore.QRect(188, 206, 37, 32))
        self.charMode_ring1.setStyleSheet(_fromUtf8("QPushButton{\n"
                                                    "\n"
                                                    "background:rgb(11, 11, 11);\n"
                                                    "color: rgb(93, 93, 93);\n"
                                                    "}\n"
                                                    "QPushButton::Pressed{\n"
                                                    "background: rgb(255, 107, 48)\n"
                                                    "}"))
        self.charMode_ring1.setIconSize(QtCore.QSize(100, 100))
        self.charMode_ring1.setObjectName(_fromUtf8("charMode_ring1"))
        self.charMode_ring2 = QtGui.QPushButton(self.charMode)
        self.charMode_ring2.setGeometry(QtCore.QRect(229, 206, 37, 32))
        self.charMode_ring2.setStyleSheet(_fromUtf8("QPushButton{\n"
                                                    "\n"
                                                    "background:rgb(11, 11, 11);\n"
                                                    "color: rgb(93, 93, 93);\n"
                                                    "}\n"
                                                    "QPushButton::Pressed{\n"
                                                    "background: rgb(255, 107, 48)\n"
                                                    "}"))
        self.charMode_ring2.setIconSize(QtCore.QSize(100, 100))
        self.charMode_ring2.setObjectName(_fromUtf8("charMode_ring2"))
        self.vaultCat.addItem(self.charMode, _fromUtf8(""))
        self.MessageBG = QtGui.QPushButton(self.itemMakerPage)
        self.MessageBG.setGeometry(QtCore.QRect(624, 20, 165, 296))
        self.MessageBG.setStyleSheet(_fromUtf8("QPushButton {\n"
                                               "color: #333;\n"
                                               "\n"
                                               "border: 2px solid #555;\n"
                                               "border-radius: 11px;\n"
                                               "padding: 5px;\n"
                                               "background:rgb(151, 156, 133)\n"
                                               "}\n"
                                               "\n"
                                               ""))
        self.MessageBG.setText(_fromUtf8(""))
        self.MessageBG.setObjectName(_fromUtf8("MessageBG"))
        self.Message = QtGui.QTextBrowser(self.itemMakerPage)
        self.Message.setGeometry(QtCore.QRect(630, 55, 154, 229))
        self.Message.setStyleSheet(_fromUtf8("background: rgb(99, 99, 99)"))
        self.Message.setObjectName(_fromUtf8("Message"))
        self.Progress = QtGui.QProgressBar(self.itemMakerPage)
        self.Progress.setGeometry(QtCore.QRect(628, 286, 168, 23))
        self.Progress.setStyleSheet(_fromUtf8("QProgressBar:horizontal {\n"
                                              "border: 1px solid gray;\n"
                                              "border-radius: 3px;\n"
                                              "background: white;\n"
                                              "padding: 1px;\n"
                                              "text-align: center;\n"
                                              "margin-right: 4ex;\n"
                                              "}\n"
                                              "QProgressBar::chunk:horizontal {\n"
                                              "background: rgb(255, 71, 47);\n"
                                              "width: 10px;\n"
                                              "}"))
        self.Progress.setProperty("value", 100)
        self.Progress.setObjectName(_fromUtf8("Progress"))
        self.BGIMaker = QtGui.QLabel(self.itemMakerPage)
        self.BGIMaker.setGeometry(QtCore.QRect(289, 0, 331, 406))
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.BGIMaker.sizePolicy().hasHeightForWidth())
        self.BGIMaker.setSizePolicy(sizePolicy)
        self.BGIMaker.setStyleSheet(_fromUtf8("background: rgb(255, 255, 255);\n"
                                              "border: 2px solid #555;\n"
                                              "border-radius: 9px;"))
        self.BGIMaker.setText(_fromUtf8(""))
        self.BGIMaker.setObjectName(_fromUtf8("BGIMaker"))
        self.CancelNow = QtGui.QPushButton(self.itemMakerPage)
        self.CancelNow.setDisabled(True) # will be update soon
        self.CancelNow.setGeometry(QtCore.QRect(628, 319, 150, 40))
        font = QtGui.QFont()
        font.setPointSize(11)
        font.setBold(True)
        font.setWeight(75)
        font.setStyleStrategy(QtGui.QFont.PreferAntialias)
        self.CancelNow.setFont(font)
        self.CancelNow.setStyleSheet(_fromUtf8("QPushButton {\n"
                                               "color: #333;\n"
                                               "\n"
                                               "border: 1px solid #555;\n"
                                               "border-radius: 11px;\n"
                                               "padding: 5px;\n"
                                               "background: qradialgradient(cx: 0.3, cy: -0.4,\n"
                                               "fx: 0.3, fy: -0.4,\n"
                                               "radius: 1.35, stop: 0 #fff, stop: 1 #888);\n"
                                               "min-width: 80px;\n"
                                               "}\n"
                                               "\n"
                                               "\n"
                                               "QPushButton:hover {\n"
                                               "background: qradialgradient(cx: 0.3, cy: -0.4,\n"
                                               "fx: 0.3, fy: -0.4,\n"
                                               "radius: 1.35, stop: 0 #fff, stop: 1 #bbb);\n"
                                               "}\n"
                                               "\n"
                                               "QPushButton:pressed {\n"
                                               "background: qradialgradient(cx: 0.4, cy: -0.1,\n"
                                               "fx: 0.4, fy: -0.1,\n"
                                               "radius: 1.35, stop: 0 #fff, stop: 1 #ddd);\n"
                                               "}"))
        self.CancelNow.setObjectName(_fromUtf8("CancelNow"))
        self.ProcessAll = QtGui.QPushButton(self.itemMakerPage)
        self.ProcessAll.setGeometry(QtCore.QRect(628, 362, 150, 40))
        font = QtGui.QFont()
        font.setPointSize(11)
        font.setBold(True)
        font.setWeight(75)
        font.setStyleStrategy(QtGui.QFont.PreferAntialias)
        self.ProcessAll.setFont(font)
        self.ProcessAll.setStyleSheet(_fromUtf8("QPushButton {\n"
                                                "color: #333;\n"
                                                "\n"
                                                "border: 1px solid #555;\n"
                                                "border-radius: 11px;\n"
                                                "padding: 5px;\n"
                                                "background: qradialgradient(cx: 0.3, cy: -0.4,\n"
                                                "fx: 0.3, fy: -0.4,\n"
                                                "radius: 1.35, stop: 0 #fff, stop: 1 #888);\n"
                                                "min-width: 80px;\n"
                                                "}\n"
                                                "\n"
                                                "\n"
                                                "QPushButton:hover {\n"
                                                "background: qradialgradient(cx: 0.3, cy: -0.4,\n"
                                                "fx: 0.3, fy: -0.4,\n"
                                                "radius: 1.35, stop: 0 #fff, stop: 1 #bbb);\n"
                                                "}\n"
                                                "\n"
                                                "QPushButton:pressed {\n"
                                                "background: qradialgradient(cx: 0.4, cy: -0.1,\n"
                                                "fx: 0.4, fy: -0.1,\n"
                                                "radius: 1.35, stop: 0 #fff, stop: 1 #ddd);\n"
                                                "}"))
        self.ProcessAll.setObjectName(_fromUtf8("ProcessAll"))
        self.ItemView = QtGui.QListWidget(self.itemMakerPage)
        self.ItemView.setGeometry(QtCore.QRect(457, 26, 159, 275))
        self.ItemView.setStyleSheet(_fromUtf8("background: transparent;\n"
                                              "border: 1px solid #555;"))
        self.ItemView.setObjectName(_fromUtf8("ItemView"))
        self.CatViewLabel = QtGui.QLabel(self.itemMakerPage)
        self.CatViewLabel.setGeometry(QtCore.QRect(298, 5, 68, 22))
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        self.CatViewLabel.setFont(font)
        self.CatViewLabel.setStyleSheet(_fromUtf8("background: transparent;\n"
                                                  "color: rgb(74, 74, 74);\n"
                                                  ""))
        self.CatViewLabel.setObjectName(_fromUtf8("CatViewLabel"))
        self.contentsLabel = QtGui.QLabel(self.itemMakerPage)
        self.contentsLabel.setGeometry(QtCore.QRect(553, 8, 64, 16))
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        self.contentsLabel.setFont(font)
        self.contentsLabel.setStyleSheet(_fromUtf8("background: transparent;\n"
                                                   "color: rgb(74, 74, 74);\n"
                                                   ""))
        self.contentsLabel.setObjectName(_fromUtf8("contentsLabel"))
        self.CategoriesView = QtGui.QComboBox(self.itemMakerPage)
        self.CategoriesView.setGeometry(QtCore.QRect(296, 31, 157, 29))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Narkisim"))
        font.setPointSize(13)
        font.setBold(True)
        font.setWeight(75)
        font.setStyleStrategy(QtGui.QFont.PreferAntialias)
        self.CategoriesView.setFont(font)
        self.CategoriesView.setStyleSheet(_fromUtf8(" QComboBox {\n"
                                                    "     border: 1px solid gray;\n"
                                                    "     border-radius: 3px;\n"
                                                    "     padding: 1px 18px 1px 3px;\n"
                                                    "     min-width: 6em;\n"
                                                    "color:rgb(79, 79, 79);\n"
                                                    " }\n"
                                                    "\n"
                                                    " QComboBox:editable {\n"
                                                    "     background: white;\n"
                                                    " }\n"
                                                    "\n"
                                                    " QComboBox:!editable, QComboBox::drop-down:editable {\n"
                                                    "      background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
                                                    "                                  stop: 0 #E1E1E1, stop: 0.4 #DDDDDD,\n"
                                                    "                                  stop: 0.5 #D8D8D8, stop: 1.0 #D3D3D3);\n"
                                                    " }\n"
                                                    "\n"
                                                    " /* QComboBox gets the \"on\" state when the popup is open */\n"
                                                    " QComboBox:!editable:on, QComboBox::drop-down:editable:on {\n"
                                                    "     background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
                                                    "                                 stop: 0 #D3D3D3, stop: 0.4 #D8D8D8,\n"
                                                    "                                 stop: 0.5 #DDDDDD, stop: 1.0 #E1E1E1);\n"
                                                    " }\n"
                                                    "\n"
                                                    " QComboBox:on { /* shift the text when the popup opens */\n"
                                                    "     padding-top: 3px;\n"
                                                    "     padding-left: 4px;\n"
                                                    " }\n"
                                                    "\n"
                                                    " QComboBox::drop-down {\n"
                                                    "     subcontrol-origin: padding;\n"
                                                    "     subcontrol-position: top right;\n"
                                                    "     width: 15px;\n"
                                                    "     background: #555;\n"
                                                    "     border-left-width: 1px;\n"
                                                    "     border-left-color: darkgray;\n"
                                                    "     border-left-style: solid; /* just a single line */\n"
                                                    "     border-top-right-radius: 3px; /* same radius as the QComboBox */\n"
                                                    "     border-bottom-right-radius: 3px;\n"
                                                    " }\n"
                                                    "QComboBox QAbstractItemView {\n"
                                                    "    border: 2px solid darkgray;\n"
                                                    "    color: white;\n"
                                                    "    padding: 10px 10px 10px 10px;\n"
                                                    "    background: brown;\n"
                                                    "    selection-background-color: black;\n"
                                                    "}\n"
                                                    "\n"
                                                    ""))
        self.CategoriesView.setObjectName(_fromUtf8("CategoriesView"))
        self.CategoriesView.addItem(_fromUtf8(""))
        self.Option_Zen = QtGui.QRadioButton(self.itemMakerPage)
        self.Option_Zen.setGeometry(QtCore.QRect(300, 80, 150, 17))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.Option_Zen.setFont(font)
        self.Option_Zen.setStyleSheet(_fromUtf8("background: transparent;"))
        self.Option_Zen.setChecked(False)
        self.Option_Zen.setAutoRepeat(False)
        self.Option_Zen.setAutoExclusive(False)
        self.Option_Zen.setObjectName(_fromUtf8("Option_Zen"))
        self.Option_DSR = QtGui.QRadioButton(self.itemMakerPage)
        self.Option_DSR.setGeometry(QtCore.QRect(300, 100, 150, 17))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.Option_DSR.setFont(font)
        self.Option_DSR.setStyleSheet(_fromUtf8("background: transparent;"))
        self.Option_DSR.setChecked(False)
        self.Option_DSR.setAutoExclusive(False)
        self.Option_DSR.setObjectName(_fromUtf8("Option_DSR"))
        self.Option_Reflect = QtGui.QRadioButton(self.itemMakerPage)
        self.Option_Reflect.setGeometry(QtCore.QRect(300, 120, 150, 17))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.Option_Reflect.setFont(font)
        self.Option_Reflect.setStyleSheet(_fromUtf8("background: transparent;"))
        self.Option_Reflect.setAutoExclusive(False)
        self.Option_Reflect.setObjectName(_fromUtf8("Option_Reflect"))
        self.Option_DD = QtGui.QRadioButton(self.itemMakerPage)
        self.Option_DD.setGeometry(QtCore.QRect(300, 140, 155, 17))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.Option_DD.setFont(font)
        self.Option_DD.setStyleSheet(_fromUtf8("background: transparent;"))
        self.Option_DD.setAutoExclusive(False)
        self.Option_DD.setObjectName(_fromUtf8("Option_DD"))
        self.Option_Mana = QtGui.QRadioButton(self.itemMakerPage)
        self.Option_Mana.setGeometry(QtCore.QRect(300, 160, 155, 17))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.Option_Mana.setFont(font)
        self.Option_Mana.setStyleSheet(_fromUtf8("background: transparent;"))
        self.Option_Mana.setAutoExclusive(False)
        self.Option_Mana.setObjectName(_fromUtf8("Option_Mana"))
        self.Option_HP = QtGui.QRadioButton(self.itemMakerPage)
        self.Option_HP.setGeometry(QtCore.QRect(300, 180, 155, 17))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.Option_HP.setFont(font)
        self.Option_HP.setStyleSheet(_fromUtf8("background: transparent;"))
        self.Option_HP.setAutoExclusive(False)
        self.Option_HP.setObjectName(_fromUtf8("Option_HP"))
        self.Option_Yellow1 = QtGui.QComboBox(self.itemMakerPage)
        self.Option_Yellow1.setGeometry(QtCore.QRect(456, 308, 157, 21))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Arial"))
        font.setPointSize(8)
        font.setBold(True)
        font.setWeight(75)
        font.setStyleStrategy(QtGui.QFont.PreferAntialias)
        self.Option_Yellow1.setFont(font)
        self.Option_Yellow1.setStyleSheet(_fromUtf8(" QComboBox {\n"
                                                    "     border: 1px solid gray;\n"
                                                    "     border-radius: 3px;\n"
                                                    "     padding: 1px 18px 1px 3px;\n"
                                                    "     min-width: 6em;\n"
                                                    "color:rgb(79, 79, 79);\n"
                                                    " }\n"
                                                    "\n"
                                                    " QComboBox:editable {\n"
                                                    "     background: white;\n"
                                                    " }\n"
                                                    "\n"
                                                    " QComboBox:!editable, QComboBox::drop-down:editable {\n"
                                                    "      background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
                                                    "                                  stop: 0 #E1E1E1, stop: 0.4 #DDDDDD,\n"
                                                    "                                  stop: 0.5 #D8D8D8, stop: 1.0 #D3D3D3);\n"
                                                    " }\n"
                                                    "\n"
                                                    " /* QComboBox gets the \"on\" state when the popup is open */\n"
                                                    " QComboBox:!editable:on, QComboBox::drop-down:editable:on {\n"
                                                    "     background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
                                                    "                                 stop: 0 #D3D3D3, stop: 0.4 #D8D8D8,\n"
                                                    "                                 stop: 0.5 #DDDDDD, stop: 1.0 #E1E1E1);\n"
                                                    " }\n"
                                                    "\n"
                                                    " QComboBox:on { /* shift the text when the popup opens */\n"
                                                    "     padding-top: 3px;\n"
                                                    "     padding-left: 4px;\n"
                                                    " }\n"
                                                    "\n"
                                                    " QComboBox::drop-down {\n"
                                                    "     subcontrol-origin: padding;\n"
                                                    "     subcontrol-position: top right;\n"
                                                    "     width: 15px;\n"
                                                    "     background: #555;\n"
                                                    "     border-left-width: 1px;\n"
                                                    "     border-left-color: darkgray;\n"
                                                    "     border-left-style: solid; /* just a single line */\n"
                                                    "     border-top-right-radius: 3px; /* same radius as the QComboBox */\n"
                                                    "     border-bottom-right-radius: 3px;\n"
                                                    " }\n"
                                                    "QComboBox QAbstractItemView {\n"
                                                    "    border: 2px solid darkgray;\n"
                                                    "    color: black;\n"
                                                    "background: yellow;\n"
                                                    "selection-color: black;\n"
                                                    "    selection-background-color: grey;\n"
                                                    "}\n"
                                                    "\n"
                                                    ""))
        self.Option_Yellow1.setObjectName(_fromUtf8("Option_Yellow1"))
        self.Option_Yellow1.addItem(_fromUtf8(""))
        self.Option_Yellow2 = QtGui.QComboBox(self.itemMakerPage)
        self.Option_Yellow2.setGeometry(QtCore.QRect(456, 335, 157, 21))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Arial"))
        font.setPointSize(8)
        font.setBold(True)
        font.setWeight(75)
        font.setStyleStrategy(QtGui.QFont.PreferAntialias)
        self.Option_Yellow2.setFont(font)
        self.Option_Yellow2.setStyleSheet(_fromUtf8(" QComboBox {\n"
                                                    "     border: 1px solid gray;\n"
                                                    "     border-radius: 3px;\n"
                                                    "     padding: 1px 18px 1px 3px;\n"
                                                    "     min-width: 6em;\n"
                                                    "color:rgb(79, 79, 79);\n"
                                                    " }\n"
                                                    "\n"
                                                    " QComboBox:editable {\n"
                                                    "     background: white;\n"
                                                    " }\n"
                                                    "\n"
                                                    " QComboBox:!editable, QComboBox::drop-down:editable {\n"
                                                    "      background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
                                                    "                                  stop: 0 #E1E1E1, stop: 0.4 #DDDDDD,\n"
                                                    "                                  stop: 0.5 #D8D8D8, stop: 1.0 #D3D3D3);\n"
                                                    " }\n"
                                                    "\n"
                                                    " /* QComboBox gets the \"on\" state when the popup is open */\n"
                                                    " QComboBox:!editable:on, QComboBox::drop-down:editable:on {\n"
                                                    "     background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
                                                    "                                 stop: 0 #D3D3D3, stop: 0.4 #D8D8D8,\n"
                                                    "                                 stop: 0.5 #DDDDDD, stop: 1.0 #E1E1E1);\n"
                                                    " }\n"
                                                    "\n"
                                                    " QComboBox:on { /* shift the text when the popup opens */\n"
                                                    "     padding-top: 3px;\n"
                                                    "     padding-left: 4px;\n"
                                                    " }\n"
                                                    "\n"
                                                    " QComboBox::drop-down {\n"
                                                    "     subcontrol-origin: padding;\n"
                                                    "     subcontrol-position: top right;\n"
                                                    "     width: 15px;\n"
                                                    "     background: #555;\n"
                                                    "     border-left-width: 1px;\n"
                                                    "     border-left-color: darkgray;\n"
                                                    "     border-left-style: solid; /* just a single line */\n"
                                                    "     border-top-right-radius: 3px; /* same radius as the QComboBox */\n"
                                                    "     border-bottom-right-radius: 3px;\n"
                                                    " }\n"
                                                    "QComboBox QAbstractItemView {\n"
                                                    "    border: 2px solid darkgray;\n"
                                                    "    color: black;\n"
                                                    "background: yellow;\n"
                                                    "selection-color: black;\n"
                                                    "    selection-background-color: grey;\n"
                                                    "}\n"
                                                    "\n"
                                                    ""))
        self.Option_Yellow2.setObjectName(_fromUtf8("Option_Yellow2"))
        self.Option_Yellow2.addItem(_fromUtf8(""))
        self.seper = QtGui.QLabel(self.itemMakerPage)
        self.seper.setGeometry(QtCore.QRect(290, 199, 168, 40))
        self.seper.setStyleSheet(_fromUtf8("background: transparent;\n"
                                           "color: rgb(154, 154, 154)"))
        self.seper.setObjectName(_fromUtf8("seper"))
        self.Option_380 = QtGui.QRadioButton(self.itemMakerPage)
        self.Option_380.setGeometry(QtCore.QRect(300, 201, 155, 17))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.Option_380.setFont(font)
        self.Option_380.setStyleSheet(_fromUtf8("background: transparent;\n"
                                                "color:rgb(170, 64, 119)"))
        self.Option_380.setAutoExclusive(False)
        self.Option_380.setObjectName(_fromUtf8("Option_380"))
        self.OptionsLabelDown = QtGui.QLabel(self.itemMakerPage)
        self.OptionsLabelDown.setGeometry(QtCore.QRect(303, 334, 46, 13))
        self.OptionsLabelDown.setStyleSheet(_fromUtf8("background: transparent;"))
        self.OptionsLabelDown.setObjectName(_fromUtf8("OptionsLabelDown"))
        self.plusLabel = QtGui.QLabel(self.itemMakerPage)
        self.plusLabel.setGeometry(QtCore.QRect(381, 328, 62, 23))
        self.plusLabel.setStyleSheet(_fromUtf8("background: transparent;"))
        self.plusLabel.setObjectName(_fromUtf8("plusLabel"))
        self.Option_Options = QtGui.QSlider(self.itemMakerPage)
        self.Option_Options.setGeometry(QtCore.QRect(304, 248, 40, 82))
        self.Option_Options.setStyleSheet(_fromUtf8("background: transparent;"))
        self.Option_Options.setMaximum(28)
        self.Option_Options.setOrientation(QtCore.Qt.Vertical)
        self.Option_Options.setTickPosition(QtGui.QSlider.TicksBothSides)
        self.Option_Options.setObjectName(_fromUtf8("Option_Options"))
        self.Option_Plus = QtGui.QSlider(self.itemMakerPage)
        self.Option_Plus.setGeometry(QtCore.QRect(391, 246, 40, 82))
        self.Option_Plus.setStyleSheet(_fromUtf8("background: transparent;"))
        self.Option_Plus.setMaximum(13)
        self.Option_Plus.setOrientation(QtCore.Qt.Vertical)
        self.Option_Plus.setTickPosition(QtGui.QSlider.TicksBothSides)
        self.Option_Plus.setObjectName(_fromUtf8("Option_Plus"))
        self.Options_OptionValue = QtGui.QLabel(self.itemMakerPage)
        self.Options_OptionValue.setGeometry(QtCore.QRect(306, 214, 39, 40))
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        self.Options_OptionValue.setFont(font)
        self.Options_OptionValue.setStyleSheet(_fromUtf8("background: transparent;"))
        self.Options_OptionValue.setObjectName(_fromUtf8("Options_OptionValue"))
        self.Options_PlusValue = QtGui.QLabel(self.itemMakerPage)
        self.Options_PlusValue.setGeometry(QtCore.QRect(392, 213, 39, 40))
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        self.Options_PlusValue.setFont(font)
        self.Options_PlusValue.setStyleSheet(_fromUtf8("background: transparent;"))
        self.Options_PlusValue.setObjectName(_fromUtf8("Options_PlusValue"))
        self.Option_Zen_2 = QtGui.QRadioButton(self.itemMakerPage)
        self.Option_Zen_2.setGeometry(QtCore.QRect(300, 61, 150, 17))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.Option_Zen_2.setFont(font)
        self.Option_Zen_2.setStyleSheet(_fromUtf8("background: transparent;"))
        self.Option_Zen_2.setChecked(False)
        self.Option_Zen_2.setAutoRepeat(False)
        self.Option_Zen_2.setAutoExclusive(False)
        self.Option_Zen_2.setObjectName(_fromUtf8("Option_Zen_2"))
        self.AccountLabel = QtGui.QLabel(self.itemMakerPage)
        self.AccountLabel.setGeometry(QtCore.QRect(46, -3, 57, 34))
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        self.AccountLabel.setFont(font)
        self.AccountLabel.setStyleSheet(_fromUtf8("background: transparent;"))
        self.AccountLabel.setObjectName(_fromUtf8("AccountLabel"))
        self.AccountValue = QtGui.QComboBox(self.itemMakerPage)
        self.AccountValue.setGeometry(QtCore.QRect(103, 6, 177, 17))
        self.AccountValue.setStyleSheet(_fromUtf8(" QComboBox {\n"
                                                  "     border: 1px solid gray;\n"
                                                  "     border-radius: 3px;\n"
                                                  "     padding: 1px 18px 1px 3px;\n"
                                                  "     min-width: 6em;\n"
                                                  "color:rgb(79, 79, 79);\n"
                                                  " }\n"
                                                  "\n"
                                                  " QComboBox:editable {\n"
                                                  "     background: white;\n"
                                                  " }\n"
                                                  "\n"
                                                  " QComboBox:!editable, QComboBox::drop-down:editable {\n"
                                                  "      background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
                                                  "                                  stop: 0 #E1E1E1, stop: 0.4 #DDDDDD,\n"
                                                  "                                  stop: 0.5 #D8D8D8, stop: 1.0 #D3D3D3);\n"
                                                  " }\n"
                                                  "\n"
                                                  " /* QComboBox gets the \"on\" state when the popup is open */\n"
                                                  " QComboBox:!editable:on, QComboBox::drop-down:editable:on {\n"
                                                  "     background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
                                                  "                                 stop: 0 #D3D3D3, stop: 0.4 #D8D8D8,\n"
                                                  "                                 stop: 0.5 #DDDDDD, stop: 1.0 #E1E1E1);\n"
                                                  " }\n"
                                                  "\n"
                                                  " QComboBox:on { /* shift the text when the popup opens */\n"
                                                  "     padding-top: 3px;\n"
                                                  "     padding-left: 4px;\n"
                                                  " }\n"
                                                  "\n"
                                                  " QComboBox::drop-down {\n"
                                                  "     subcontrol-origin: padding;\n"
                                                  "     subcontrol-position: top right;\n"
                                                  "     width: 15px;\n"
                                                  "     background: #555;\n"
                                                  "     border-left-width: 1px;\n"
                                                  "     border-left-color: darkgray;\n"
                                                  "     border-left-style: solid; /* just a single line */\n"
                                                  "     border-top-right-radius: 3px; /* same radius as the QComboBox */\n"
                                                  "     border-bottom-right-radius: 3px;\n"
                                                  " }\n"
                                                  "QComboBox QAbstractItemView {\n"
                                                  "    border: 2px solid darkgray;\n"
                                                  "    color: white;\n"
                                                  "    padding: 10px 10px 10px 10px;\n"
                                                  "    background: brown;\n"
                                                  "    selection-background-color: black;\n"
                                                  "}\n"
                                                  "\n"
                                                  ""))
        self.AccountValue.setEditable(True)
        self.AccountValue.setObjectName(_fromUtf8("AccountValue"))
        self.AccountValue.addItem(_fromUtf8(""))
        self.AccountValue.addItem(_fromUtf8(""))
        self.lineEdit = QtGui.QLineEdit(self.itemMakerPage)
        self.lineEdit.setGeometry(QtCore.QRect(296, 367, 230, 30))
        font = QtGui.QFont()
        font.setPointSize(8)
        font.setBold(True)
        font.setItalic(False)
        font.setUnderline(False)
        font.setWeight(75)
        font.setStrikeOut(False)
        self.lineEdit.setFont(font)
        self.lineEdit.setCursor(QtGui.QCursor(QtCore.Qt.SplitHCursor))
        self.lineEdit.setStyleSheet(_fromUtf8("color: rgb(252, 252, 252);\n"
                                              "background: rgb(150, 150, 150);\n"
                                              "border: 2px solid #555;\n"
                                              "border-radius: 9px;\n"
                                              "opacity: 39;"))
        self.lineEdit.setObjectName(_fromUtf8("lineEdit"))
        self.ProcessAll_2 = QtGui.QPushButton(self.itemMakerPage)
        self.ProcessAll_2.setGeometry(QtCore.QRect(501, 367, 109, 30))
        font = QtGui.QFont()
        font.setPointSize(8)
        font.setBold(True)
        font.setWeight(75)
        font.setStyleStrategy(QtGui.QFont.PreferAntialias)
        self.ProcessAll_2.setFont(font)
        self.ProcessAll_2.setStyleSheet(_fromUtf8("QPushButton {\n"
                                                  "color: #333;\n"
                                                  "\n"
                                                  "border: 1px solid #555;\n"
                                                  "border-radius: 11px;\n"
                                                  "padding: 5px;\n"
                                                  "background: qradialgradient(cx: 0.3, cy: -0.4,\n"
                                                  "fx: 0.3, fy: -0.4,\n"
                                                  "radius: 1.35, stop: 0 #fff, stop: 1 #888);\n"
                                                  "min-width: 80px;\n"
                                                  "}\n"
                                                  "\n"
                                                  "\n"
                                                  "QPushButton:hover {\n"
                                                  "background: qradialgradient(cx: 0.3, cy: -0.4,\n"
                                                  "fx: 0.3, fy: -0.4,\n"
                                                  "radius: 1.35, stop: 0 #fff, stop: 1 #bbb);\n"
                                                  "}\n"
                                                  "\n"
                                                  "QPushButton:pressed {\n"
                                                  "background: qradialgradient(cx: 0.4, cy: -0.1,\n"
                                                  "fx: 0.4, fy: -0.1,\n"
                                                  "radius: 1.35, stop: 0 #fff, stop: 1 #ddd);\n"
                                                  "}"))
        self.ProcessAll_2.setObjectName(_fromUtf8("ProcessAll_2"))
        self.label_6 = QtGui.QLabel(self.itemMakerPage)
        self.label_6.setGeometry(QtCore.QRect(628, 16, 69, 47))
        self.label_6.setStyleSheet(_fromUtf8("background: transparent;\n"
                                             "opacity: 4%;"))
        self.label_6.setText(_fromUtf8(""))
        self.label_6.setPixmap(QtGui.QPixmap(_fromUtf8(":/coresec/im/csec.png")))
        self.label_6.setScaledContents(True)
        self.label_6.setObjectName(_fromUtf8("label_6"))
        self.BGIMaker.raise_()
        self.imtitle.raise_()
        self.vaultBG.raise_()
        self.vaultCat.raise_()
        self.MessageBG.raise_()
        self.Message.raise_()
        self.Progress.raise_()
        self.CancelNow.raise_()
        self.ProcessAll.raise_()
        self.ItemView.raise_()
        self.CatViewLabel.raise_()
        self.contentsLabel.raise_()
        self.CategoriesView.raise_()
        self.Option_Zen.raise_()
        self.Option_DSR.raise_()
        self.Option_Reflect.raise_()
        self.Option_DD.raise_()
        self.Option_Mana.raise_()
        self.Option_HP.raise_()
        self.Option_Yellow1.raise_()
        self.Option_Yellow2.raise_()
        self.seper.raise_()
        self.Option_380.raise_()
        self.OptionsLabelDown.raise_()
        self.plusLabel.raise_()
        self.Option_Options.raise_()
        self.Option_Plus.raise_()
        self.Options_OptionValue.raise_()
        self.Options_PlusValue.raise_()
        self.Option_Zen_2.raise_()
        self.AccountLabel.raise_()
        self.AccountValue.raise_()
        self.lineEdit.raise_()
        self.ProcessAll_2.raise_()
        self.label_6.raise_()
        self.bmuPager.addWidget(self.itemMakerPage)
        self.packageMakerPage = QtGui.QWidget()
        self.packageMakerPage.setObjectName(_fromUtf8("packageMakerPage"))
        self.pmtitle = QtGui.QLabel(self.packageMakerPage)
        self.pmtitle.setGeometry(QtCore.QRect(674, -1, 94, 16))
        self.pmtitle.setStyleSheet(_fromUtf8("color: rgb(255, 73, 12)"))
        self.pmtitle.setObjectName(_fromUtf8("pmtitle"))
        self.Package_List = QtGui.QListWidget(self.packageMakerPage)
        self.Package_List.setGeometry(QtCore.QRect(18, 72, 231, 314))
        self.Package_List.setStyleSheet(_fromUtf8("\n"
                                                  "color: #333;\n"
                                                  "\n"
                                                  "border: 2px solid #555;\n"
                                                  "border-radius: 11px;\n"
                                                  "padding: 5px;\n"
                                                  "background:rgb(151, 156, 133)\n"
                                                  "\n"
                                                  ""))
        self.Package_List.setObjectName(_fromUtf8("Package_List"))
        item = QtGui.QListWidgetItem()
        self.Package_List.addItem(item)
        self.Package_Choose = QtGui.QComboBox(self.packageMakerPage)
        self.Package_Choose.setGeometry(QtCore.QRect(19, 37, 231, 29))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Narkisim"))
        font.setPointSize(13)
        font.setBold(True)
        font.setWeight(75)
        font.setStyleStrategy(QtGui.QFont.PreferAntialias)
        self.Package_Choose.setFont(font)
        self.Package_Choose.setStyleSheet(_fromUtf8(" QComboBox {\n"
                                                    "     border: 1px solid gray;\n"
                                                    "     border-radius: 3px;\n"
                                                    "     padding: 1px 18px 1px 3px;\n"
                                                    "     min-width: 6em;\n"
                                                    "color:rgb(79, 79, 79);\n"
                                                    " }\n"
                                                    "\n"
                                                    " QComboBox:editable {\n"
                                                    "     background: white;\n"
                                                    " }\n"
                                                    "\n"
                                                    " QComboBox:!editable, QComboBox::drop-down:editable {\n"
                                                    "      background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
                                                    "                                  stop: 0 #E1E1E1, stop: 0.4 #DDDDDD,\n"
                                                    "                                  stop: 0.5 #D8D8D8, stop: 1.0 #D3D3D3);\n"
                                                    " }\n"
                                                    "\n"
                                                    " /* QComboBox gets the \"on\" state when the popup is open */\n"
                                                    " QComboBox:!editable:on, QComboBox::drop-down:editable:on {\n"
                                                    "     background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
                                                    "                                 stop: 0 #D3D3D3, stop: 0.4 #D8D8D8,\n"
                                                    "                                 stop: 0.5 #DDDDDD, stop: 1.0 #E1E1E1);\n"
                                                    " }\n"
                                                    "\n"
                                                    " QComboBox:on { /* shift the text when the popup opens */\n"
                                                    "     padding-top: 3px;\n"
                                                    "     padding-left: 4px;\n"
                                                    " }\n"
                                                    "\n"
                                                    " QComboBox::drop-down {\n"
                                                    "     subcontrol-origin: padding;\n"
                                                    "     subcontrol-position: top right;\n"
                                                    "     width: 15px;\n"
                                                    "     background: #555;\n"
                                                    "     border-left-width: 1px;\n"
                                                    "     border-left-color: darkgray;\n"
                                                    "     border-left-style: solid; /* just a single line */\n"
                                                    "     border-top-right-radius: 3px; /* same radius as the QComboBox */\n"
                                                    "     border-bottom-right-radius: 3px;\n"
                                                    " }\n"
                                                    "QComboBox QAbstractItemView {\n"
                                                    "    border: 2px solid darkgray;\n"
                                                    "    color: white;\n"
                                                    "    padding: 10px 10px 10px 10px;\n"
                                                    "    background: brown;\n"
                                                    "    selection-background-color: black;\n"
                                                    "}\n"
                                                    "\n"
                                                    ""))
        self.Package_Choose.setObjectName(_fromUtf8("Package_Choose"))
        self.Package_Choose.addItem(_fromUtf8(""))
        self.label_10 = QtGui.QLabel(self.packageMakerPage)
        self.label_10.setGeometry(QtCore.QRect(268, 11, 396, 16))
        font = QtGui.QFont()
        font.setPointSize(7)
        self.label_10.setFont(font)
        self.label_10.setStyleSheet(_fromUtf8("color: rgb(170, 170, 127)"))
        self.label_10.setObjectName(_fromUtf8("label_10"))
        self.ProcessPackage = QtGui.QPushButton(self.packageMakerPage)
        self.ProcessPackage.setGeometry(QtCore.QRect(278, 188, 304, 55))
        font = QtGui.QFont()
        font.setPointSize(11)
        font.setBold(True)
        font.setWeight(75)
        font.setStyleStrategy(QtGui.QFont.PreferAntialias)
        self.ProcessPackage.setFont(font)
        self.ProcessPackage.setStyleSheet(_fromUtf8("QPushButton {\n"
                                                    "color: #333;\n"
                                                    "\n"
                                                    "border: 1px solid #555;\n"
                                                    "border-radius: 11px;\n"
                                                    "padding: 5px;\n"
                                                    "background: qradialgradient(cx: 0.3, cy: -0.4,\n"
                                                    "fx: 0.3, fy: -0.4,\n"
                                                    "radius: 1.35, stop: 0 #fff, stop: 1 #888);\n"
                                                    "min-width: 80px;\n"
                                                    "}\n"
                                                    "\n"
                                                    "\n"
                                                    "QPushButton:hover {\n"
                                                    "background: qradialgradient(cx: 0.3, cy: -0.4,\n"
                                                    "fx: 0.3, fy: -0.4,\n"
                                                    "radius: 1.35, stop: 0 #fff, stop: 1 #bbb);\n"
                                                    "}\n"
                                                    "\n"
                                                    "QPushButton:pressed {\n"
                                                    "background: qradialgradient(cx: 0.4, cy: -0.1,\n"
                                                    "fx: 0.4, fy: -0.1,\n"
                                                    "radius: 1.35, stop: 0 #fff, stop: 1 #ddd);\n"
                                                    "}"))
        self.ProcessPackage.setObjectName(_fromUtf8("ProcessPackage"))
        self.PackageMessageIncludes = QtGui.QTextEdit(self.packageMakerPage)
        self.PackageMessageIncludes.setGeometry(QtCore.QRect(597, 36, 184, 349))
        self.PackageMessageIncludes.setStyleSheet(_fromUtf8("\n"
                                                            "color: #333;\n"
                                                            "\n"
                                                            "border: 2px solid #555;\n"
                                                            "border-radius: 11px;\n"
                                                            "padding: 5px;\n"
                                                            "background:rgb(151, 156, 133)\n"
                                                            "\n"
                                                            ""))
        self.PackageMessageIncludes.setObjectName(_fromUtf8("PackageMessageIncludes"))
        self.Package_UsernameList = QtGui.QLineEdit(self.packageMakerPage)
        self.Package_UsernameList.setGeometry(QtCore.QRect(277, 134, 305, 40))
        font = QtGui.QFont()
        font.setPointSize(10)
        font.setBold(False)
        font.setItalic(False)
        font.setUnderline(False)
        font.setWeight(50)
        font.setStrikeOut(False)
        self.Package_UsernameList.setFont(font)
        self.Package_UsernameList.setStyleSheet(_fromUtf8("color:rgb(20, 20, 20);\n"
                                                          "background: rgb(199, 199, 199);\n"
                                                          "border: 1px solid #555;\n"
                                                          "border-radius: 11px;\n"
                                                          "padding: 5px;\n"
                                                          "min-width: 80px;\n"
                                                          ""))
        self.Package_UsernameList.setObjectName(_fromUtf8("Package_UsernameList"))
        self.Package_EditIncludes = QtGui.QPushButton(self.packageMakerPage)
        self.Package_EditIncludes.setGeometry(QtCore.QRect(632, 325, 111, 55))
        font = QtGui.QFont()
        font.setPointSize(7)
        font.setBold(True)
        font.setWeight(75)
        font.setStyleStrategy(QtGui.QFont.PreferAntialias)
        self.Package_EditIncludes.setFont(font)
        self.Package_EditIncludes.setStyleSheet(_fromUtf8("QPushButton {\n"
                                                          "color: #333;\n"
                                                          "\n"
                                                          "border: 1px solid #555;\n"
                                                          "border-radius: 11px;\n"
                                                          "padding: 5px;\n"
                                                          "background: qradialgradient(cx: 0.3, cy: -0.4,\n"
                                                          "fx: 0.3, fy: -0.4,\n"
                                                          "radius: 1.35, stop: 0 #fff, stop: 1 #888);\n"
                                                          "min-width: 80px;\n"
                                                          "}\n"
                                                          "\n"
                                                          "\n"
                                                          "QPushButton:hover {\n"
                                                          "background: qradialgradient(cx: 0.3, cy: -0.4,\n"
                                                          "fx: 0.3, fy: -0.4,\n"
                                                          "radius: 1.35, stop: 0 #fff, stop: 1 #bbb);\n"
                                                          "}\n"
                                                          "\n"
                                                          "QPushButton:pressed {\n"
                                                          "background: qradialgradient(cx: 0.4, cy: -0.1,\n"
                                                          "fx: 0.4, fy: -0.1,\n"
                                                          "radius: 1.35, stop: 0 #fff, stop: 1 #ddd);\n"
                                                          "}"))
        self.Package_EditIncludes.setObjectName(_fromUtf8("Package_EditIncludes"))
        self.Package_ChooseClass = QtGui.QComboBox(self.packageMakerPage)
        self.Package_ChooseClass.setGeometry(QtCore.QRect(18, 5, 231, 29))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Narkisim"))
        font.setPointSize(13)
        font.setBold(True)
        font.setWeight(75)
        font.setStyleStrategy(QtGui.QFont.PreferAntialias)
        self.Package_ChooseClass.setFont(font)
        self.Package_ChooseClass.setStyleSheet(_fromUtf8(" QComboBox {\n"
                                                         "     border: 1px solid gray;\n"
                                                         "     border-radius: 3px;\n"
                                                         "     padding: 1px 18px 1px 3px;\n"
                                                         "     min-width: 6em;\n"
                                                         "color:rgb(79, 79, 79);\n"
                                                         " }\n"
                                                         "\n"
                                                         " QComboBox:editable {\n"
                                                         "     background: white;\n"
                                                         " }\n"
                                                         "\n"
                                                         " QComboBox:!editable, QComboBox::drop-down:editable {\n"
                                                         "      background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
                                                         "                                  stop: 0 #E1E1E1, stop: 0.4 #DDDDDD,\n"
                                                         "                                  stop: 0.5 #D8D8D8, stop: 1.0 #D3D3D3);\n"
                                                         " }\n"
                                                         "\n"
                                                         " /* QComboBox gets the \"on\" state when the popup is open */\n"
                                                         " QComboBox:!editable:on, QComboBox::drop-down:editable:on {\n"
                                                         "     background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
                                                         "                                 stop: 0 #D3D3D3, stop: 0.4 #D8D8D8,\n"
                                                         "                                 stop: 0.5 #DDDDDD, stop: 1.0 #E1E1E1);\n"
                                                         " }\n"
                                                         "\n"
                                                         " QComboBox:on { /* shift the text when the popup opens */\n"
                                                         "     padding-top: 3px;\n"
                                                         "     padding-left: 4px;\n"
                                                         " }\n"
                                                         "\n"
                                                         " QComboBox::drop-down {\n"
                                                         "     subcontrol-origin: padding;\n"
                                                         "     subcontrol-position: top right;\n"
                                                         "     width: 15px;\n"
                                                         "     background: #555;\n"
                                                         "     border-left-width: 1px;\n"
                                                         "     border-left-color: darkgray;\n"
                                                         "     border-left-style: solid; /* just a single line */\n"
                                                         "     border-top-right-radius: 3px; /* same radius as the QComboBox */\n"
                                                         "     border-bottom-right-radius: 3px;\n"
                                                         " }\n"
                                                         "QComboBox QAbstractItemView {\n"
                                                         "    border: 2px solid darkgray;\n"
                                                         "    color: white;\n"
                                                         "    padding: 10px 10px 10px 10px;\n"
                                                         "    background: brown;\n"
                                                         "    selection-background-color: black;\n"
                                                         "}\n"
                                                         "\n"
                                                         ""))
        self.Package_ChooseClass.setObjectName(_fromUtf8("Package_ChooseClass"))
        self.Package_ChooseClass.addItem(_fromUtf8(""))
        self.pmtitle.raise_()
        self.Package_List.raise_()
        self.Package_Choose.raise_()
        self.label_10.raise_()
        self.PackageMessageIncludes.raise_()
        self.ProcessPackage.raise_()
        self.Package_UsernameList.raise_()
        self.Package_EditIncludes.raise_()
        self.Package_ChooseClass.raise_()
        self.bmuPager.addWidget(self.packageMakerPage)
        self.accountManagerPage = QtGui.QWidget()
        self.accountManagerPage.setObjectName(_fromUtf8("accountManagerPage"))
        self.amtitle = QtGui.QLabel(self.accountManagerPage)
        self.amtitle.setGeometry(QtCore.QRect(676, -2, 95, 16))
        self.amtitle.setStyleSheet(_fromUtf8("color: rgb(255, 73, 12)"))
        self.amtitle.setObjectName(_fromUtf8("amtitle"))
        self.AccManagerTabber = QtGui.QTabWidget(self.accountManagerPage)
        self.AccManagerTabber.setGeometry(QtCore.QRect(25, 11, 757, 404))
        self.AccManagerTabber.setStyleSheet(
            _fromUtf8("QTabBar::tab { background: gray; color: rgb(50, 50, 50); padding: 10px; } \n"
                      "      QTabBar::tab:selected { background: lightgray; color:rgb(162, 68, 5)} \n"
                      "      QTabWidget::pane { border: 0;color:rgb(81, 81, 81) } \n"
                      "      QWidget { background: lightgray; }"))
        self.AccManagerTabber.setTabShape(QtGui.QTabWidget.Rounded)
        self.AccManagerTabber.setElideMode(QtCore.Qt.ElideRight)
        self.AccManagerTabber.setUsesScrollButtons(False)
        self.AccManagerTabber.setTabsClosable(False)
        self.AccManagerTabber.setMovable(True)
        self.AccManagerTabber.setObjectName(_fromUtf8("AccManagerTabber"))
        self.AccManager_Info = QtGui.QWidget()
        self.AccManager_Info.setObjectName(_fromUtf8("AccManager_Info"))
        self.AccMan_UsersList = QtGui.QListWidget(self.AccManager_Info)
        self.AccMan_UsersList.setGeometry(QtCore.QRect(14, 50, 165, 316))
        self.AccMan_UsersList.setObjectName(_fromUtf8("AccMan_UsersList"))
        item = QtGui.QListWidgetItem()
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Miriam"))
        font.setBold(True)
        font.setWeight(75)
        item.setFont(font)
        self.AccMan_UsersList.addItem(item)
        item = QtGui.QListWidgetItem()
        self.AccMan_UsersList.addItem(item)
        item = QtGui.QListWidgetItem()
        self.AccMan_UsersList.addItem(item)
        item = QtGui.QListWidgetItem()
        self.AccMan_UsersList.addItem(item)
        item = QtGui.QListWidgetItem()
        self.AccMan_UsersList.addItem(item)
        item = QtGui.QListWidgetItem()
        self.AccMan_UsersList.addItem(item)
        item = QtGui.QListWidgetItem()
        self.AccMan_UsersList.addItem(item)
        item = QtGui.QListWidgetItem()
        self.AccMan_UsersList.addItem(item)
        item = QtGui.QListWidgetItem()
        self.AccMan_UsersList.addItem(item)
        item = QtGui.QListWidgetItem()
        self.AccMan_UsersList.addItem(item)
        item = QtGui.QListWidgetItem()
        self.AccMan_UsersList.addItem(item)
        item = QtGui.QListWidgetItem()
        self.AccMan_UsersList.addItem(item)
        item = QtGui.QListWidgetItem()
        self.AccMan_UsersList.addItem(item)
        item = QtGui.QListWidgetItem()
        self.AccMan_UsersList.addItem(item)
        item = QtGui.QListWidgetItem()
        self.AccMan_UsersList.addItem(item)
        item = QtGui.QListWidgetItem()
        self.AccMan_UsersList.addItem(item)
        item = QtGui.QListWidgetItem()
        self.AccMan_UsersList.addItem(item)
        item = QtGui.QListWidgetItem()
        self.AccMan_UsersList.addItem(item)
        item = QtGui.QListWidgetItem()
        self.AccMan_UsersList.addItem(item)
        item = QtGui.QListWidgetItem()
        self.AccMan_UsersList.addItem(item)
        self.Account_GSeperator = QtGui.QGroupBox(self.AccManager_Info)
        self.Account_GSeperator.setGeometry(QtCore.QRect(188, 9, 255, 356))
        self.Account_GSeperator.setStyleSheet(_fromUtf8("QGroupBox {\n"
                                                        "\n"
                                                        " border: 2px solid gray;\n"
                                                        " border-radius: 5px;\n"
                                                        " margin-top: 1ex; /* leave space at the top for the title */\n"
                                                        "}"))
        self.Account_GSeperator.setObjectName(_fromUtf8("Account_GSeperator"))
        self.accman_accin_label = QtGui.QLabel(self.Account_GSeperator)
        self.accman_accin_label.setGeometry(QtCore.QRect(22, 34, 71, 16))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.accman_accin_label.setFont(font)
        self.accman_accin_label.setObjectName(_fromUtf8("accman_accin_label"))
        self.AccMan_info_username = QtGui.QLabel(self.Account_GSeperator)
        self.AccMan_info_username.setGeometry(QtCore.QRect(116, 34, 122, 16))
        font = QtGui.QFont()
        font.setPointSize(10)
        font.setUnderline(True)
        self.AccMan_info_username.setFont(font)
        self.AccMan_info_username.setObjectName(_fromUtf8("AccMan_info_username"))
        self.accman_accin_label_2 = QtGui.QLabel(self.Account_GSeperator)
        self.accman_accin_label_2.setGeometry(QtCore.QRect(22, 60, 71, 16))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.accman_accin_label_2.setFont(font)
        self.accman_accin_label_2.setObjectName(_fromUtf8("accman_accin_label_2"))
        self.AccMan_info_password = QtGui.QLabel(self.Account_GSeperator)
        self.AccMan_info_password.setGeometry(QtCore.QRect(116, 60, 128, 16))
        font = QtGui.QFont()
        font.setPointSize(10)
        font.setUnderline(True)
        self.AccMan_info_password.setFont(font)
        self.AccMan_info_password.setObjectName(_fromUtf8("AccMan_info_password"))
        self.accman_accin_label_4 = QtGui.QLabel(self.Account_GSeperator)
        self.accman_accin_label_4.setGeometry(QtCore.QRect(22, 85, 71, 16))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.accman_accin_label_4.setFont(font)
        self.accman_accin_label_4.setObjectName(_fromUtf8("accman_accin_label_4"))
        self.AccMan_info_email = QtGui.QLabel(self.Account_GSeperator)
        self.AccMan_info_email.setGeometry(QtCore.QRect(116, 85, 128, 16))
        font = QtGui.QFont()
        font.setPointSize(10)
        font.setUnderline(True)
        self.AccMan_info_email.setFont(font)
        self.AccMan_info_email.setObjectName(_fromUtf8("AccMan_info_email"))
        self.accman_accin_label_5 = QtGui.QLabel(self.Account_GSeperator)
        self.accman_accin_label_5.setGeometry(QtCore.QRect(22, 110, 71, 16))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.accman_accin_label_5.setFont(font)
        self.accman_accin_label_5.setObjectName(_fromUtf8("accman_accin_label_5"))
        self.AccMan_info_joindate = QtGui.QLabel(self.Account_GSeperator)
        self.AccMan_info_joindate.setGeometry(QtCore.QRect(116, 110, 131, 16))
        font = QtGui.QFont()
        font.setPointSize(10)
        font.setUnderline(True)
        self.AccMan_info_joindate.setFont(font)
        self.AccMan_info_joindate.setObjectName(_fromUtf8("AccMan_info_joindate"))
        self.accman_accin_label_11 = QtGui.QLabel(self.Account_GSeperator)
        self.accman_accin_label_11.setGeometry(QtCore.QRect(22, 137, 71, 16))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.accman_accin_label_11.setFont(font)
        self.accman_accin_label_11.setObjectName(_fromUtf8("accman_accin_label_11"))
        self.AccMan_info_secanswer = QtGui.QLabel(self.Account_GSeperator)
        self.AccMan_info_secanswer.setGeometry(QtCore.QRect(116, 137, 135, 16))
        font = QtGui.QFont()
        font.setPointSize(10)
        font.setUnderline(True)
        self.AccMan_info_secanswer.setFont(font)
        self.AccMan_info_secanswer.setObjectName(_fromUtf8("AccMan_info_secanswer"))
        self.accman_accin_label_13 = QtGui.QLabel(self.Account_GSeperator)
        self.accman_accin_label_13.setGeometry(QtCore.QRect(22, 163, 71, 16))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.accman_accin_label_13.setFont(font)
        self.accman_accin_label_13.setObjectName(_fromUtf8("accman_accin_label_13"))
        self.AccMan_info_vipmoney = QtGui.QLabel(self.Account_GSeperator)
        self.AccMan_info_vipmoney.setGeometry(QtCore.QRect(116, 163, 122, 16))
        font = QtGui.QFont()
        font.setPointSize(10)
        font.setUnderline(True)
        self.AccMan_info_vipmoney.setFont(font)
        self.AccMan_info_vipmoney.setObjectName(_fromUtf8("AccMan_info_vipmoney"))
        self.accman_accin_label_15 = QtGui.QLabel(self.Account_GSeperator)
        self.accman_accin_label_15.setGeometry(QtCore.QRect(24, 330, 71, 16))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.accman_accin_label_15.setFont(font)
        self.accman_accin_label_15.setObjectName(_fromUtf8("accman_accin_label_15"))
        self.AccMan_info_accserial = QtGui.QLabel(self.Account_GSeperator)
        self.AccMan_info_accserial.setGeometry(QtCore.QRect(124, 330, 80, 16))
        font = QtGui.QFont()
        font.setPointSize(10)
        font.setUnderline(True)
        self.AccMan_info_accserial.setFont(font)
        self.AccMan_info_accserial.setObjectName(_fromUtf8("AccMan_info_accserial"))
        self.accman_accin_label_17 = QtGui.QLabel(self.Account_GSeperator)
        self.accman_accin_label_17.setGeometry(QtCore.QRect(23, 188, 71, 16))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.accman_accin_label_17.setFont(font)
        self.accman_accin_label_17.setObjectName(_fromUtf8("accman_accin_label_17"))
        self.AccMan_info_country = QtGui.QLabel(self.Account_GSeperator)
        self.AccMan_info_country.setGeometry(QtCore.QRect(117, 188, 122, 16))
        font = QtGui.QFont()
        font.setPointSize(10)
        font.setUnderline(True)
        self.AccMan_info_country.setFont(font)
        self.AccMan_info_country.setObjectName(_fromUtf8("AccMan_info_country"))
        self.accman_accin_label_19 = QtGui.QLabel(self.Account_GSeperator)
        self.accman_accin_label_19.setGeometry(QtCore.QRect(23, 217, 71, 16))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.accman_accin_label_19.setFont(font)
        self.accman_accin_label_19.setObjectName(_fromUtf8("accman_accin_label_19"))
        self.AccMan_info_charlen = QtGui.QLabel(self.Account_GSeperator)
        self.AccMan_info_charlen.setGeometry(QtCore.QRect(117, 217, 128, 16))
        font = QtGui.QFont()
        font.setPointSize(10)
        font.setUnderline(True)
        self.AccMan_info_charlen.setFont(font)
        self.AccMan_info_charlen.setObjectName(_fromUtf8("AccMan_info_charlen"))
        self.accman_accin_label_21 = QtGui.QLabel(self.Account_GSeperator)
        self.accman_accin_label_21.setGeometry(QtCore.QRect(24, 242, 71, 16))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.accman_accin_label_21.setFont(font)
        self.accman_accin_label_21.setObjectName(_fromUtf8("accman_accin_label_21"))
        self.AccMan_info_gender = QtGui.QLabel(self.Account_GSeperator)
        self.AccMan_info_gender.setGeometry(QtCore.QRect(118, 242, 129, 16))
        font = QtGui.QFont()
        font.setPointSize(10)
        font.setUnderline(True)
        self.AccMan_info_gender.setFont(font)
        self.AccMan_info_gender.setObjectName(_fromUtf8("AccMan_info_gender"))
        self.charGSeperator = QtGui.QGroupBox(self.AccManager_Info)
        self.charGSeperator.setGeometry(QtCore.QRect(447, 9, 299, 355))
        self.charGSeperator.setStyleSheet(_fromUtf8("QGroupBox {\n"
                                                    "\n"
                                                    " border: 2px solid gray;\n"
                                                    " border-radius: 5px;\n"
                                                    " margin-top: 1ex; /* leave space at the top for the title */\n"
                                                    "}"))
        self.charGSeperator.setObjectName(_fromUtf8("charGSeperator"))
        self.AccMan_ChooseChar = QtGui.QComboBox(self.charGSeperator)
        self.AccMan_ChooseChar.setGeometry(QtCore.QRect(132, 12, 163, 28))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Narkisim"))
        font.setPointSize(10)
        font.setBold(True)
        font.setWeight(75)
        font.setStyleStrategy(QtGui.QFont.PreferAntialias)
        self.AccMan_ChooseChar.setFont(font)
        self.AccMan_ChooseChar.setStyleSheet(_fromUtf8(" QComboBox {\n"
                                                       "     border: 1px solid gray;\n"
                                                       "     border-radius: 3px;\n"
                                                       "     padding: 1px 18px 1px 3px;\n"
                                                       "     min-width: 6em;\n"
                                                       "color:rgb(79, 79, 79);\n"
                                                       " }\n"
                                                       "\n"
                                                       " QComboBox:editable {\n"
                                                       "     background: white;\n"
                                                       " }\n"
                                                       "\n"
                                                       " QComboBox:!editable, QComboBox::drop-down:editable {\n"
                                                       "      background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
                                                       "                                  stop: 0 #E1E1E1, stop: 0.4 #DDDDDD,\n"
                                                       "                                  stop: 0.5 #D8D8D8, stop: 1.0 #D3D3D3);\n"
                                                       " }\n"
                                                       "\n"
                                                       " /* QComboBox gets the \"on\" state when the popup is open */\n"
                                                       " QComboBox:!editable:on, QComboBox::drop-down:editable:on {\n"
                                                       "     background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
                                                       "                                 stop: 0 #D3D3D3, stop: 0.4 #D8D8D8,\n"
                                                       "                                 stop: 0.5 #DDDDDD, stop: 1.0 #E1E1E1);\n"
                                                       " }\n"
                                                       "\n"
                                                       " QComboBox:on { /* shift the text when the popup opens */\n"
                                                       "     padding-top: 3px;\n"
                                                       "     padding-left: 4px;\n"
                                                       " }\n"
                                                       "\n"
                                                       " QComboBox::drop-down {\n"
                                                       "     subcontrol-origin: padding;\n"
                                                       "     subcontrol-position: top right;\n"
                                                       "     width: 15px;\n"
                                                       "     background: #555;\n"
                                                       "     border-left-width: 1px;\n"
                                                       "     border-left-color: darkgray;\n"
                                                       "     border-left-style: solid; /* just a single line */\n"
                                                       "     border-top-right-radius: 3px; /* same radius as the QComboBox */\n"
                                                       "     border-bottom-right-radius: 3px;\n"
                                                       " }\n"
                                                       "QComboBox QAbstractItemView {\n"
                                                       "    border: 2px solid darkgray;\n"
                                                       "    color: white;\n"
                                                       "    padding: 10px 10px 10px 10px;\n"
                                                       "    background: brown;\n"
                                                       "    selection-background-color: black;\n"
                                                       "}\n"
                                                       "\n"
                                                       ""))
        self.AccMan_ChooseChar.setObjectName(_fromUtf8("AccMan_ChooseChar"))
        self.AccMan_ChooseChar.addItem(_fromUtf8(""))
        self.AccMan_ChooseChar.addItem(_fromUtf8(""))
        self.AccMan_charDetails_tableWidget = QtGui.QTableWidget(self.charGSeperator)
        self.AccMan_charDetails_tableWidget.setGeometry(QtCore.QRect(1, 44, 298, 310))
        font = QtGui.QFont()
        font.setPointSize(12)
        self.AccMan_charDetails_tableWidget.setFont(font)
        self.AccMan_charDetails_tableWidget.setStyleSheet(_fromUtf8("QWidget {\n"
                                                                    "    background-color: #333333;\n"
                                                                    "    color: #fffff8;\n"
                                                                    "}\n"
                                                                    "\n"
                                                                    "QHeaderView::section {\n"
                                                                    "    background-color: #646464;\n"
                                                                    "    padding: 4px;\n"
                                                                    "    border: 0px solid #fffff8;\n"
                                                                    "    font-size: 8pt;\n"
                                                                    "}\n"
                                                                    "\n"
                                                                    "QTableWidget {\n"
                                                                    "    gridline-color: #fffff8;\n"
                                                                    "    font-size: 12pt;\n"
                                                                    "}\n"
                                                                    "\n"
                                                                    "QTableWidget QTableCornerButton::section {\n"
                                                                    "    background-color: #646464;\n"
                                                                    "    border: 0px solid #fffff8;\n"
                                                                    "}"))
        self.AccMan_charDetails_tableWidget.setFrameShadow(QtGui.QFrame.Plain)
        self.AccMan_charDetails_tableWidget.setLineWidth(0)
        self.AccMan_charDetails_tableWidget.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.AccMan_charDetails_tableWidget.setAutoScrollMargin(40)
        self.AccMan_charDetails_tableWidget.setProperty("showDropIndicator", True)
        self.AccMan_charDetails_tableWidget.setDragDropOverwriteMode(False)
        self.AccMan_charDetails_tableWidget.setSelectionBehavior(QtGui.QAbstractItemView.SelectItems)
        self.AccMan_charDetails_tableWidget.setTextElideMode(QtCore.Qt.ElideRight)
        self.AccMan_charDetails_tableWidget.setVerticalScrollMode(QtGui.QAbstractItemView.ScrollPerItem)
        self.AccMan_charDetails_tableWidget.setShowGrid(True)
        self.AccMan_charDetails_tableWidget.setGridStyle(QtCore.Qt.NoPen)
        self.AccMan_charDetails_tableWidget.setWordWrap(True)
        self.AccMan_charDetails_tableWidget.setCornerButtonEnabled(False)
        self.AccMan_charDetails_tableWidget.setObjectName(_fromUtf8("AccMan_charDetails_tableWidget"))
        self.AccMan_charDetails_tableWidget.setColumnCount(1)
        self.AccMan_charDetails_tableWidget.setRowCount(13)
        item = QtGui.QTableWidgetItem()
        self.AccMan_charDetails_tableWidget.setVerticalHeaderItem(0, item)
        item = QtGui.QTableWidgetItem()
        self.AccMan_charDetails_tableWidget.setVerticalHeaderItem(1, item)
        item = QtGui.QTableWidgetItem()
        self.AccMan_charDetails_tableWidget.setVerticalHeaderItem(2, item)
        item = QtGui.QTableWidgetItem()
        self.AccMan_charDetails_tableWidget.setVerticalHeaderItem(3, item)
        item = QtGui.QTableWidgetItem()
        self.AccMan_charDetails_tableWidget.setVerticalHeaderItem(4, item)
        item = QtGui.QTableWidgetItem()
        self.AccMan_charDetails_tableWidget.setVerticalHeaderItem(5, item)
        item = QtGui.QTableWidgetItem()
        self.AccMan_charDetails_tableWidget.setVerticalHeaderItem(6, item)
        item = QtGui.QTableWidgetItem()
        self.AccMan_charDetails_tableWidget.setVerticalHeaderItem(7, item)
        item = QtGui.QTableWidgetItem()
        self.AccMan_charDetails_tableWidget.setVerticalHeaderItem(8, item)
        item = QtGui.QTableWidgetItem()
        self.AccMan_charDetails_tableWidget.setVerticalHeaderItem(9, item)
        item = QtGui.QTableWidgetItem()
        self.AccMan_charDetails_tableWidget.setVerticalHeaderItem(10, item)
        item = QtGui.QTableWidgetItem()
        self.AccMan_charDetails_tableWidget.setVerticalHeaderItem(11, item)
        item = QtGui.QTableWidgetItem()
        self.AccMan_charDetails_tableWidget.setVerticalHeaderItem(12, item)
        item = QtGui.QTableWidgetItem()
        self.AccMan_charDetails_tableWidget.setHorizontalHeaderItem(0, item)
        item = QtGui.QTableWidgetItem()
        self.AccMan_charDetails_tableWidget.setItem(0, 0, item)
        item = QtGui.QTableWidgetItem()
        self.AccMan_charDetails_tableWidget.setItem(1, 0, item)
        item = QtGui.QTableWidgetItem()
        self.AccMan_charDetails_tableWidget.setItem(2, 0, item)
        item = QtGui.QTableWidgetItem()
        self.AccMan_charDetails_tableWidget.setItem(3, 0, item)
        item = QtGui.QTableWidgetItem()
        self.AccMan_charDetails_tableWidget.setItem(4, 0, item)
        item = QtGui.QTableWidgetItem()
        self.AccMan_charDetails_tableWidget.setItem(5, 0, item)
        item = QtGui.QTableWidgetItem()
        self.AccMan_charDetails_tableWidget.setItem(6, 0, item)
        item = QtGui.QTableWidgetItem()
        self.AccMan_charDetails_tableWidget.setItem(7, 0, item)
        item = QtGui.QTableWidgetItem()
        self.AccMan_charDetails_tableWidget.setItem(8, 0, item)
        item = QtGui.QTableWidgetItem()
        self.AccMan_charDetails_tableWidget.setItem(9, 0, item)
        item = QtGui.QTableWidgetItem()
        self.AccMan_charDetails_tableWidget.setItem(10, 0, item)
        item = QtGui.QTableWidgetItem()
        self.AccMan_charDetails_tableWidget.setItem(11, 0, item)
        item = QtGui.QTableWidgetItem()
        self.AccMan_charDetails_tableWidget.setItem(12, 0, item)
        self.AccMan_charDetails_tableWidget.horizontalHeader().setVisible(False)
        self.AccMan_charDetails_tableWidget.horizontalHeader().setCascadingSectionResizes(False)
        self.AccMan_charDetails_tableWidget.horizontalHeader().setHighlightSections(False)
        self.AccMan_charDetails_tableWidget.horizontalHeader().setMinimumSectionSize(40)
        self.AccMan_charDetails_tableWidget.horizontalHeader().setSortIndicatorShown(False)
        self.AccMan_charDetails_tableWidget.horizontalHeader().setStretchLastSection(True)
        self.AccMan_charDetails_tableWidget.verticalHeader().setCascadingSectionResizes(True)
        self.AccMan_charDetails_tableWidget.verticalHeader().setMinimumSectionSize(40)
        self.AccMan_ChooseAcc_EditChar_2 = QtGui.QComboBox(self.AccManager_Info)
        self.AccMan_ChooseAcc_EditChar_2.setGeometry(QtCore.QRect(14, 14, 165, 31))
        self.AccMan_ChooseAcc_EditChar_2.setStyleSheet(_fromUtf8(" QComboBox {\n"
                                                                 "     border: 1px solid gray;\n"
                                                                 "     border-radius: 3px;\n"
                                                                 "     padding: 1px 18px 1px 3px;\n"
                                                                 "     min-width: 6em;\n"
                                                                 "color:rgb(79, 79, 79);\n"
                                                                 " }\n"
                                                                 "\n"
                                                                 " QComboBox:editable {\n"
                                                                 "     background: white;\n"
                                                                 " }\n"
                                                                 "\n"
                                                                 " QComboBox:!editable, QComboBox::drop-down:editable {\n"
                                                                 "      background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
                                                                 "                                  stop: 0 #E1E1E1, stop: 0.4 #DDDDDD,\n"
                                                                 "                                  stop: 0.5 #D8D8D8, stop: 1.0 #D3D3D3);\n"
                                                                 " }\n"
                                                                 "\n"
                                                                 " /* QComboBox gets the \"on\" state when the popup is open */\n"
                                                                 " QComboBox:!editable:on, QComboBox::drop-down:editable:on {\n"
                                                                 "     background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
                                                                 "                                 stop: 0 #D3D3D3, stop: 0.4 #D8D8D8,\n"
                                                                 "                                 stop: 0.5 #DDDDDD, stop: 1.0 #E1E1E1);\n"
                                                                 " }\n"
                                                                 "\n"
                                                                 " QComboBox:on { /* shift the text when the popup opens */\n"
                                                                 "     padding-top: 3px;\n"
                                                                 "     padding-left: 4px;\n"
                                                                 " }\n"
                                                                 "\n"
                                                                 " QComboBox::drop-down {\n"
                                                                 "     subcontrol-origin: padding;\n"
                                                                 "     subcontrol-position: top right;\n"
                                                                 "     width: 15px;\n"
                                                                 "     background: #555;\n"
                                                                 "     border-left-width: 1px;\n"
                                                                 "     border-left-color: darkgray;\n"
                                                                 "     border-left-style: solid; /* just a single line */\n"
                                                                 "     border-top-right-radius: 3px; /* same radius as the QComboBox */\n"
                                                                 "     border-bottom-right-radius: 3px;\n"
                                                                 " }\n"
                                                                 "QComboBox QAbstractItemView {\n"
                                                                 "    border: 2px solid darkgray;\n"
                                                                 "    color: white;\n"
                                                                 "    padding: 10px 10px 10px 10px;\n"
                                                                 "    background: brown;\n"
                                                                 "    selection-background-color: black;\n"
                                                                 "}\n"
                                                                 "\n"
                                                                 ""))
        self.AccMan_ChooseAcc_EditChar_2.setEditable(True)
        self.AccMan_ChooseAcc_EditChar_2.setObjectName(_fromUtf8("AccMan_ChooseAcc_EditChar_2"))
        self.AccMan_ChooseAcc_EditChar_2.addItem(_fromUtf8(""))
        self.AccMan_ChooseAcc_EditChar_2.addItem(_fromUtf8(""))
        self.AccMan_ChooseAcc_EditChar_2.addItem(_fromUtf8(""))
        self.AccManagerTabber.addTab(self.AccManager_Info, _fromUtf8(""))
        self.AccManager_EditCharacter = QtGui.QWidget()
        self.AccManager_EditCharacter.setObjectName(_fromUtf8("AccManager_EditCharacter"))
        self.AccMan_EditChar_Choose = QtGui.QComboBox(self.AccManager_EditCharacter)
        self.AccMan_EditChar_Choose.setGeometry(QtCore.QRect(181, 26, 157, 31))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Narkisim"))
        font.setPointSize(13)
        font.setBold(True)
        font.setWeight(75)
        font.setStyleStrategy(QtGui.QFont.PreferAntialias)
        self.AccMan_EditChar_Choose.setFont(font)
        self.AccMan_EditChar_Choose.setStyleSheet(_fromUtf8(" QComboBox {\n"
                                                            "     border: 1px solid gray;\n"
                                                            "     border-radius: 3px;\n"
                                                            "     padding: 1px 18px 1px 3px;\n"
                                                            "     min-width: 6em;\n"
                                                            "color:rgb(79, 79, 79);\n"
                                                            " }\n"
                                                            "\n"
                                                            " QComboBox:editable {\n"
                                                            "     background: white;\n"
                                                            " }\n"
                                                            "\n"
                                                            " QComboBox:!editable, QComboBox::drop-down:editable {\n"
                                                            "      background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
                                                            "                                  stop: 0 #E1E1E1, stop: 0.4 #DDDDDD,\n"
                                                            "                                  stop: 0.5 #D8D8D8, stop: 1.0 #D3D3D3);\n"
                                                            " }\n"
                                                            "\n"
                                                            " /* QComboBox gets the \"on\" state when the popup is open */\n"
                                                            " QComboBox:!editable:on, QComboBox::drop-down:editable:on {\n"
                                                            "     background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
                                                            "                                 stop: 0 #D3D3D3, stop: 0.4 #D8D8D8,\n"
                                                            "                                 stop: 0.5 #DDDDDD, stop: 1.0 #E1E1E1);\n"
                                                            " }\n"
                                                            "\n"
                                                            " QComboBox:on { /* shift the text when the popup opens */\n"
                                                            "     padding-top: 3px;\n"
                                                            "     padding-left: 4px;\n"
                                                            " }\n"
                                                            "\n"
                                                            " QComboBox::drop-down {\n"
                                                            "     subcontrol-origin: padding;\n"
                                                            "     subcontrol-position: top right;\n"
                                                            "     width: 15px;\n"
                                                            "     background: #555;\n"
                                                            "     border-left-width: 1px;\n"
                                                            "     border-left-color: darkgray;\n"
                                                            "     border-left-style: solid; /* just a single line */\n"
                                                            "     border-top-right-radius: 3px; /* same radius as the QComboBox */\n"
                                                            "     border-bottom-right-radius: 3px;\n"
                                                            " }\n"
                                                            "QComboBox QAbstractItemView {\n"
                                                            "    border: 2px solid darkgray;\n"
                                                            "background: brown;\n"
                                                            "    color: white;\n"
                                                            "    selection-background-color: grey;\n"
                                                            "}\n"
                                                            "\n"
                                                            ""))
        self.AccMan_EditChar_Choose.setObjectName(_fromUtf8("AccMan_EditChar_Choose"))
        self.AccMan_EditChar_Choose.addItem(_fromUtf8(""))
        self.AccMan_EditChar_Choose.addItem(_fromUtf8(""))
        self.AccMan_ChooseAcc_EditChar = QtGui.QComboBox(self.AccManager_EditCharacter)
        self.AccMan_ChooseAcc_EditChar.setGeometry(QtCore.QRect(17, 26, 162, 31))
        self.AccMan_ChooseAcc_EditChar.setStyleSheet(_fromUtf8(" QComboBox {\n"
                                                               "     border: 1px solid gray;\n"
                                                               "     border-radius: 3px;\n"
                                                               "     padding: 1px 18px 1px 3px;\n"
                                                               "     min-width: 6em;\n"
                                                               "color:rgb(79, 79, 79);\n"
                                                               " }\n"
                                                               "\n"
                                                               " QComboBox:editable {\n"
                                                               "     background: white;\n"
                                                               " }\n"
                                                               "\n"
                                                               " QComboBox:!editable, QComboBox::drop-down:editable {\n"
                                                               "      background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
                                                               "                                  stop: 0 #E1E1E1, stop: 0.4 #DDDDDD,\n"
                                                               "                                  stop: 0.5 #D8D8D8, stop: 1.0 #D3D3D3);\n"
                                                               " }\n"
                                                               "\n"
                                                               " /* QComboBox gets the \"on\" state when the popup is open */\n"
                                                               " QComboBox:!editable:on, QComboBox::drop-down:editable:on {\n"
                                                               "     background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
                                                               "                                 stop: 0 #D3D3D3, stop: 0.4 #D8D8D8,\n"
                                                               "                                 stop: 0.5 #DDDDDD, stop: 1.0 #E1E1E1);\n"
                                                               " }\n"
                                                               "\n"
                                                               " QComboBox:on { /* shift the text when the popup opens */\n"
                                                               "     padding-top: 3px;\n"
                                                               "     padding-left: 4px;\n"
                                                               " }\n"
                                                               "\n"
                                                               " QComboBox::drop-down {\n"
                                                               "     subcontrol-origin: padding;\n"
                                                               "     subcontrol-position: top right;\n"
                                                               "     width: 15px;\n"
                                                               "     background: #555;\n"
                                                               "     border-left-width: 1px;\n"
                                                               "     border-left-color: darkgray;\n"
                                                               "     border-left-style: solid; /* just a single line */\n"
                                                               "     border-top-right-radius: 3px; /* same radius as the QComboBox */\n"
                                                               "     border-bottom-right-radius: 3px;\n"
                                                               " }\n"
                                                               "QComboBox QAbstractItemView {\n"
                                                               "    border: 2px solid darkgray;\n"
                                                               "    color: white;\n"
                                                               "    padding: 10px 10px 10px 10px;\n"
                                                               "    background: brown;\n"
                                                               "    selection-background-color: black;\n"
                                                               "}\n"
                                                               "\n"
                                                               ""))
        self.AccMan_ChooseAcc_EditChar.setEditable(True)
        self.AccMan_ChooseAcc_EditChar.setObjectName(_fromUtf8("AccMan_ChooseAcc_EditChar"))
        self.AccMan_ChooseAcc_EditChar.addItem(_fromUtf8(""))
        self.AccMan_ChooseAcc_EditChar.addItem(_fromUtf8(""))
        self.AccMan_EditChar_SaveAll = QtGui.QPushButton(self.AccManager_EditCharacter)
        self.AccMan_EditChar_SaveAll.setGeometry(QtCore.QRect(578, 323, 166, 40))
        font = QtGui.QFont()
        font.setPointSize(11)
        font.setBold(True)
        font.setWeight(75)
        font.setStyleStrategy(QtGui.QFont.PreferAntialias)
        self.AccMan_EditChar_SaveAll.setFont(font)
        self.AccMan_EditChar_SaveAll.setStyleSheet(_fromUtf8("QPushButton {\n"
                                                             "color: #333;\n"
                                                             "\n"
                                                             "border: 1px solid #555;\n"
                                                             "border-radius: 11px;\n"
                                                             "padding: 5px;\n"
                                                             "background: qradialgradient(cx: 0.3, cy: -0.4,\n"
                                                             "fx: 0.3, fy: -0.4,\n"
                                                             "radius: 1.35, stop: 0 #fff, stop: 1 #888);\n"
                                                             "min-width: 80px;\n"
                                                             "}\n"
                                                             "\n"
                                                             "\n"
                                                             "QPushButton:hover {\n"
                                                             "background: qradialgradient(cx: 0.3, cy: -0.4,\n"
                                                             "fx: 0.3, fy: -0.4,\n"
                                                             "radius: 1.35, stop: 0 #fff, stop: 1 #bbb);\n"
                                                             "}\n"
                                                             "\n"
                                                             "QPushButton:pressed {\n"
                                                             "background: qradialgradient(cx: 0.4, cy: -0.1,\n"
                                                             "fx: 0.4, fy: -0.1,\n"
                                                             "radius: 1.35, stop: 0 #fff, stop: 1 #ddd);\n"
                                                             "}"))
        self.AccMan_EditChar_SaveAll.setObjectName(_fromUtf8("AccMan_EditChar_SaveAll"))
        self.AccMan_EditChar_GSepCharStats = QtGui.QGroupBox(self.AccManager_EditCharacter)
        self.AccMan_EditChar_GSepCharStats.setGeometry(QtCore.QRect(17, 65, 209, 297))
        self.AccMan_EditChar_GSepCharStats.setStyleSheet(_fromUtf8("QGroupBox {\n"
                                                                   "\n"
                                                                   " border: 2px solid gray;\n"
                                                                   " border-radius: 5px;\n"
                                                                   " margin-top: 1ex; /* leave space at the top for the title */\n"
                                                                   "}"))
        self.AccMan_EditChar_GSepCharStats.setObjectName(_fromUtf8("AccMan_EditChar_GSepCharStats"))
        self.AccMan_Edit_Account = QtGui.QTableWidget(self.AccMan_EditChar_GSepCharStats)
        self.AccMan_Edit_Account.setGeometry(QtCore.QRect(1, 16, 209, 280))
        font = QtGui.QFont()
        font.setPointSize(12)
        self.AccMan_Edit_Account.setFont(font)
        self.AccMan_Edit_Account.setStyleSheet(_fromUtf8("QWidget {\n"
                                                         "    background-color: #333333;\n"
                                                         "    color: #fffff8;\n"
                                                         "}\n"
                                                         "\n"
                                                         "QHeaderView::section {\n"
                                                         "    background-color: #646464;\n"
                                                         "    padding: 4px;\n"
                                                         "    border: 0px solid #fffff8;\n"
                                                         "    font-size: 8pt;\n"
                                                         "}\n"
                                                         "\n"
                                                         "QTableWidget {\n"
                                                         "    gridline-color: #fffff8;\n"
                                                         "    font-size: 12pt;\n"
                                                         "}\n"
                                                         "\n"
                                                         "QTableWidget QTableCornerButton::section {\n"
                                                         "    background-color: #646464;\n"
                                                         "    border: 0px solid #fffff8;\n"
                                                         "}"))
        self.AccMan_Edit_Account.setFrameShadow(QtGui.QFrame.Plain)
        self.AccMan_Edit_Account.setLineWidth(0)
        self.AccMan_Edit_Account.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.AccMan_Edit_Account.setAutoScrollMargin(40)
        self.AccMan_Edit_Account.setProperty("showDropIndicator", True)
        self.AccMan_Edit_Account.setDragDropOverwriteMode(False)
        self.AccMan_Edit_Account.setSelectionBehavior(QtGui.QAbstractItemView.SelectItems)
        self.AccMan_Edit_Account.setTextElideMode(QtCore.Qt.ElideRight)
        self.AccMan_Edit_Account.setVerticalScrollMode(QtGui.QAbstractItemView.ScrollPerItem)
        self.AccMan_Edit_Account.setShowGrid(True)
        self.AccMan_Edit_Account.setGridStyle(QtCore.Qt.NoPen)
        self.AccMan_Edit_Account.setWordWrap(True)
        self.AccMan_Edit_Account.setCornerButtonEnabled(False)
        self.AccMan_Edit_Account.setObjectName(_fromUtf8("AccMan_Edit_Account"))
        self.AccMan_Edit_Account.setColumnCount(1)
        self.AccMan_Edit_Account.setRowCount(5)
        item = QtGui.QTableWidgetItem()
        self.AccMan_Edit_Account.setVerticalHeaderItem(0, item)
        item = QtGui.QTableWidgetItem()
        self.AccMan_Edit_Account.setVerticalHeaderItem(1, item)
        item = QtGui.QTableWidgetItem()
        self.AccMan_Edit_Account.setVerticalHeaderItem(2, item)
        item = QtGui.QTableWidgetItem()
        self.AccMan_Edit_Account.setVerticalHeaderItem(3, item)
        item = QtGui.QTableWidgetItem()
        self.AccMan_Edit_Account.setVerticalHeaderItem(4, item)
        item = QtGui.QTableWidgetItem()
        self.AccMan_Edit_Account.setHorizontalHeaderItem(0, item)
        item = QtGui.QTableWidgetItem()
        item.setFlags(
            QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEditable | QtCore.Qt.ItemIsDragEnabled | QtCore.Qt.ItemIsUserCheckable | QtCore.Qt.ItemIsEnabled)
        self.AccMan_Edit_Account.setItem(0, 0, item)
        item = QtGui.QTableWidgetItem()
        self.AccMan_Edit_Account.setItem(2, 0, item)
        item = QtGui.QTableWidgetItem()
        self.AccMan_Edit_Account.setItem(1, 0, item)
        item = QtGui.QTableWidgetItem()
        self.AccMan_Edit_Account.setItem(3, 0, item)
        item = QtGui.QTableWidgetItem()
        self.AccMan_Edit_Account.setItem(4, 0, item)
        self.AccMan_Edit_Account.horizontalHeader().setVisible(False)
        self.AccMan_Edit_Account.horizontalHeader().setCascadingSectionResizes(False)
        self.AccMan_Edit_Account.horizontalHeader().setHighlightSections(False)
        self.AccMan_Edit_Account.horizontalHeader().setMinimumSectionSize(40)
        self.AccMan_Edit_Account.horizontalHeader().setSortIndicatorShown(False)
        self.AccMan_Edit_Account.horizontalHeader().setStretchLastSection(True)
        self.AccMan_Edit_Account.verticalHeader().setCascadingSectionResizes(True)
        self.AccMan_Edit_Account.verticalHeader().setMinimumSectionSize(40)
        self.AccMan_EditChar_CPoints = QtGui.QGroupBox(self.AccManager_EditCharacter)
        self.AccMan_EditChar_CPoints.setGeometry(QtCore.QRect(580, 25, 160, 184))
        self.AccMan_EditChar_CPoints.setStyleSheet(_fromUtf8("QGroupBox {\n"
                                                             "\n"
                                                             " border: 2px solid gray;\n"
                                                             " border-radius: 5px;\n"
                                                             " margin-top: 1ex; /* leave space at the top for the title */\n"
                                                             "}"))
        self.AccMan_EditChar_CPoints.setObjectName(_fromUtf8("AccMan_EditChar_CPoints"))
        self.AccMan_EditChar_Credits = QtGui.QLineEdit(self.AccMan_EditChar_CPoints)
        self.AccMan_EditChar_Credits.setGeometry(QtCore.QRect(11, 28, 138, 40))
        font = QtGui.QFont()
        font.setPointSize(7)
        font.setBold(True)
        font.setItalic(False)
        font.setUnderline(False)
        font.setWeight(75)
        font.setStrikeOut(False)
        self.AccMan_EditChar_Credits.setFont(font)
        self.AccMan_EditChar_Credits.setCursor(QtGui.QCursor(QtCore.Qt.OpenHandCursor))
        self.AccMan_EditChar_Credits.setStyleSheet(_fromUtf8("color: rgb(252, 252, 252);\n"
                                                             "background: rgb(150, 150, 150);\n"
                                                             "border: 2px solid #555;\n"
                                                             "border-radius: 9px;\n"
                                                             "opacity: 39;"))
        self.AccMan_EditChar_Credits.setText(_fromUtf8(""))
        self.AccMan_EditChar_Credits.setObjectName(_fromUtf8("AccMan_EditChar_Credits"))
        self.AccMan_EditChar_PCPoints = QtGui.QLineEdit(self.AccMan_EditChar_CPoints)
        self.AccMan_EditChar_PCPoints.setGeometry(QtCore.QRect(12, 76, 138, 40))
        font = QtGui.QFont()
        font.setPointSize(7)
        font.setBold(True)
        font.setItalic(False)
        font.setUnderline(False)
        font.setWeight(75)
        font.setStrikeOut(False)
        self.AccMan_EditChar_PCPoints.setFont(font)
        self.AccMan_EditChar_PCPoints.setCursor(QtGui.QCursor(QtCore.Qt.OpenHandCursor))
        self.AccMan_EditChar_PCPoints.setStyleSheet(_fromUtf8("color: rgb(252, 252, 252);\n"
                                                              "background: rgb(150, 150, 150);\n"
                                                              "border: 2px solid #555;\n"
                                                              "border-radius: 9px;\n"
                                                              "opacity: 39;"))
        self.AccMan_EditChar_PCPoints.setText(_fromUtf8(""))
        self.AccMan_EditChar_PCPoints.setObjectName(_fromUtf8("AccMan_EditChar_PCPoints"))
        self.AccMan_EditChar_WCoin = QtGui.QLineEdit(self.AccMan_EditChar_CPoints)
        self.AccMan_EditChar_WCoin.setGeometry(QtCore.QRect(12, 126, 138, 40))
        font = QtGui.QFont()
        font.setPointSize(7)
        font.setBold(True)
        font.setItalic(False)
        font.setUnderline(False)
        font.setWeight(75)
        font.setStrikeOut(False)
        self.AccMan_EditChar_WCoin.setFont(font)
        self.AccMan_EditChar_WCoin.setCursor(QtGui.QCursor(QtCore.Qt.OpenHandCursor))
        self.AccMan_EditChar_WCoin.setStyleSheet(_fromUtf8("color: rgb(252, 252, 252);\n"
                                                           "background: rgb(150, 150, 150);\n"
                                                           "border: 2px solid #555;\n"
                                                           "border-radius: 9px;\n"
                                                           "opacity: 39;"))
        self.AccMan_EditChar_WCoin.setText(_fromUtf8(""))
        self.AccMan_EditChar_WCoin.setObjectName(_fromUtf8("AccMan_EditChar_WCoin"))
        self.AccMan_EditChar_BPoints = QtGui.QGroupBox(self.AccManager_EditCharacter)
        self.AccMan_EditChar_BPoints.setGeometry(QtCore.QRect(579, 214, 162, 57))
        self.AccMan_EditChar_BPoints.setStyleSheet(_fromUtf8("QGroupBox {\n"
                                                             "\n"
                                                             " border: 2px solid gray;\n"
                                                             " border-radius: 5px;\n"
                                                             " margin-top: 1ex; /* leave space at the top for the title */\n"
                                                             "}"))
        self.AccMan_EditChar_BPoints.setObjectName(_fromUtf8("AccMan_EditChar_BPoints"))
        self.AccMan_EditChar_Evolve = QtGui.QComboBox(self.AccMan_EditChar_BPoints)
        self.AccMan_EditChar_Evolve.setGeometry(QtCore.QRect(7, 22, 151, 31))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Narkisim"))
        font.setPointSize(10)
        font.setBold(True)
        font.setWeight(75)
        font.setStyleStrategy(QtGui.QFont.PreferAntialias)
        self.AccMan_EditChar_Evolve.setFont(font)
        self.AccMan_EditChar_Evolve.setStyleSheet(_fromUtf8(" QComboBox {\n"
                                                            "     border: 1px solid gray;\n"
                                                            "     border-radius: 3px;\n"
                                                            "     padding: 1px 18px 1px 3px;\n"
                                                            "     min-width: 6em;\n"
                                                            "color:rgb(79, 79, 79);\n"
                                                            " }\n"
                                                            "\n"
                                                            " QComboBox:editable {\n"
                                                            "     background: white;\n"
                                                            " }\n"
                                                            "\n"
                                                            " QComboBox:!editable, QComboBox::drop-down:editable {\n"
                                                            "      background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
                                                            "                                  stop: 0 #E1E1E1, stop: 0.4 #DDDDDD,\n"
                                                            "                                  stop: 0.5 #D8D8D8, stop: 1.0 #D3D3D3);\n"
                                                            " }\n"
                                                            "\n"
                                                            " /* QComboBox gets the \"on\" state when the popup is open */\n"
                                                            " QComboBox:!editable:on, QComboBox::drop-down:editable:on {\n"
                                                            "     background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
                                                            "                                 stop: 0 #D3D3D3, stop: 0.4 #D8D8D8,\n"
                                                            "                                 stop: 0.5 #DDDDDD, stop: 1.0 #E1E1E1);\n"
                                                            " }\n"
                                                            "\n"
                                                            " QComboBox:on { /* shift the text when the popup opens */\n"
                                                            "     padding-top: 3px;\n"
                                                            "     padding-left: 4px;\n"
                                                            " }\n"
                                                            "\n"
                                                            " QComboBox::drop-down {\n"
                                                            "     subcontrol-origin: padding;\n"
                                                            "     subcontrol-position: top right;\n"
                                                            "     width: 15px;\n"
                                                            "     background: #555;\n"
                                                            "     border-left-width: 1px;\n"
                                                            "     border-left-color: darkgray;\n"
                                                            "     border-left-style: solid; /* just a single line */\n"
                                                            "     border-top-right-radius: 3px; /* same radius as the QComboBox */\n"
                                                            "     border-bottom-right-radius: 3px;\n"
                                                            " }\n"
                                                            "QComboBox QAbstractItemView {\n"
                                                            "    border: 2px solid darkgray;\n"
                                                            "background: brown;\n"
                                                            "    color: white;\n"
                                                            "    selection-background-color: grey;\n"
                                                            "}\n"
                                                            "\n"
                                                            ""))
        self.AccMan_EditChar_Evolve.setObjectName(_fromUtf8("AccMan_EditChar_Evolve"))
        self.AccMan_EditChar_Evolve.addItem(_fromUtf8(""))
        self.AccMan_EditChar_Lottery = QtGui.QGroupBox(self.AccManager_EditCharacter)
        self.AccMan_EditChar_Lottery.setGeometry(QtCore.QRect(443, 195, 126, 129))
        self.AccMan_EditChar_Lottery.setStyleSheet(_fromUtf8("QGroupBox {\n"
                                                             "\n"
                                                             " border: 2px solid gray;\n"
                                                             " border-radius: 5px;\n"
                                                             " margin-top: 1ex; /* leave space at the top for the title */\n"
                                                             "}"))
        self.AccMan_EditChar_Lottery.setObjectName(_fromUtf8("AccMan_EditChar_Lottery"))
        self.AccMan_EditChar_ChangeClassChar = QtGui.QComboBox(self.AccMan_EditChar_Lottery)
        self.AccMan_EditChar_ChangeClassChar.setGeometry(QtCore.QRect(13, 24, 101, 31))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Narkisim"))
        font.setPointSize(10)
        font.setBold(True)
        font.setWeight(75)
        font.setStyleStrategy(QtGui.QFont.PreferAntialias)
        self.AccMan_EditChar_ChangeClassChar.setFont(font)
        self.AccMan_EditChar_ChangeClassChar.setStyleSheet(_fromUtf8(" QComboBox {\n"
                                                                     "     border: 1px solid gray;\n"
                                                                     "     border-radius: 3px;\n"
                                                                     "     padding: 1px 18px 1px 3px;\n"
                                                                     "     min-width: 6em;\n"
                                                                     "color:rgb(79, 79, 79);\n"
                                                                     " }\n"
                                                                     "\n"
                                                                     " QComboBox:editable {\n"
                                                                     "     background: white;\n"
                                                                     " }\n"
                                                                     "\n"
                                                                     " QComboBox:!editable, QComboBox::drop-down:editable {\n"
                                                                     "      background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
                                                                     "                                  stop: 0 #E1E1E1, stop: 0.4 #DDDDDD,\n"
                                                                     "                                  stop: 0.5 #D8D8D8, stop: 1.0 #D3D3D3);\n"
                                                                     " }\n"
                                                                     "\n"
                                                                     " /* QComboBox gets the \"on\" state when the popup is open */\n"
                                                                     " QComboBox:!editable:on, QComboBox::drop-down:editable:on {\n"
                                                                     "     background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
                                                                     "                                 stop: 0 #D3D3D3, stop: 0.4 #D8D8D8,\n"
                                                                     "                                 stop: 0.5 #DDDDDD, stop: 1.0 #E1E1E1);\n"
                                                                     " }\n"
                                                                     "\n"
                                                                     " QComboBox:on { /* shift the text when the popup opens */\n"
                                                                     "     padding-top: 3px;\n"
                                                                     "     padding-left: 4px;\n"
                                                                     " }\n"
                                                                     "\n"
                                                                     " QComboBox::drop-down {\n"
                                                                     "     subcontrol-origin: padding;\n"
                                                                     "     subcontrol-position: top right;\n"
                                                                     "     width: 15px;\n"
                                                                     "     background: #555;\n"
                                                                     "     border-left-width: 1px;\n"
                                                                     "     border-left-color: darkgray;\n"
                                                                     "     border-left-style: solid; /* just a single line */\n"
                                                                     "     border-top-right-radius: 3px; /* same radius as the QComboBox */\n"
                                                                     "     border-bottom-right-radius: 3px;\n"
                                                                     " }\n"
                                                                     "QComboBox QAbstractItemView {\n"
                                                                     "    border: 2px solid darkgray;\n"
                                                                     "background: brown;\n"
                                                                     "    color: white;\n"
                                                                     "    selection-background-color: grey;\n"
                                                                     "}\n"
                                                                     "\n"
                                                                     ""))
        self.AccMan_EditChar_ChangeClassChar.setObjectName(_fromUtf8("AccMan_EditChar_ChangeClassChar"))
        self.AccMan_EditChar_ChangeClassChar.addItem(_fromUtf8(""))
        self.label_7 = QtGui.QLabel(self.AccMan_EditChar_Lottery)
        self.label_7.setGeometry(QtCore.QRect(50, 68, 37, 16))
        self.label_7.setStyleSheet(_fromUtf8("color: grey"))
        self.label_7.setObjectName(_fromUtf8("label_7"))
        self.AccMan_EditChar_ChangeClass_Class = QtGui.QComboBox(self.AccMan_EditChar_Lottery)
        self.AccMan_EditChar_ChangeClass_Class.setGeometry(QtCore.QRect(14, 92, 101, 31))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Narkisim"))
        font.setPointSize(10)
        font.setBold(True)
        font.setWeight(75)
        font.setStyleStrategy(QtGui.QFont.PreferAntialias)
        self.AccMan_EditChar_ChangeClass_Class.setFont(font)
        self.AccMan_EditChar_ChangeClass_Class.setStyleSheet(_fromUtf8(" QComboBox {\n"
                                                                       "     border: 1px solid gray;\n"
                                                                       "     border-radius: 3px;\n"
                                                                       "     padding: 1px 18px 1px 3px;\n"
                                                                       "     min-width: 6em;\n"
                                                                       "color:rgb(79, 79, 79);\n"
                                                                       " }\n"
                                                                       "\n"
                                                                       " QComboBox:editable {\n"
                                                                       "     background: white;\n"
                                                                       " }\n"
                                                                       "\n"
                                                                       " QComboBox:!editable, QComboBox::drop-down:editable {\n"
                                                                       "      background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
                                                                       "                                  stop: 0 #E1E1E1, stop: 0.4 #DDDDDD,\n"
                                                                       "                                  stop: 0.5 #D8D8D8, stop: 1.0 #D3D3D3);\n"
                                                                       " }\n"
                                                                       "\n"
                                                                       " /* QComboBox gets the \"on\" state when the popup is open */\n"
                                                                       " QComboBox:!editable:on, QComboBox::drop-down:editable:on {\n"
                                                                       "     background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
                                                                       "                                 stop: 0 #D3D3D3, stop: 0.4 #D8D8D8,\n"
                                                                       "                                 stop: 0.5 #DDDDDD, stop: 1.0 #E1E1E1);\n"
                                                                       " }\n"
                                                                       "\n"
                                                                       " QComboBox:on { /* shift the text when the popup opens */\n"
                                                                       "     padding-top: 3px;\n"
                                                                       "     padding-left: 4px;\n"
                                                                       " }\n"
                                                                       "\n"
                                                                       " QComboBox::drop-down {\n"
                                                                       "     subcontrol-origin: padding;\n"
                                                                       "     subcontrol-position: top right;\n"
                                                                       "     width: 15px;\n"
                                                                       "     background: #555;\n"
                                                                       "     border-left-width: 1px;\n"
                                                                       "     border-left-color: darkgray;\n"
                                                                       "     border-left-style: solid; /* just a single line */\n"
                                                                       "     border-top-right-radius: 3px; /* same radius as the QComboBox */\n"
                                                                       "     border-bottom-right-radius: 3px;\n"
                                                                       " }\n"
                                                                       "QComboBox QAbstractItemView {\n"
                                                                       "    border: 2px solid darkgray;\n"
                                                                       "background: brown;\n"
                                                                       "    color: white;\n"
                                                                       "    selection-background-color: grey;\n"
                                                                       "}\n"
                                                                       "\n"
                                                                       ""))
        self.AccMan_EditChar_ChangeClass_Class.setObjectName(_fromUtf8("AccMan_EditChar_ChangeClass_Class"))
        self.AccMan_EditChar_ChangeClass_Class.addItem(_fromUtf8(""))
        self.AccMan_EditChar_GSepMisc = QtGui.QGroupBox(self.AccManager_EditCharacter)
        self.AccMan_EditChar_GSepMisc.setGeometry(QtCore.QRect(232, 65, 209, 297))
        self.AccMan_EditChar_GSepMisc.setStyleSheet(_fromUtf8("QGroupBox {\n"
                                                              "\n"
                                                              " border: 2px solid gray;\n"
                                                              " border-radius: 5px;\n"
                                                              " margin-top: 1ex; /* leave space at the top for the title */\n"
                                                              "}"))
        self.AccMan_EditChar_GSepMisc.setObjectName(_fromUtf8("AccMan_EditChar_GSepMisc"))
        self.AccMan_Edit_Char = QtGui.QTableWidget(self.AccMan_EditChar_GSepMisc)
        self.AccMan_Edit_Char.setGeometry(QtCore.QRect(1, 16, 207, 280))
        font = QtGui.QFont()
        font.setPointSize(12)
        self.AccMan_Edit_Char.setFont(font)
        self.AccMan_Edit_Char.setStyleSheet(_fromUtf8("QWidget {\n"
                                                      "    background-color: #333333;\n"
                                                      "    color: #fffff8;\n"
                                                      "}\n"
                                                      "\n"
                                                      "QHeaderView::section {\n"
                                                      "    background-color: #646464;\n"
                                                      "    padding: 4px;\n"
                                                      "    border: 0px solid #fffff8;\n"
                                                      "    font-size: 8pt;\n"
                                                      "}\n"
                                                      "\n"
                                                      "QTableWidget {\n"
                                                      "    gridline-color: #fffff8;\n"
                                                      "    font-size: 12pt;\n"
                                                      "}\n"
                                                      "\n"
                                                      "QTableWidget QTableCornerButton::section {\n"
                                                      "    background-color: #646464;\n"
                                                      "    border: 0px solid #fffff8;\n"
                                                      "}"))
        self.AccMan_Edit_Char.setFrameShadow(QtGui.QFrame.Plain)
        self.AccMan_Edit_Char.setLineWidth(0)
        self.AccMan_Edit_Char.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.AccMan_Edit_Char.setAutoScrollMargin(40)
        self.AccMan_Edit_Char.setProperty("showDropIndicator", True)
        self.AccMan_Edit_Char.setDragDropOverwriteMode(False)
        self.AccMan_Edit_Char.setSelectionBehavior(QtGui.QAbstractItemView.SelectItems)
        self.AccMan_Edit_Char.setTextElideMode(QtCore.Qt.ElideRight)
        self.AccMan_Edit_Char.setVerticalScrollMode(QtGui.QAbstractItemView.ScrollPerItem)
        self.AccMan_Edit_Char.setShowGrid(True)
        self.AccMan_Edit_Char.setGridStyle(QtCore.Qt.NoPen)
        self.AccMan_Edit_Char.setWordWrap(True)
        self.AccMan_Edit_Char.setCornerButtonEnabled(False)
        self.AccMan_Edit_Char.setObjectName(_fromUtf8("AccMan_Edit_Char"))
        self.AccMan_Edit_Char.setColumnCount(1)
        self.AccMan_Edit_Char.setRowCount(13)
        item = QtGui.QTableWidgetItem()
        self.AccMan_Edit_Char.setVerticalHeaderItem(0, item)
        item = QtGui.QTableWidgetItem()
        self.AccMan_Edit_Char.setVerticalHeaderItem(1, item)
        item = QtGui.QTableWidgetItem()
        self.AccMan_Edit_Char.setVerticalHeaderItem(2, item)
        item = QtGui.QTableWidgetItem()
        self.AccMan_Edit_Char.setVerticalHeaderItem(3, item)
        item = QtGui.QTableWidgetItem()
        self.AccMan_Edit_Char.setVerticalHeaderItem(4, item)
        item = QtGui.QTableWidgetItem()
        self.AccMan_Edit_Char.setVerticalHeaderItem(5, item)
        item = QtGui.QTableWidgetItem()
        self.AccMan_Edit_Char.setVerticalHeaderItem(6, item)
        item = QtGui.QTableWidgetItem()
        self.AccMan_Edit_Char.setVerticalHeaderItem(7, item)
        item = QtGui.QTableWidgetItem()
        self.AccMan_Edit_Char.setVerticalHeaderItem(8, item)
        item = QtGui.QTableWidgetItem()
        self.AccMan_Edit_Char.setVerticalHeaderItem(9, item)
        item = QtGui.QTableWidgetItem()
        self.AccMan_Edit_Char.setVerticalHeaderItem(10, item)
        item = QtGui.QTableWidgetItem()
        self.AccMan_Edit_Char.setVerticalHeaderItem(11, item)
        item = QtGui.QTableWidgetItem()
        self.AccMan_Edit_Char.setVerticalHeaderItem(12, item)
        item = QtGui.QTableWidgetItem()
        self.AccMan_Edit_Char.setHorizontalHeaderItem(0, item)
        item = QtGui.QTableWidgetItem()
        self.AccMan_Edit_Char.setItem(0, 0, item)
        item = QtGui.QTableWidgetItem()
        self.AccMan_Edit_Char.setItem(1, 0, item)
        item = QtGui.QTableWidgetItem()
        self.AccMan_Edit_Char.setItem(2, 0, item)
        item = QtGui.QTableWidgetItem()
        self.AccMan_Edit_Char.setItem(3, 0, item)
        item = QtGui.QTableWidgetItem()
        self.AccMan_Edit_Char.setItem(4, 0, item)
        item = QtGui.QTableWidgetItem()
        self.AccMan_Edit_Char.setItem(5, 0, item)
        item = QtGui.QTableWidgetItem()
        self.AccMan_Edit_Char.setItem(6, 0, item)
        item = QtGui.QTableWidgetItem()
        self.AccMan_Edit_Char.setItem(7, 0, item)
        item = QtGui.QTableWidgetItem()
        self.AccMan_Edit_Char.setItem(8, 0, item)
        item = QtGui.QTableWidgetItem()
        self.AccMan_Edit_Char.setItem(9, 0, item)
        item = QtGui.QTableWidgetItem()
        self.AccMan_Edit_Char.setItem(10, 0, item)
        item = QtGui.QTableWidgetItem()
        self.AccMan_Edit_Char.setItem(11, 0, item)
        item = QtGui.QTableWidgetItem()
        self.AccMan_Edit_Char.setItem(12, 0, item)
        self.AccMan_Edit_Char.horizontalHeader().setVisible(False)
        self.AccMan_Edit_Char.horizontalHeader().setCascadingSectionResizes(False)
        self.AccMan_Edit_Char.horizontalHeader().setHighlightSections(True)
        self.AccMan_Edit_Char.horizontalHeader().setMinimumSectionSize(40)
        self.AccMan_Edit_Char.horizontalHeader().setSortIndicatorShown(False)
        self.AccMan_Edit_Char.horizontalHeader().setStretchLastSection(True)
        self.AccMan_Edit_Char.verticalHeader().setCascadingSectionResizes(True)
        self.AccMan_Edit_Char.verticalHeader().setMinimumSectionSize(40)
        self.AccMan_EditChar_GSepGuild = QtGui.QGroupBox(self.AccManager_EditCharacter)
        self.AccMan_EditChar_GSepGuild.setGeometry(QtCore.QRect(444, 24, 125, 159))
        self.AccMan_EditChar_GSepGuild.setStyleSheet(_fromUtf8("QGroupBox {\n"
                                                               "\n"
                                                               " border: 2px solid gray;\n"
                                                               " border-radius: 5px;\n"
                                                               " margin-top: 1ex; /* leave space at the top for the title */\n"
                                                               "}"))
        self.AccMan_EditChar_GSepGuild.setObjectName(_fromUtf8("AccMan_EditChar_GSepGuild"))
        self.AccMan_Edit_Char_2 = QtGui.QTableWidget(self.AccMan_EditChar_GSepGuild)
        self.AccMan_Edit_Char_2.setGeometry(QtCore.QRect(2, 55, 121, 59))
        font = QtGui.QFont()
        font.setPointSize(9)
        self.AccMan_Edit_Char_2.setFont(font)
        self.AccMan_Edit_Char_2.setStyleSheet(_fromUtf8("QWidget {\n"
                                                        "    background-color: #333333;\n"
                                                        "    color: #fffff8;\n"
                                                        "}\n"
                                                        "\n"
                                                        "QHeaderView::section {\n"
                                                        "    background-color: #646464;\n"
                                                        "    padding: 4px;\n"
                                                        "    border: 0px solid #fffff8;\n"
                                                        "    font-size: 8pt;\n"
                                                        "}\n"
                                                        "\n"
                                                        "QTableWidget {\n"
                                                        "    gridline-color: #fffff8;\n"
                                                        "    font-size: 9pt;\n"
                                                        "}\n"
                                                        "\n"
                                                        "QTableWidget QTableCornerButton::section {\n"
                                                        "    background-color: #646464;\n"
                                                        "    border: 0px solid #fffff8;\n"
                                                        "}"))
        self.AccMan_Edit_Char_2.setFrameShadow(QtGui.QFrame.Plain)
        self.AccMan_Edit_Char_2.setLineWidth(0)
        self.AccMan_Edit_Char_2.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.AccMan_Edit_Char_2.setAutoScrollMargin(40)
        self.AccMan_Edit_Char_2.setProperty("showDropIndicator", True)
        self.AccMan_Edit_Char_2.setDragDropOverwriteMode(False)
        self.AccMan_Edit_Char_2.setSelectionBehavior(QtGui.QAbstractItemView.SelectItems)
        self.AccMan_Edit_Char_2.setTextElideMode(QtCore.Qt.ElideRight)
        self.AccMan_Edit_Char_2.setVerticalScrollMode(QtGui.QAbstractItemView.ScrollPerItem)
        self.AccMan_Edit_Char_2.setShowGrid(True)
        self.AccMan_Edit_Char_2.setGridStyle(QtCore.Qt.NoPen)
        self.AccMan_Edit_Char_2.setWordWrap(True)
        self.AccMan_Edit_Char_2.setCornerButtonEnabled(False)
        self.AccMan_Edit_Char_2.setObjectName(_fromUtf8("AccMan_Edit_Char_2"))
        self.AccMan_Edit_Char_2.setColumnCount(1)
        self.AccMan_Edit_Char_2.setRowCount(2)
        item = QtGui.QTableWidgetItem()
        self.AccMan_Edit_Char_2.setVerticalHeaderItem(0, item)
        item = QtGui.QTableWidgetItem()
        self.AccMan_Edit_Char_2.setVerticalHeaderItem(1, item)
        item = QtGui.QTableWidgetItem()
        self.AccMan_Edit_Char_2.setHorizontalHeaderItem(0, item)
        item = QtGui.QTableWidgetItem()
        self.AccMan_Edit_Char_2.setItem(0, 0, item)
        item = QtGui.QTableWidgetItem()
        self.AccMan_Edit_Char_2.setItem(1, 0, item)
        self.AccMan_Edit_Char_2.horizontalHeader().setVisible(False)
        self.AccMan_Edit_Char_2.horizontalHeader().setCascadingSectionResizes(False)
        self.AccMan_Edit_Char_2.horizontalHeader().setHighlightSections(False)
        self.AccMan_Edit_Char_2.horizontalHeader().setMinimumSectionSize(40)
        self.AccMan_Edit_Char_2.horizontalHeader().setSortIndicatorShown(False)
        self.AccMan_Edit_Char_2.horizontalHeader().setStretchLastSection(True)
        self.AccMan_Edit_Char_2.verticalHeader().setCascadingSectionResizes(True)
        self.AccMan_Edit_Char_2.verticalHeader().setMinimumSectionSize(40)
        self.AccMan_EditChar_SaveAll_3 = QtGui.QPushButton(self.AccMan_EditChar_GSepGuild)
        self.AccMan_EditChar_SaveAll_3.setGeometry(QtCore.QRect(4, 115, 116, 40))
        font = QtGui.QFont()
        font.setPointSize(8)
        font.setBold(True)
        font.setWeight(75)
        font.setStyleStrategy(QtGui.QFont.PreferAntialias)
        self.AccMan_EditChar_SaveAll_3.setFont(font)
        self.AccMan_EditChar_SaveAll_3.setStyleSheet(_fromUtf8("QPushButton {\n"
                                                               "color: #333;\n"
                                                               "\n"
                                                               "border: 1px solid #555;\n"
                                                               "border-radius: 11px;\n"
                                                               "padding: 5px;\n"
                                                               "background: qradialgradient(cx: 0.3, cy: -0.4,\n"
                                                               "fx: 0.3, fy: -0.4,\n"
                                                               "radius: 1.35, stop: 0 #fff, stop: 1 #888);\n"
                                                               "min-width: 80px;\n"
                                                               "}\n"
                                                               "\n"
                                                               "\n"
                                                               "QPushButton:hover {\n"
                                                               "background: qradialgradient(cx: 0.3, cy: -0.4,\n"
                                                               "fx: 0.3, fy: -0.4,\n"
                                                               "radius: 1.35, stop: 0 #fff, stop: 1 #bbb);\n"
                                                               "}\n"
                                                               "\n"
                                                               "QPushButton:pressed {\n"
                                                               "background: qradialgradient(cx: 0.4, cy: -0.1,\n"
                                                               "fx: 0.4, fy: -0.1,\n"
                                                               "radius: 1.35, stop: 0 #fff, stop: 1 #ddd);\n"
                                                               "}"))
        self.AccMan_EditChar_SaveAll_3.setObjectName(_fromUtf8("AccMan_EditChar_SaveAll_3"))
        self.AccMan_EditChar_Evolve_2 = QtGui.QComboBox(self.AccMan_EditChar_GSepGuild)
        self.AccMan_EditChar_Evolve_2.setGeometry(QtCore.QRect(3, 21, 119, 31))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Narkisim"))
        font.setPointSize(10)
        font.setBold(True)
        font.setWeight(75)
        font.setStyleStrategy(QtGui.QFont.PreferAntialias)
        self.AccMan_EditChar_Evolve_2.setFont(font)
        self.AccMan_EditChar_Evolve_2.setStyleSheet(_fromUtf8(" QComboBox {\n"
                                                              "     border: 1px solid gray;\n"
                                                              "     border-radius: 3px;\n"
                                                              "     padding: 1px 18px 1px 3px;\n"
                                                              "     min-width: 6em;\n"
                                                              "color:rgb(79, 79, 79);\n"
                                                              " }\n"
                                                              "\n"
                                                              " QComboBox:editable {\n"
                                                              "     background: white;\n"
                                                              " }\n"
                                                              "\n"
                                                              " QComboBox:!editable, QComboBox::drop-down:editable {\n"
                                                              "      background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
                                                              "                                  stop: 0 #E1E1E1, stop: 0.4 #DDDDDD,\n"
                                                              "                                  stop: 0.5 #D8D8D8, stop: 1.0 #D3D3D3);\n"
                                                              " }\n"
                                                              "\n"
                                                              " /* QComboBox gets the \"on\" state when the popup is open */\n"
                                                              " QComboBox:!editable:on, QComboBox::drop-down:editable:on {\n"
                                                              "     background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
                                                              "                                 stop: 0 #D3D3D3, stop: 0.4 #D8D8D8,\n"
                                                              "                                 stop: 0.5 #DDDDDD, stop: 1.0 #E1E1E1);\n"
                                                              " }\n"
                                                              "\n"
                                                              " QComboBox:on { /* shift the text when the popup opens */\n"
                                                              "     padding-top: 3px;\n"
                                                              "     padding-left: 4px;\n"
                                                              " }\n"
                                                              "\n"
                                                              " QComboBox::drop-down {\n"
                                                              "     subcontrol-origin: padding;\n"
                                                              "     subcontrol-position: top right;\n"
                                                              "     width: 15px;\n"
                                                              "     background: #555;\n"
                                                              "     border-left-width: 1px;\n"
                                                              "     border-left-color: darkgray;\n"
                                                              "     border-left-style: solid; /* just a single line */\n"
                                                              "     border-top-right-radius: 3px; /* same radius as the QComboBox */\n"
                                                              "     border-bottom-right-radius: 3px;\n"
                                                              " }\n"
                                                              "QComboBox QAbstractItemView {\n"
                                                              "    border: 2px solid darkgray;\n"
                                                              "background: brown;\n"
                                                              "    color: white;\n"
                                                              "    selection-background-color: grey;\n"
                                                              "}\n"
                                                              "\n"
                                                              ""))
        self.AccMan_EditChar_Evolve_2.setObjectName(_fromUtf8("AccMan_EditChar_Evolve_2"))
        self.AccMan_EditChar_Evolve_2.addItem(_fromUtf8(""))
        self.AccMan_EditChar_Choose_3 = QtGui.QComboBox(self.AccManager_EditCharacter)
        self.AccMan_EditChar_Choose_3.setGeometry(QtCore.QRect(341, 26, 101, 31))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Narkisim"))
        font.setPointSize(10)
        font.setBold(True)
        font.setWeight(75)
        font.setStyleStrategy(QtGui.QFont.PreferAntialias)
        self.AccMan_EditChar_Choose_3.setFont(font)
        self.AccMan_EditChar_Choose_3.setStyleSheet(_fromUtf8(" QComboBox {\n"
                                                              "     border: 1px solid gray;\n"
                                                              "     border-radius: 3px;\n"
                                                              "     padding: 1px 18px 1px 3px;\n"
                                                              "     min-width: 6em;\n"
                                                              "color:rgb(79, 79, 79);\n"
                                                              " }\n"
                                                              "\n"
                                                              " QComboBox:editable {\n"
                                                              "     background: white;\n"
                                                              " }\n"
                                                              "\n"
                                                              " QComboBox:!editable, QComboBox::drop-down:editable {\n"
                                                              "      background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
                                                              "                                  stop: 0 #E1E1E1, stop: 0.4 #DDDDDD,\n"
                                                              "                                  stop: 0.5 #D8D8D8, stop: 1.0 #D3D3D3);\n"
                                                              " }\n"
                                                              "\n"
                                                              " /* QComboBox gets the \"on\" state when the popup is open */\n"
                                                              " QComboBox:!editable:on, QComboBox::drop-down:editable:on {\n"
                                                              "     background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
                                                              "                                 stop: 0 #D3D3D3, stop: 0.4 #D8D8D8,\n"
                                                              "                                 stop: 0.5 #DDDDDD, stop: 1.0 #E1E1E1);\n"
                                                              " }\n"
                                                              "\n"
                                                              " QComboBox:on { /* shift the text when the popup opens */\n"
                                                              "     padding-top: 3px;\n"
                                                              "     padding-left: 4px;\n"
                                                              " }\n"
                                                              "\n"
                                                              " QComboBox::drop-down {\n"
                                                              "     subcontrol-origin: padding;\n"
                                                              "     subcontrol-position: top right;\n"
                                                              "     width: 15px;\n"
                                                              "     background: #555;\n"
                                                              "     border-left-width: 1px;\n"
                                                              "     border-left-color: darkgray;\n"
                                                              "     border-left-style: solid; /* just a single line */\n"
                                                              "     border-top-right-radius: 3px; /* same radius as the QComboBox */\n"
                                                              "     border-bottom-right-radius: 3px;\n"
                                                              " }\n"
                                                              "QComboBox QAbstractItemView {\n"
                                                              "    border: 2px solid darkgray;\n"
                                                              "background: brown;\n"
                                                              "    color: white;\n"
                                                              "    selection-background-color: grey;\n"
                                                              "}\n"
                                                              "\n"
                                                              ""))
        self.AccMan_EditChar_Choose_3.setObjectName(_fromUtf8("AccMan_EditChar_Choose_3"))
        self.AccMan_EditChar_Choose_3.addItem(_fromUtf8(""))
        self.AccMan_EditChar_Choose_3.addItem(_fromUtf8(""))
        self.AccMan_EditChar_Choose_3.addItem(_fromUtf8(""))
        self.AccMan_EditChar_Choose_3.addItem(_fromUtf8(""))
        self.AccMan_EditChar_Choose_3.addItem(_fromUtf8(""))
        self.AccMan_EditChar_Choose_3.addItem(_fromUtf8(""))
        self.AccMan_EditChar_Choose_3.addItem(_fromUtf8(""))
        self.AccMan_EditChar_Choose_3.addItem(_fromUtf8(""))
        self.AccMan_EditChar_Restore = QtGui.QPushButton(self.AccManager_EditCharacter)
        self.AccMan_EditChar_Restore.setGeometry(QtCore.QRect(577, 278, 166, 40))
        font = QtGui.QFont()
        font.setPointSize(9)
        font.setBold(True)
        font.setWeight(75)
        font.setStyleStrategy(QtGui.QFont.PreferAntialias)
        self.AccMan_EditChar_Restore.setFont(font)
        self.AccMan_EditChar_Restore.setStyleSheet(_fromUtf8("QPushButton {\n"
                                                             "color: #333;\n"
                                                             "\n"
                                                             "border: 1px solid #555;\n"
                                                             "border-radius: 11px;\n"
                                                             "padding: 5px;\n"
                                                             "background: qradialgradient(cx: 0.3, cy: -0.4,\n"
                                                             "fx: 0.3, fy: -0.4,\n"
                                                             "radius: 1.35, stop: 0 #fff, stop: 1 #888);\n"
                                                             "min-width: 80px;\n"
                                                             "}\n"
                                                             "\n"
                                                             "\n"
                                                             "QPushButton:hover {\n"
                                                             "background: qradialgradient(cx: 0.3, cy: -0.4,\n"
                                                             "fx: 0.3, fy: -0.4,\n"
                                                             "radius: 1.35, stop: 0 #fff, stop: 1 #bbb);\n"
                                                             "}\n"
                                                             "\n"
                                                             "QPushButton:pressed {\n"
                                                             "background: qradialgradient(cx: 0.4, cy: -0.1,\n"
                                                             "fx: 0.4, fy: -0.1,\n"
                                                             "radius: 1.35, stop: 0 #fff, stop: 1 #ddd);\n"
                                                             "}"))
        self.AccMan_EditChar_Restore.setObjectName(_fromUtf8("AccMan_EditChar_Restore"))
        self.AccManagerTabber.addTab(self.AccManager_EditCharacter, _fromUtf8(""))
        self.AccManagement_Ban = QtGui.QWidget()
        self.AccManagement_Ban.setObjectName(_fromUtf8("AccManagement_Ban"))
        self.label_9 = QtGui.QLabel(self.AccManagement_Ban)
        self.label_9.setGeometry(QtCore.QRect(158, 83, 486, 198))
        self.label_9.setText(_fromUtf8(""))
        self.label_9.setPixmap(QtGui.QPixmap(_fromUtf8(":/coresec/im/csec.png")))
        self.label_9.setObjectName(_fromUtf8("label_9"))
        self.AccManagerTabber.addTab(self.AccManagement_Ban, _fromUtf8(""))
        self.AccManager_Skill = QtGui.QWidget()
        self.AccManager_Skill.setObjectName(_fromUtf8("AccManager_Skill"))
        self.label_8 = QtGui.QLabel(self.AccManager_Skill)
        self.label_8.setGeometry(QtCore.QRect(481, 12, 268, 16))
        self.label_8.setObjectName(_fromUtf8("label_8"))
        self.Notes_NotesText = QtGui.QPlainTextEdit(self.AccManager_Skill)
        self.Notes_NotesText.setGeometry(QtCore.QRect(6, 36, 745, 271))
        font = QtGui.QFont()
        font.setPointSize(13)
        self.Notes_NotesText.setFont(font)
        self.Notes_NotesText.setStyleSheet(_fromUtf8("color: rgb(48, 48, 48);\n"
                                                     "background: rgb(150, 150, 150);\n"
                                                     "border: 2px solid #555;\n"
                                                     "border-radius: 9px;\n"
                                                     "opacity: 39;"))
        self.Notes_NotesText.setObjectName(_fromUtf8("Notes_NotesText"))
        self.Notes_NotesSave = QtGui.QPushButton(self.AccManager_Skill)
        self.Notes_NotesSave.setGeometry(QtCore.QRect(592, 314, 153, 40))
        font = QtGui.QFont()
        font.setPointSize(11)
        font.setBold(True)
        font.setWeight(75)
        font.setStyleStrategy(QtGui.QFont.PreferAntialias)
        self.Notes_NotesSave.setFont(font)
        self.Notes_NotesSave.setStyleSheet(_fromUtf8("QPushButton {\n"
                                                     "color: #333;\n"
                                                     "\n"
                                                     "border: 1px solid #555;\n"
                                                     "border-radius: 11px;\n"
                                                     "padding: 5px;\n"
                                                     "background: qradialgradient(cx: 0.3, cy: -0.4,\n"
                                                     "fx: 0.3, fy: -0.4,\n"
                                                     "radius: 1.35, stop: 0 #fff, stop: 1 #888);\n"
                                                     "min-width: 80px;\n"
                                                     "}\n"
                                                     "\n"
                                                     "\n"
                                                     "QPushButton:hover {\n"
                                                     "background: qradialgradient(cx: 0.3, cy: -0.4,\n"
                                                     "fx: 0.3, fy: -0.4,\n"
                                                     "radius: 1.35, stop: 0 #fff, stop: 1 #bbb);\n"
                                                     "}\n"
                                                     "\n"
                                                     "QPushButton:pressed {\n"
                                                     "background: qradialgradient(cx: 0.4, cy: -0.1,\n"
                                                     "fx: 0.4, fy: -0.1,\n"
                                                     "radius: 1.35, stop: 0 #fff, stop: 1 #ddd);\n"
                                                     "}"))
        self.Notes_NotesSave.setObjectName(_fromUtf8("Notes_NotesSave"))
        self.AccMan_EditChar_Choose_4 = QtGui.QComboBox(self.AccManager_Skill)
        self.AccMan_EditChar_Choose_4.setGeometry(QtCore.QRect(410, 316, 167, 38))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Narkisim"))
        font.setPointSize(10)
        font.setBold(True)
        font.setWeight(75)
        font.setStyleStrategy(QtGui.QFont.PreferAntialias)
        self.AccMan_EditChar_Choose_4.setFont(font)
        self.AccMan_EditChar_Choose_4.setStyleSheet(_fromUtf8(" QComboBox {\n"
                                                              "     border: 1px solid gray;\n"
                                                              "     border-radius: 3px;\n"
                                                              "     padding: 1px 18px 1px 3px;\n"
                                                              "     min-width: 6em;\n"
                                                              "color:rgb(79, 79, 79);\n"
                                                              " }\n"
                                                              "\n"
                                                              " QComboBox:editable {\n"
                                                              "     background: white;\n"
                                                              " }\n"
                                                              "\n"
                                                              " QComboBox:!editable, QComboBox::drop-down:editable {\n"
                                                              "      background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
                                                              "                                  stop: 0 #E1E1E1, stop: 0.4 #DDDDDD,\n"
                                                              "                                  stop: 0.5 #D8D8D8, stop: 1.0 #D3D3D3);\n"
                                                              " }\n"
                                                              "\n"
                                                              " /* QComboBox gets the \"on\" state when the popup is open */\n"
                                                              " QComboBox:!editable:on, QComboBox::drop-down:editable:on {\n"
                                                              "     background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
                                                              "                                 stop: 0 #D3D3D3, stop: 0.4 #D8D8D8,\n"
                                                              "                                 stop: 0.5 #DDDDDD, stop: 1.0 #E1E1E1);\n"
                                                              " }\n"
                                                              "\n"
                                                              " QComboBox:on { /* shift the text when the popup opens */\n"
                                                              "     padding-top: 3px;\n"
                                                              "     padding-left: 4px;\n"
                                                              " }\n"
                                                              "\n"
                                                              " QComboBox::drop-down {\n"
                                                              "     subcontrol-origin: padding;\n"
                                                              "     subcontrol-position: top right;\n"
                                                              "     width: 15px;\n"
                                                              "     background: #555;\n"
                                                              "     border-left-width: 1px;\n"
                                                              "     border-left-color: darkgray;\n"
                                                              "     border-left-style: solid; /* just a single line */\n"
                                                              "     border-top-right-radius: 3px; /* same radius as the QComboBox */\n"
                                                              "     border-bottom-right-radius: 3px;\n"
                                                              " }\n"
                                                              "QComboBox QAbstractItemView {\n"
                                                              "    border: 2px solid darkgray;\n"
                                                              "background: brown;\n"
                                                              "    color: white;\n"
                                                              "    selection-background-color: grey;\n"
                                                              "}\n"
                                                              "\n"
                                                              ""))
        self.AccMan_EditChar_Choose_4.setObjectName(_fromUtf8("AccMan_EditChar_Choose_4"))
        self.AccMan_EditChar_Choose_4.addItem(_fromUtf8(""))
        self.AccMan_EditChar_Choose_4.addItem(_fromUtf8(""))
        self.AccManagerTabber.addTab(self.AccManager_Skill, _fromUtf8(""))
        self.bmuPager.addWidget(self.accountManagerPage)
        self.connectionPage = QtGui.QWidget()
        self.connectionPage.setObjectName(_fromUtf8("connectionPage"))
        self.ctitle = QtGui.QLabel(self.connectionPage)
        self.ctitle.setGeometry(QtCore.QRect(692, -2, 76, 16))
        self.ctitle.setStyleSheet(_fromUtf8("color: rgb(255, 73, 12)"))
        self.ctitle.setObjectName(_fromUtf8("ctitle"))
        self.label_2 = QtGui.QLabel(self.connectionPage)
        self.label_2.setGeometry(QtCore.QRect(237, 123, 126, 16))
        font = QtGui.QFont()
        font.setPointSize(13)
        font.setBold(False)
        font.setWeight(50)
        self.label_2.setFont(font)
        self.label_2.setStyleSheet(_fromUtf8("background: transparent;\n"
                                             "color: rgb(255, 255, 255)"))
        self.label_2.setObjectName(_fromUtf8("label_2"))
        self.label_3 = QtGui.QLabel(self.connectionPage)
        self.label_3.setGeometry(QtCore.QRect(270, 177, 88, 16))
        font = QtGui.QFont()
        font.setPointSize(13)
        font.setBold(False)
        font.setWeight(50)
        self.label_3.setFont(font)
        self.label_3.setStyleSheet(_fromUtf8("background: transparent;\n"
                                             "color: rgb(255, 255, 255)"))
        self.label_3.setObjectName(_fromUtf8("label_3"))
        self.label_4 = QtGui.QLabel(self.connectionPage)
        self.label_4.setGeometry(QtCore.QRect(274, 222, 87, 28))
        font = QtGui.QFont()
        font.setPointSize(13)
        font.setBold(False)
        font.setWeight(50)
        self.label_4.setFont(font)
        self.label_4.setStyleSheet(_fromUtf8("background: transparent;\n"
                                             "color: rgb(255, 255, 255)"))
        self.label_4.setObjectName(_fromUtf8("label_4"))
        self.label_5 = QtGui.QLabel(self.connectionPage)
        self.label_5.setGeometry(QtCore.QRect(198, 74, 167, 16))
        font = QtGui.QFont()
        font.setPointSize(13)
        font.setBold(False)
        font.setWeight(50)
        self.label_5.setFont(font)
        self.label_5.setStyleSheet(_fromUtf8("background: transparent;\n"
                                             "color: rgb(255, 255, 255)"))
        self.label_5.setObjectName(_fromUtf8("label_5"))
        self.ConnectionValue = QtGui.QPlainTextEdit(self.connectionPage)
        self.ConnectionValue.setGeometry(QtCore.QRect(365, 60, 231, 42))
        font = QtGui.QFont()
        font.setPointSize(11)
        self.ConnectionValue.setFont(font)
        self.ConnectionValue.setStyleSheet(_fromUtf8("\n"
                                                     "color: #333;\n"
                                                     "\n"
                                                     "border: 1px solid #555;\n"
                                                     "border-radius: 11px;\n"
                                                     "padding: 5px;\n"
                                                     "background: white;"))
        self.ConnectionValue.setObjectName(_fromUtf8("ConnectionValue"))
        self.Connection_Version = QtGui.QPlainTextEdit(self.connectionPage)
        self.Connection_Version.setGeometry(QtCore.QRect(364, 111, 231, 42))
        font = QtGui.QFont()
        font.setPointSize(11)
        self.Connection_Version.setFont(font)
        self.Connection_Version.setStyleSheet(_fromUtf8("\n"
                                                        "color: #333;\n"
                                                        "\n"
                                                        "border: 1px solid #555;\n"
                                                        "border-radius: 11px;\n"
                                                        "padding: 5px;\n"
                                                        "background: white;"))
        self.Connection_Version.setObjectName(_fromUtf8("Connection_Version"))
        self.Connection_userName = QtGui.QPlainTextEdit(self.connectionPage)
        self.Connection_userName.setGeometry(QtCore.QRect(363, 162, 232, 42))
        font = QtGui.QFont()
        font.setPointSize(11)
        self.Connection_userName.setFont(font)
        self.Connection_userName.setStyleSheet(_fromUtf8("\n"
                                                         "color: #333;\n"
                                                         "\n"
                                                         "border: 1px solid #555;\n"
                                                         "border-radius: 11px;\n"
                                                         "padding: 5px;\n"
                                                         "background: white;"))
        self.Connection_userName.setObjectName(_fromUtf8("Connection_userName"))
        self.Connection_passWord = QtGui.QLineEdit(self.connectionPage)
        self.Connection_passWord.setGeometry(QtCore.QRect(364, 213, 231, 40))
        self.Connection_passWord.setStyleSheet(_fromUtf8("\n"
                                                         "color: #333;\n"
                                                         "\n"
                                                         "border: 1px solid #555;\n"
                                                         "border-radius: 11px;\n"
                                                         "padding: 5px;\n"
                                                         "background: white;"))
        self.Connection_passWord.setEchoMode(QtGui.QLineEdit.Password)
        self.Connection_passWord.setObjectName(_fromUtf8("Connection_passWord"))
        self.ConnectNow = QtGui.QPushButton(self.connectionPage)
        self.ConnectNow.setGeometry(QtCore.QRect(194, 280, 402, 66))
        font = QtGui.QFont()
        font.setPointSize(13)
        font.setBold(False)
        font.setWeight(50)
        self.ConnectNow.setFont(font)
        self.ConnectNow.setStyleSheet(_fromUtf8("QPushButton {\n"
                                                "color: #333;\n"
                                                "\n"
                                                "border: 1px solid #555;\n"
                                                "border-radius: 11px;\n"
                                                "padding: 5px;\n"
                                                "background: qradialgradient(cx: 0.3, cy: -0.4,\n"
                                                "fx: 0.3, fy: -0.4,\n"
                                                "radius: 1.35, stop: 0 #fff, stop: 1 #888);\n"
                                                "min-width: 80px;\n"
                                                "}\n"
                                                "\n"
                                                "\n"
                                                "QPushButton:hover {\n"
                                                "background: qradialgradient(cx: 0.3, cy: -0.4,\n"
                                                "fx: 0.3, fy: -0.4,\n"
                                                "radius: 1.35, stop: 0 #fff, stop: 1 #bbb);\n"
                                                "}\n"
                                                "\n"
                                                "QPushButton:pressed {\n"
                                                "background: qradialgradient(cx: 0.4, cy: -0.1,\n"
                                                "fx: 0.4, fy: -0.1,\n"
                                                "radius: 1.35, stop: 0 #fff, stop: 1 #ddd);\n"
                                                "}"))
        self.ConnectNow.setObjectName(_fromUtf8("ConnectNow"))
        self.bmuPager.addWidget(self.connectionPage)
        self.resourcePage = QtGui.QWidget()
        self.resourcePage.setObjectName(_fromUtf8("resourcePage"))
        self.rtitle = QtGui.QLabel(self.resourcePage)
        self.rtitle.setGeometry(QtCore.QRect(703, -2, 68, 16))
        self.rtitle.setStyleSheet(_fromUtf8("color: rgb(255, 73, 12)"))
        self.rtitle.setObjectName(_fromUtf8("rtitle"))
        self.Resources_CustomQ = QtGui.QPushButton(self.resourcePage)
        self.Resources_CustomQ.setGeometry(QtCore.QRect(322, 315, 161, 40))
        font = QtGui.QFont()
        font.setPointSize(11)
        font.setBold(True)
        font.setWeight(75)
        font.setStyleStrategy(QtGui.QFont.PreferAntialias)
        self.Resources_CustomQ.setFont(font)
        self.Resources_CustomQ.setStyleSheet(_fromUtf8("QPushButton {\n"
                                                       "color: #333;\n"
                                                       "\n"
                                                       "border: 1px solid #555;\n"
                                                       "border-radius: 11px;\n"
                                                       "padding: 5px;\n"
                                                       "background: qradialgradient(cx: 0.3, cy: -0.4,\n"
                                                       "fx: 0.3, fy: -0.4,\n"
                                                       "radius: 1.35, stop: 0 #fff, stop: 1 #888);\n"
                                                       "min-width: 80px;\n"
                                                       "}\n"
                                                       "\n"
                                                       "\n"
                                                       "QPushButton:hover {\n"
                                                       "background: qradialgradient(cx: 0.3, cy: -0.4,\n"
                                                       "fx: 0.3, fy: -0.4,\n"
                                                       "radius: 1.35, stop: 0 #fff, stop: 1 #bbb);\n"
                                                       "}\n"
                                                       "\n"
                                                       "QPushButton:pressed {\n"
                                                       "background: qradialgradient(cx: 0.4, cy: -0.1,\n"
                                                       "fx: 0.4, fy: -0.1,\n"
                                                       "radius: 1.35, stop: 0 #fff, stop: 1 #ddd);\n"
                                                       "}"))
        self.Resources_CustomQ.setObjectName(_fromUtf8("Resources_CustomQ"))
        self.tsqlBox = QtGui.QTextEdit(self.resourcePage)
        self.tsqlBox.setGeometry(QtCore.QRect(23, 23, 742, 278))
        self.tsqlBox.setStyleSheet(_fromUtf8("background: white;"))
        self.tsqlBox.setObjectName(_fromUtf8("tsqlBox"))
        self.bmuPager.addWidget(self.resourcePage)
        self.MiscPage = QtGui.QWidget()
        self.MiscPage.setObjectName(_fromUtf8("MiscPage"))
        self.mtitle = QtGui.QLabel(self.MiscPage)
        self.mtitle.setGeometry(QtCore.QRect(697, -2, 77, 18))
        self.mtitle.setStyleSheet(_fromUtf8("color: rgb(255, 73, 12)"))
        self.mtitle.setObjectName(_fromUtf8("mtitle"))
        self.bmuPager.addWidget(self.MiscPage)
        self.AboutPage = QtGui.QWidget()
        self.AboutPage.setObjectName(_fromUtf8("AboutPage"))
        self.devtitle = QtGui.QLabel(self.AboutPage)
        self.devtitle.setGeometry(QtCore.QRect(715, -1, 56, 16))
        self.devtitle.setStyleSheet(_fromUtf8("color: rgb(255, 73, 12)"))
        self.devtitle.setObjectName(_fromUtf8("devtitle"))
        self.dev_copyright = QtGui.QLabel(self.AboutPage)
        self.dev_copyright.setGeometry(QtCore.QRect(267, 213, 326, 46))
        self.dev_copyright.setStyleSheet(_fromUtf8("background: transparent;\n"
                                                   "color: rgb(255, 255, 255)"))
        self.dev_copyright.setObjectName(_fromUtf8("dev_copyright"))
        self.dev_copyright_2 = QtGui.QLabel(self.AboutPage)
        self.dev_copyright_2.setGeometry(QtCore.QRect(328, 196, 190, 46))
        self.dev_copyright_2.setStyleSheet(_fromUtf8("background: transparent;\n"
                                                     "color: rgb(255, 255, 255)"))
        self.dev_copyright_2.setObjectName(_fromUtf8("dev_copyright_2"))
        self.dev_copyright_3 = QtGui.QLabel(self.AboutPage)
        self.dev_copyright_3.setGeometry(QtCore.QRect(328, 228, 190, 46))
        self.dev_copyright_3.setStyleSheet(_fromUtf8("background: transparent;\n"
                                                     "color: rgb(255, 255, 255)"))
        self.dev_copyright_3.setObjectName(_fromUtf8("dev_copyright_3"))
        self.label = QtGui.QLabel(self.AboutPage)
        self.label.setGeometry(QtCore.QRect(275, 54, 253, 188))
        self.label.setStyleSheet(_fromUtf8("background: transparent;"))
        self.label.setText(_fromUtf8(""))
        self.label.setPixmap(QtGui.QPixmap(_fromUtf8(":/coresec/im/csec.png")))
        self.label.setScaledContents(True)
        self.label.setObjectName(_fromUtf8("label"))
        self.dev_BGWhite = QtGui.QPushButton(self.AboutPage)
        self.dev_BGWhite.setGeometry(QtCore.QRect(-18, 87, 846, 119))
        self.dev_BGWhite.setStyleSheet(_fromUtf8("background: rgb(232, 232, 232)"))
        self.dev_BGWhite.setText(_fromUtf8(""))
        self.dev_BGWhite.setObjectName(_fromUtf8("dev_BGWhite"))
        self.dev_BGWhite.raise_()
        self.devtitle.raise_()
        self.dev_copyright.raise_()
        self.dev_copyright_2.raise_()
        self.dev_copyright_3.raise_()
        self.label.raise_()
        self.bmuPager.addWidget(self.AboutPage)
        self.verb = QtGui.QPushButton(BeastMaker)
        self.verb.setGeometry(QtCore.QRect(699, 122, 92, 23))
        font = QtGui.QFont()
        font.setPointSize(8)
        font.setBold(False)
        font.setWeight(50)
        font.setStyleStrategy(QtGui.QFont.PreferAntialias)
        self.verb.setFont(font)
        self.verb.setStyleSheet(_fromUtf8("QPushButton {\n"
                                          "color: #333;\n"
                                          "\n"
                                          "border: 1px solid #555;\n"
                                          "border-radius: 11px;\n"
                                          "padding: 5px;\n"
                                          "background: qradialgradient(cx: 0.3, cy: -0.4,\n"
                                          "fx: 0.3, fy: -0.4,\n"
                                          "radius: 1.35, stop: 0 #fff, stop: 1 #888);\n"
                                          "min-width: 80px;\n"
                                          "}\n"
                                          "\n"
                                          "\n"
                                          "QPushButton:hover {\n"
                                          "background: qradialgradient(cx: 0.3, cy: -0.4,\n"
                                          "fx: 0.3, fy: -0.4,\n"
                                          "radius: 1.35, stop: 0 #fff, stop: 1 #bbb);\n"
                                          "}\n"
                                          "\n"
                                          "QPushButton:pressed {\n"
                                          "background: qradialgradient(cx: 0.4, cy: -0.1,\n"
                                          "fx: 0.4, fy: -0.1,\n"
                                          "radius: 1.35, stop: 0 #fff, stop: 1 #ddd);\n"
                                          "}"))
        self.verb.setObjectName(_fromUtf8("verb"))
        self.updater = QtGui.QPushButton(BeastMaker)
        self.updater.setGeometry(QtCore.QRect(700, 150, 92, 23))
        font = QtGui.QFont()
        font.setPointSize(8)
        font.setBold(False)
        font.setWeight(50)
        font.setStyleStrategy(QtGui.QFont.PreferAntialias)
        self.updater.setFont(font)
        self.updater.setStyleSheet(_fromUtf8("QPushButton {\n"
                                             "color: rgb(176, 176, 176);\n"
                                             "\n"
                                             "border: 1px solid #555;\n"
                                             "border-radius: 11px;\n"
                                             "padding: 5px;\n"
                                             "background:rgb(88, 88, 88);\n"
                                             "min-width: 80px;\n"
                                             "}\n"
                                             "\n"
                                             "\n"
                                             "QPushButton:hover {\n"
                                             "background: qradialgradient(cx: 0.3, cy: -0.4,\n"
                                             "fx: 0.3, fy: -0.4,\n"
                                             "radius: 1.35, stop: 0 #fff, stop: 1 #bbb);\n"
                                             "color: rgb(50, 50, 50);\n"
                                             "}\n"
                                             "\n"
                                             "QPushButton:pressed {\n"
                                             "background: qradialgradient(cx: 0.4, cy: -0.1,\n"
                                             "fx: 0.4, fy: -0.1,\n"
                                             "radius: 1.35, stop: 0 #fff, stop: 1 #ddd);\n"
                                             "color: rgb(50, 50, 50);\n"
                                             "}"))
        self.updater.setObjectName(_fromUtf8("updater"))

        self.retranslateUi(BeastMaker)
        self.bmuPager.setCurrentIndex(0)
        self.vaultCat.setCurrentIndex(0)
        self.vaultCat.layout().setSpacing(0)
        self.AccManagerTabber.setCurrentIndex(1)
        QtCore.QMetaObject.connectSlotsByName(BeastMaker)

    def retranslateUi(self, BeastMaker):
        BeastMaker.setWindowTitle(_translate("BeastMaker", "BeastMaker - BeastMU Online | CoreSEC Softwares, CA", None))
        self.ItemMakerButton.setText(_translate("BeastMaker", "Item  Maker", None))
        self.PackageMakerButton.setText(_translate("BeastMaker", "Package Maker", None))
        self.AccountManagerButton.setText(_translate("BeastMaker", "Account Manager", None))
        self.ConnectionButton.setText(_translate("BeastMaker", "Connection", None))
        self.ResourcesButton.setText(_translate("BeastMaker", "Resources", None))
        self.pushButton.setText(_translate("BeastMaker", "Miscellaneous", None))
        self.imtitle.setText(_translate("BeastMaker", "Item Maker", None))
        self.Details_Name.setText(_translate("BeastMaker", "                          Excellent Bronze Helm", None))
        self.AppendOnOff.setText(_translate("BeastMaker", "Append: Off", None))
        self.SafetyOnOff.setText(_translate("BeastMaker", "Safety: On", None))
        self.Details_Options.setHtml(_translate("BeastMaker",
                                                "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
                                                "<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
                                                "p, li { white-space: pre-wrap; }\n"
                                                "</style></head><body style=\" font-family:\'MS Shell Dlg 2\'; font-size:7pt; font-weight:400; font-style:normal;\">\n"
                                                "<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt; font-weight:600; color:#aaaa7f;\">Caution!</span></p>\n"
                                                "<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt; color:#aaaa7f;\">Every Items Generated and Processed</span></p>\n"
                                                "<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt; color:#aaaa7f;\">will Replace the Old Database contents!</span></p>\n"
                                                "<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt; color:#aaaa7f;\"><br /></p></body></html>",
                                                None))
        self.SocketOptions_Val2.setItemText(0, _translate("BeastMaker", "Socket II", None))
        self.SocketOptions.setItemText(0, _translate("BeastMaker", "Sockets: None", None))
        self.SocketOptions.setItemText(1, _translate("BeastMaker", "Sockets: Enabled", None))
        self.AncientOptions.setItemText(0, _translate("BeastMaker", "Ancient: None", None))
        self.AncientOptions.setItemText(1, _translate("BeastMaker", "Cache", None))
        self.SocketOptions_Val3.setItemText(0, _translate("BeastMaker", "Socket III", None))
        self.SocketOptions_Val1.setItemText(0, _translate("BeastMaker", "Socket I", None))
        self.Serialized.setText(_translate("BeastMaker", "                      A96BFF001EC60CC00900", None))
        self.AppendOnOff_2.setText(_translate("BeastMaker", "Restore", None))
        self.vaultCat.setItemText(self.vaultCat.indexOf(self.wareMode), _translate("BeastMaker", "Details", None))
        self.slot_1_1.setText(_translate("BeastMaker", "None", None))
        self.slot_1_2.setText(_translate("BeastMaker", "None", None))
        self.slot_1_3.setText(_translate("BeastMaker", "None", None))
        self.slot_1_4.setText(_translate("BeastMaker", "None", None))
        self.slot_2_3.setText(_translate("BeastMaker", "None", None))
        self.slot_2_4.setText(_translate("BeastMaker", "None", None))
        self.slot_2_1.setText(_translate("BeastMaker", "None", None))
        self.slot_2_2.setText(_translate("BeastMaker", "None", None))
        self.slot_3_3.setText(_translate("BeastMaker", "None", None))
        self.slot_3_4.setText(_translate("BeastMaker", "None", None))
        self.slot_3_1.setText(_translate("BeastMaker", "None", None))
        self.slot_3_2.setText(_translate("BeastMaker", "None", None))
        self.slot_4_3.setText(_translate("BeastMaker", "None", None))
        self.slot_4_4.setText(_translate("BeastMaker", "None", None))
        self.slot_4_1.setText(_translate("BeastMaker", "None", None))
        self.slot_4_2.setText(_translate("BeastMaker", "None", None))
        self.warninglabel.setText(_translate("BeastMaker", "Empty your Inventory before Processing", None))
        self.vaultCat.setItemText(self.vaultCat.indexOf(self.inventMode), _translate("BeastMaker", "Inventory", None))
        self.charMode_helm.setText(_translate("BeastMaker", "Helm", None))
        self.charMode_armor.setText(_translate("BeastMaker", "Armor", None))
        self.charMode_pants.setText(_translate("BeastMaker", "Pants", None))
        self.charMode_weap2.setText(_translate("BeastMaker", "Weapon (R)", None))
        self.charMode_weap1.setText(_translate("BeastMaker", "Weapon (L)", None))
        self.charMode_boot.setText(_translate("BeastMaker", "Boot", None))
        self.charMode_gloves.setText(_translate("BeastMaker", "Gloves", None))
        self.charMode_wing.setText(_translate("BeastMaker", "Wing", None))
        self.charMode_pet.setText(_translate("BeastMaker", "Pet", None))
        self.charMode_pendant.setText(_translate("BeastMaker", "Pen", None))
        self.charMode_ring1.setText(_translate("BeastMaker", "Ring", None))
        self.charMode_ring2.setText(_translate("BeastMaker", "Ring", None))
        self.vaultCat.setItemText(self.vaultCat.indexOf(self.charMode), _translate("BeastMaker", "Character", None))
        self.Message.setHtml(_translate("BeastMaker",
                                        "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
                                        "<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
                                        "p, li { white-space: pre-wrap; }\n"
                                        "</style></head><body style=\" font-family:\'MS Shell Dlg 2\'; font-size:8.25pt; font-weight:400; font-style:normal;\">\n"
                                        "<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt; font-weight:600; color:#ebebeb;\">Items Database</span><span style=\" font-size:8pt; color:#ebebeb;\"> Loaded...</span></p>\n"
                                        "<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt; color:#ebebeb;\">SQLServer:  </span><span style=\" font-size:8pt; font-weight:600; color:#54d920;\">Connected!</span></p>\n"
                                        "<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt; font-weight:600; color:#54d920;\"><br /></p>\n"
                                        "<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt;\"><br /></p></body></html>",
                                        None))
        self.CancelNow.setText(_translate("BeastMaker", "Command: On", None))
        self.ProcessAll.setText(_translate("BeastMaker", "Process", None))
        self.CatViewLabel.setText(_translate("BeastMaker", "Categories", None))
        self.contentsLabel.setText(_translate("BeastMaker", "Contents", None))
        self.CategoriesView.setItemText(0, _translate("BeastMaker", "Choose", None))
        self.Option_Zen.setText(_translate("BeastMaker", "Zen Bonus", None))
        self.Option_DSR.setText(_translate("BeastMaker", "Defence Success Rate", None))
        self.Option_Reflect.setText(_translate("BeastMaker", "Reflect Damage Rate", None))
        self.Option_DD.setText(_translate("BeastMaker", "Damage Decrease Rate", None))
        self.Option_Mana.setText(_translate("BeastMaker", "Mana Increase", None))
        self.Option_HP.setText(_translate("BeastMaker", "HP Increase", None))
        self.Option_Yellow1.setItemText(0, _translate("BeastMaker", "Yellow Option", None))
        self.Option_Yellow2.setItemText(0, _translate("BeastMaker", "Yellow Option Plus", None))
        self.seper.setText(_translate("BeastMaker", "________________________________", None))
        self.Option_380.setText(_translate("BeastMaker", "Refinery Level 380", None))
        self.OptionsLabelDown.setText(_translate("BeastMaker", "Options", None))
        self.plusLabel.setText(_translate("BeastMaker", "Level / Plus", None))
        self.Options_OptionValue.setText(_translate("BeastMaker", " + 0", None))
        self.Options_PlusValue.setText(_translate("BeastMaker", " + 0", None))
        self.Option_Zen_2.setText(_translate("BeastMaker", "Luck", None))
        self.AccountLabel.setText(_translate("BeastMaker", "Account:", None))
        self.AccountValue.setItemText(0, _translate("BeastMaker", "Choose Account", None))
        self.AccountValue.setItemText(1, _translate("BeastMaker", "TestAccount", None))
        self.lineEdit.setText(_translate("BeastMaker", "/beastitem 0 0 0 0 0 0 0 0 ", None))
        self.ProcessAll_2.setText(_translate("BeastMaker", "Copy", None))
        self.pmtitle.setText(_translate("BeastMaker", "Package Manager", None))
        __sortingEnabled = self.Package_List.isSortingEnabled()
        self.Package_List.setSortingEnabled(False)
        item = self.Package_List.item(0)
        item.setText(_translate("BeastMaker", "None", None))
        self.Package_List.setSortingEnabled(__sortingEnabled)
        self.Package_Choose.setItemText(0, _translate("BeastMaker", "Choose Package", None))
        self.label_10.setText(_translate("BeastMaker",
                                         "Information: BeastData (Python Module) is required or a BeastData.py Source modified file.",
                                         None))
        self.ProcessPackage.setText(_translate("BeastMaker", "Process Now", None))
        self.PackageMessageIncludes.setHtml(_translate("BeastMaker",
                                                       "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
                                                       "<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
                                                       "p, li { white-space: pre-wrap; }\n"
                                                       "</style></head><body style=\" font-family:\'MS Shell Dlg 2\'; font-size:8.25pt; font-weight:400; font-style:normal;\">\n"
                                                       "<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt; font-weight:600;\">PACKAGE INCLUDES:</span></p>\n"
                                                       "<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt; font-weight:600;\"><br /></p>\n"
                                                       "<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt; font-weight:600;\">- Full Gear</span></p>\n"
                                                       "<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt; font-weight:600;\">**</span></p></body></html>",
                                                       None))
        self.Package_UsernameList.setPlaceholderText(_translate("BeastMaker", "Username?", None))
        self.Package_EditIncludes.setText(_translate("BeastMaker", "Edit Includes", None))
        self.Package_ChooseClass.setItemText(0, _translate("BeastMaker", "Choose Class", None))
        self.amtitle.setText(_translate("BeastMaker", "Account Manager", None))
        __sortingEnabled = self.AccMan_UsersList.isSortingEnabled()
        self.AccMan_UsersList.setSortingEnabled(False)
        item = self.AccMan_UsersList.item(0)
        item.setText(_translate("BeastMaker", "asdasdasd", None))
        item = self.AccMan_UsersList.item(1)
        item.setText(_translate("BeastMaker", "123", None))
        item = self.AccMan_UsersList.item(2)
        item.setText(_translate("BeastMaker", "12", None))
        item = self.AccMan_UsersList.item(3)
        item.setText(_translate("BeastMaker", "31", None))
        item = self.AccMan_UsersList.item(4)
        item.setText(_translate("BeastMaker", "23", None))
        item = self.AccMan_UsersList.item(5)
        item.setText(_translate("BeastMaker", "1", None))
        item = self.AccMan_UsersList.item(6)
        item.setText(_translate("BeastMaker", "23", None))
        item = self.AccMan_UsersList.item(7)
        item.setText(_translate("BeastMaker", "12", None))
        item = self.AccMan_UsersList.item(8)
        item.setText(_translate("BeastMaker", "3", None))
        item = self.AccMan_UsersList.item(9)
        item.setText(_translate("BeastMaker", "12", None))
        item = self.AccMan_UsersList.item(10)
        item.setText(_translate("BeastMaker", "3", None))
        item = self.AccMan_UsersList.item(11)
        item.setText(_translate("BeastMaker", "12", None))
        item = self.AccMan_UsersList.item(12)
        item.setText(_translate("BeastMaker", "3", None))
        item = self.AccMan_UsersList.item(13)
        item.setText(_translate("BeastMaker", "12", None))
        item = self.AccMan_UsersList.item(14)
        item.setText(_translate("BeastMaker", "31", None))
        item = self.AccMan_UsersList.item(15)
        item.setText(_translate("BeastMaker", "2", None))
        item = self.AccMan_UsersList.item(16)
        item.setText(_translate("BeastMaker", "31", None))
        item = self.AccMan_UsersList.item(17)
        item.setText(_translate("BeastMaker", "23", None))
        item = self.AccMan_UsersList.item(18)
        item.setText(_translate("BeastMaker", "123", None))
        item = self.AccMan_UsersList.item(19)
        item.setText(_translate("BeastMaker", "asdasdasd", None))
        self.AccMan_UsersList.setSortingEnabled(__sortingEnabled)
        self.Account_GSeperator.setTitle(_translate("BeastMaker", "Account Information", None))
        self.accman_accin_label.setText(_translate("BeastMaker", "Username:", None))
        self.AccMan_info_username.setText(_translate("BeastMaker", "None", None))
        self.accman_accin_label_2.setText(_translate("BeastMaker", "Password:", None))
        self.AccMan_info_password.setText(_translate("BeastMaker", "None", None))
        self.accman_accin_label_4.setText(_translate("BeastMaker", "Email:", None))
        self.AccMan_info_email.setText(_translate("BeastMaker", "None", None))
        self.accman_accin_label_5.setText(_translate("BeastMaker", "Join Date:", None))
        self.AccMan_info_joindate.setText(_translate("BeastMaker", "None", None))
        self.accman_accin_label_11.setText(_translate("BeastMaker", "S-Answer:", None))
        self.AccMan_info_secanswer.setText(_translate("BeastMaker", "None", None))
        self.accman_accin_label_13.setText(_translate("BeastMaker", "VIP Money:", None))
        self.AccMan_info_vipmoney.setText(_translate("BeastMaker", "None", None))
        self.accman_accin_label_15.setText(_translate("BeastMaker", "Beast Serial:", None))
        self.AccMan_info_accserial.setText(_translate("BeastMaker", "# Username", None))
        self.accman_accin_label_17.setText(_translate("BeastMaker", "Country:", None))
        self.AccMan_info_country.setText(_translate("BeastMaker", "None", None))
        self.accman_accin_label_19.setText(_translate("BeastMaker", "Characters:", None))
        self.AccMan_info_charlen.setText(_translate("BeastMaker", "None", None))
        self.accman_accin_label_21.setText(_translate("BeastMaker", "Gender:", None))
        self.AccMan_info_gender.setText(_translate("BeastMaker", "None", None))
        self.charGSeperator.setTitle(_translate("BeastMaker", "Character Details", None))
        self.AccMan_ChooseChar.setItemText(0, _translate("BeastMaker", "Choose Character", None))
        self.AccMan_ChooseChar.setItemText(1, _translate("BeastMaker", "Cache", None))
        item = self.AccMan_charDetails_tableWidget.verticalHeaderItem(0)
        item.setText(_translate("BeastMaker", "Level", None))
        item = self.AccMan_charDetails_tableWidget.verticalHeaderItem(1)
        item.setText(_translate("BeastMaker", "Master Level", None))
        item = self.AccMan_charDetails_tableWidget.verticalHeaderItem(2)
        item.setText(_translate("BeastMaker", "Free Points", None))
        item = self.AccMan_charDetails_tableWidget.verticalHeaderItem(3)
        item.setText(_translate("BeastMaker", "Beast Zen", None))
        item = self.AccMan_charDetails_tableWidget.verticalHeaderItem(4)
        item.setText(_translate("BeastMaker", "Resets", None))
        item = self.AccMan_charDetails_tableWidget.verticalHeaderItem(5)
        item.setText(_translate("BeastMaker", "PC Points", None))
        item = self.AccMan_charDetails_tableWidget.verticalHeaderItem(6)
        item.setText(_translate("BeastMaker", "Strenght", None))
        item = self.AccMan_charDetails_tableWidget.verticalHeaderItem(7)
        item.setText(_translate("BeastMaker", "Agility", None))
        item = self.AccMan_charDetails_tableWidget.verticalHeaderItem(8)
        item.setText(_translate("BeastMaker", "Vitality", None))
        item = self.AccMan_charDetails_tableWidget.verticalHeaderItem(9)
        item.setText(_translate("BeastMaker", "Energy", None))
        item = self.AccMan_charDetails_tableWidget.verticalHeaderItem(10)
        item.setText(_translate("BeastMaker", "Command", None))
        item = self.AccMan_charDetails_tableWidget.verticalHeaderItem(11)
        item.setText(_translate("BeastMaker", "Game Master", None))
        item = self.AccMan_charDetails_tableWidget.verticalHeaderItem(12)
        item.setText(_translate("BeastMaker", "Blocked", None))
        item = self.AccMan_charDetails_tableWidget.horizontalHeaderItem(0)
        item.setText(_translate("BeastMaker", "Strenght", None))
        __sortingEnabled = self.AccMan_charDetails_tableWidget.isSortingEnabled()
        self.AccMan_charDetails_tableWidget.setSortingEnabled(False)
        item = self.AccMan_charDetails_tableWidget.item(0, 0)
        item.setText(_translate("BeastMaker", "0", None))
        item = self.AccMan_charDetails_tableWidget.item(1, 0)
        item.setText(_translate("BeastMaker", "0", None))
        item = self.AccMan_charDetails_tableWidget.item(2, 0)
        item.setText(_translate("BeastMaker", "0", None))
        item = self.AccMan_charDetails_tableWidget.item(3, 0)
        item.setText(_translate("BeastMaker", "0", None))
        item = self.AccMan_charDetails_tableWidget.item(4, 0)
        item.setText(_translate("BeastMaker", "0", None))
        item = self.AccMan_charDetails_tableWidget.item(5, 0)
        item.setText(_translate("BeastMaker", "0", None))
        item = self.AccMan_charDetails_tableWidget.item(6, 0)
        item.setText(_translate("BeastMaker", "0", None))
        item = self.AccMan_charDetails_tableWidget.item(7, 0)
        item.setText(_translate("BeastMaker", "0", None))
        item = self.AccMan_charDetails_tableWidget.item(8, 0)
        item.setText(_translate("BeastMaker", "0", None))
        item = self.AccMan_charDetails_tableWidget.item(9, 0)
        item.setText(_translate("BeastMaker", "0", None))
        item = self.AccMan_charDetails_tableWidget.item(10, 0)
        item.setText(_translate("BeastMaker", "0", None))
        item = self.AccMan_charDetails_tableWidget.item(11, 0)
        item.setText(_translate("BeastMaker", "No", None))
        item = self.AccMan_charDetails_tableWidget.item(12, 0)
        item.setText(_translate("BeastMaker", "No", None))
        self.AccMan_charDetails_tableWidget.setSortingEnabled(__sortingEnabled)
        self.AccMan_ChooseAcc_EditChar_2.setItemText(0, _translate("BeastMaker", "Choose Account", None))
        self.AccMan_ChooseAcc_EditChar_2.setItemText(1, _translate("BeastMaker", "asdasdasd", None))
        self.AccMan_ChooseAcc_EditChar_2.setItemText(2, _translate("BeastMaker", "sdasdasdasd", None))
        self.AccManagerTabber.setTabText(self.AccManagerTabber.indexOf(self.AccManager_Info),
                                         _translate("BeastMaker", "Account Information", None))
        self.AccMan_EditChar_Choose.setItemText(0, _translate("BeastMaker", "Choose", None))
        self.AccMan_EditChar_Choose.setItemText(1, _translate("BeastMaker", "Cache", None))
        self.AccMan_ChooseAcc_EditChar.setItemText(0, _translate("BeastMaker", "Choose Account", None))
        self.AccMan_ChooseAcc_EditChar.setItemText(1, _translate("BeastMaker", "Cache", None))
        self.AccMan_EditChar_SaveAll.setText(_translate("BeastMaker", "Save All", None))
        self.AccMan_EditChar_GSepCharStats.setTitle(_translate("BeastMaker", "Edit Account", None))
        item = self.AccMan_Edit_Account.verticalHeaderItem(0)
        item.setText(_translate("BeastMaker", "Username", None))
        item = self.AccMan_Edit_Account.verticalHeaderItem(1)
        item.setText(_translate("BeastMaker", "Password", None))
        item = self.AccMan_Edit_Account.verticalHeaderItem(2)
        item.setText(_translate("BeastMaker", "Join Date", None))
        item = self.AccMan_Edit_Account.verticalHeaderItem(3)
        item.setText(_translate("BeastMaker", "Beast Serial", None))
        item = self.AccMan_Edit_Account.verticalHeaderItem(4)
        item.setText(_translate("BeastMaker", "Banned:", None))
        item = self.AccMan_Edit_Account.horizontalHeaderItem(0)
        item.setText(_translate("BeastMaker", "Strenght", None))
        __sortingEnabled = self.AccMan_Edit_Account.isSortingEnabled()
        self.AccMan_Edit_Account.setSortingEnabled(False)
        item = self.AccMan_Edit_Account.item(1, 0)
        item.setText(_translate("BeastMaker", "...", None))
        item = self.AccMan_Edit_Account.item(2, 0)
        item.setText(_translate("BeastMaker", "11/11/11", None))
        item = self.AccMan_Edit_Account.item(3, 0)
        item.setText(_translate("BeastMaker", "143", None))
        item = self.AccMan_Edit_Account.item(4, 0)
        item.setText(_translate("BeastMaker", "No", None))
        self.AccMan_Edit_Account.setSortingEnabled(__sortingEnabled)
        self.AccMan_EditChar_CPoints.setTitle(_translate("BeastMaker", "Edit Credits", None))
        self.AccMan_EditChar_Credits.setPlaceholderText(_translate("BeastMaker", "Current: 1200 (Credits)", None))
        self.AccMan_EditChar_PCPoints.setPlaceholderText(_translate("BeastMaker", "Current: 1200 (PCP)", None))
        self.AccMan_EditChar_WCoin.setPlaceholderText(_translate("BeastMaker", "Current: 1200 (WCoin)", None))
        self.AccMan_EditChar_BPoints.setTitle(_translate("BeastMaker", "Upgrade Class", None))
        self.AccMan_EditChar_Evolve.setItemText(0, _translate("BeastMaker", "Blade Knight", None))
        self.AccMan_EditChar_Lottery.setTitle(_translate("BeastMaker", "Trans-Gender", None))
        self.AccMan_EditChar_ChangeClassChar.setItemText(0, _translate("BeastMaker", "Character", None))
        self.label_7.setText(_translate("BeastMaker", "TO", None))
        self.AccMan_EditChar_ChangeClass_Class.setItemText(0, _translate("BeastMaker", "Class", None))
        self.AccMan_EditChar_GSepMisc.setTitle(_translate("BeastMaker", "Edit Character", None))
        item = self.AccMan_Edit_Char.verticalHeaderItem(0)
        item.setText(_translate("BeastMaker", "Level", None))
        item = self.AccMan_Edit_Char.verticalHeaderItem(1)
        item.setText(_translate("BeastMaker", "Master Level", None))
        item = self.AccMan_Edit_Char.verticalHeaderItem(2)
        item.setText(_translate("BeastMaker", "Free Points", None))
        item = self.AccMan_Edit_Char.verticalHeaderItem(3)
        item.setText(_translate("BeastMaker", "Beast Zen", None))
        item = self.AccMan_Edit_Char.verticalHeaderItem(4)
        item.setText(_translate("BeastMaker", "Resets", None))
        item = self.AccMan_Edit_Char.verticalHeaderItem(5)
        item.setText(_translate("BeastMaker", "PC Points", None))
        item = self.AccMan_Edit_Char.verticalHeaderItem(6)
        item.setText(_translate("BeastMaker", "Strenght", None))
        item = self.AccMan_Edit_Char.verticalHeaderItem(7)
        item.setText(_translate("BeastMaker", "Agility", None))
        item = self.AccMan_Edit_Char.verticalHeaderItem(8)
        item.setText(_translate("BeastMaker", "Vitality", None))
        item = self.AccMan_Edit_Char.verticalHeaderItem(9)
        item.setText(_translate("BeastMaker", "Energy", None))
        item = self.AccMan_Edit_Char.verticalHeaderItem(10)
        item.setText(_translate("BeastMaker", "Command", None))
        item = self.AccMan_Edit_Char.verticalHeaderItem(11)
        item.setText(_translate("BeastMaker", "Game Master", None))
        item = self.AccMan_Edit_Char.verticalHeaderItem(12)
        item.setText(_translate("BeastMaker", "Blocked", None))
        item = self.AccMan_Edit_Char.horizontalHeaderItem(0)
        item.setText(_translate("BeastMaker", "Strenght", None))
        __sortingEnabled = self.AccMan_Edit_Char.isSortingEnabled()
        self.AccMan_Edit_Char.setSortingEnabled(False)
        item = self.AccMan_Edit_Char.item(0, 0)
        item.setText(_translate("BeastMaker", "0", None))
        item = self.AccMan_Edit_Char.item(1, 0)
        item.setText(_translate("BeastMaker", "0", None))
        item = self.AccMan_Edit_Char.item(2, 0)
        item.setText(_translate("BeastMaker", "0", None))
        item = self.AccMan_Edit_Char.item(3, 0)
        item.setText(_translate("BeastMaker", "0", None))
        item = self.AccMan_Edit_Char.item(4, 0)
        item.setText(_translate("BeastMaker", "0", None))
        item = self.AccMan_Edit_Char.item(5, 0)
        item.setText(_translate("BeastMaker", "0", None))
        item = self.AccMan_Edit_Char.item(6, 0)
        item.setText(_translate("BeastMaker", "0", None))
        item = self.AccMan_Edit_Char.item(7, 0)
        item.setText(_translate("BeastMaker", "0", None))
        item = self.AccMan_Edit_Char.item(8, 0)
        item.setText(_translate("BeastMaker", "0", None))
        item = self.AccMan_Edit_Char.item(9, 0)
        item.setText(_translate("BeastMaker", "0", None))
        item = self.AccMan_Edit_Char.item(10, 0)
        item.setText(_translate("BeastMaker", "0", None))
        item = self.AccMan_Edit_Char.item(11, 0)
        item.setText(_translate("BeastMaker", "No", None))
        item = self.AccMan_Edit_Char.item(12, 0)
        item.setText(_translate("BeastMaker", "No", None))
        self.AccMan_Edit_Char.setSortingEnabled(__sortingEnabled)
        self.AccMan_EditChar_GSepGuild.setTitle(_translate("BeastMaker", "Edit Guild", None))
        item = self.AccMan_Edit_Char_2.verticalHeaderItem(0)
        item.setText(_translate("BeastMaker", "Master", None))
        item = self.AccMan_Edit_Char_2.verticalHeaderItem(1)
        item.setText(_translate("BeastMaker", "Score", None))
        item = self.AccMan_Edit_Char_2.horizontalHeaderItem(0)
        item.setText(_translate("BeastMaker", "Name", None))
        __sortingEnabled = self.AccMan_Edit_Char_2.isSortingEnabled()
        self.AccMan_Edit_Char_2.setSortingEnabled(False)
        item = self.AccMan_Edit_Char_2.item(0, 0)
        item.setText(_translate("BeastMaker", "Mark", None))
        item = self.AccMan_Edit_Char_2.item(1, 0)
        item.setText(_translate("BeastMaker", "1", None))
        self.AccMan_Edit_Char_2.setSortingEnabled(__sortingEnabled)
        self.AccMan_EditChar_SaveAll_3.setText(_translate("BeastMaker", "Show Members", None))
        self.AccMan_EditChar_Evolve_2.setItemText(0, _translate("BeastMaker", "Name", None))
        self.AccMan_EditChar_Choose_3.setItemText(0, _translate("BeastMaker", "Action", None))
        self.AccMan_EditChar_Choose_3.setItemText(1, _translate("BeastMaker", "None", None))
        self.AccMan_EditChar_Choose_3.setItemText(2, _translate("BeastMaker", "Delete", None))
        self.AccMan_EditChar_Choose_3.setItemText(3, _translate("BeastMaker", "Set PK", None))
        self.AccMan_EditChar_Choose_3.setItemText(4, _translate("BeastMaker", "Clear PK", None))
        self.AccMan_EditChar_Choose_3.setItemText(5, _translate("BeastMaker", "Set to GM", None))
        self.AccMan_EditChar_Choose_3.setItemText(6, _translate("BeastMaker", "Set to Normal", None))
        self.AccMan_EditChar_Choose_3.setItemText(7, _translate("BeastMaker", "Set to VIP", None))
        self.AccMan_EditChar_Restore.setText(_translate("BeastMaker", "Restore to Default", None))
        self.AccManagerTabber.setTabText(self.AccManagerTabber.indexOf(self.AccManager_EditCharacter),
                                         _translate("BeastMaker", "Edit Character", None))
        self.AccManagerTabber.setTabText(self.AccManagerTabber.indexOf(self.AccManagement_Ban),
                                         _translate("BeastMaker", "Game Master Resources", None))
        self.label_8.setText(_translate("BeastMaker", "Useful for Taking notes and Saving Information", None))
        self.Notes_NotesSave.setText(_translate("BeastMaker", "Save Note", None))
        self.AccMan_EditChar_Choose_4.setItemText(0, _translate("BeastMaker", "Load Notes", None))
        self.AccMan_EditChar_Choose_4.setItemText(1, _translate("BeastMaker", "bnotes-11/11/20", None))
        self.AccManagerTabber.setTabText(self.AccManagerTabber.indexOf(self.AccManager_Skill),
                                         _translate("BeastMaker", "Notes", None))
        self.ctitle.setText(_translate("BeastMaker", "Connection", None))
        self.label_2.setText(_translate("BeastMaker", "MSSQL Version:", None))
        self.label_3.setText(_translate("BeastMaker", "Username:", None))
        self.label_4.setText(_translate("BeastMaker", "Password:", None))
        self.label_5.setText(_translate("BeastMaker", "IP Address/Instance:", None))
        self.ConnectionValue.setPlainText(_translate("BeastMaker", "BeastMU\\SQLEXPRESS", None))
        self.Connection_Version.setPlainText(_translate("BeastMaker", "2005", None))
        self.Connection_userName.setPlainText(_translate("BeastMaker", "sa", None))
        self.Connection_passWord.setText(_translate("BeastMaker", "mypassword", None))
        self.ConnectNow.setText(_translate("BeastMaker", "CONNECT NOW", None))
        self.rtitle.setText(_translate("BeastMaker", "Resources", None))
        self.Resources_CustomQ.setText(_translate("BeastMaker", "Execute Queries", None))
        self.tsqlBox.setHtml(_translate("BeastMaker",
                                        "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
                                        "<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
                                        "p, li { white-space: pre-wrap; }\n"
                                        "</style></head><body style=\" font-family:\'MS Shell Dlg 2\'; font-size:8.25pt; font-weight:400; font-style:normal;\">\n"
                                        "<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt;\">TSQL Queries Compatible...</span></p></body></html>",
                                        None))
        self.mtitle.setText(_translate("BeastMaker", "Miscellaneous", None))
        self.devtitle.setText(_translate("BeastMaker", "Developer", None))
        self.dev_copyright.setText(
            _translate("BeastMaker", "Copyright © CoreSEC Softwares, Ca - Mark A. R. Pequeras", None))
        self.dev_copyright_2.setText(_translate("BeastMaker", "Developed for BeastMU Online", None))
        self.dev_copyright_3.setText(_translate("BeastMaker", "www.coresecsoftware.com", None))
        self.verb.setText(_translate("BeastMaker", "Version", None))
        self.updater.setText(_translate("BeastMaker", "Updates", None))


        # // Line of Recovery for UI Changes

        BeastMaker.setStyleSheet(_fromUtf8("QWidget {\n"
                                           "border: none;\n"
                                           "background-image: url(:/coresec/im/bg.png)\n"
                                           "} QToolTip { color: #00FF00; background-color: #180000; border: 3px solid white;}"))
        try:
            import beasthook #// BeastMU Online Hook (BeastEMUlator)

        except ImportError:
            pass  # none for BeastEMUlator Engine

        #// Globals Class Prop
        self.Auth = None
        self.AuthI = None

        self.AccountsCache = None
        self.CharactersCache = None
        self.ItemCache = None
        self.ItemSerialCache = None

        self.BeastOptions = self.ParseOptions()

        self.con = None
        self.Cursor = None
        self.Connection = None
        self.BMLoader()  #// Function for Calling other functions for Initialiation.
        self.Packages = {}
        self.Cache = {"helm": "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF", "armor": "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF",
                      "gloves": "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF", "pants": "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF",
                      "boots": "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF",
                      "pet": "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF", "wing": "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF",
                      "pendant": "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF", "ring1": "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF",
                      "ring2": "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF", "wep1": "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF",
                      "wep2": "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF", }

    # / Custom Resources
    global iIndex, iPath, iItem, iParse

    iIndex = 0      # // Pattern of Index (folder)
    iPath = None    # // Item Path (Image)
    iItem = 0       # // Item Index
    iParse = None   # // Parsed Codes.

    def LoadCategories(self):
        """
        Categories Loader
        """
        try:
            from Data.Database import Plus as PlusCon
            from Data.Database import Sockets as SocketsCon
            from Data.Database import Weapons as WeaponsCon
            from Data.Database import Yellow as YellowCon
            from Data.Database import Items as ItemsCon

            Cats = ItemsCon.Menus
            Yell = [ YellowCon.Gear, YellowCon.Weapons ]
            MaxPlus = PlusCon.Plus
            MaxOpts = PlusCon.PlusOptions
            self.CategoriesView.addItems(Cats)
            self.Option_Yellow1.addItems(Yell[ 0 ])
            self.Option_Yellow2.addItems(Yell[ 0 ])
            self.Option_Plus.setMaximum(13 if MaxPlus >= 16 else MaxPlus)
            self.Option_Options.setMaximum(28)
            self.Message.setHtml("<p><span style='color:#33cc00;'>Items Data Loaded!</span></p>")

        except:
            print "Missing/Error Configuration Files!"

    def Logger(self, msg, clear=False):
        "Message Maker"
        if clear:
            self.Message.clear()

        self.Message.append("\n<p><span style='color:#33cc00;'>{m}</span></p>".format(m=msg))

    def LoadSocketsAndAcients(self):
        """
        Sockets and Ancients Options Loader
        """
        Sock = self.SocketOptions
        Anc = self.AncientOptions

        try:
            from Data.Database.Ancients import Ancients
            from Data.Database.Sockets import Sockets

            AcCache = [ ]
            for Ac in Ancients.iterkeys():
                AcCache.append(Ac)

            Anc.addItems(AcCache)
            Sock.clear()
            Sock.addItems(["Disabled","Enabled"])
            self.Message.append("<p><span style='color:#33cc00;'>S/A Options Loaded!</span></p>")

        except:
            self.Message.append("Error: Loading (Sockets/Ancients) Options")

    def Load(self):
        "Called when Authenticated"
        BeastMaker.show()

    def ParseOptions(self):
        "Option Parser"
        mainPath = 'Data/Database/Config/'
        try:
            from Data.Database import cred  # remove on release.
            SQLConnection = cred.creds  # // Local,Remote Address or Instance/Domain
            SQLUser = cred.credu  # // MSSQL Username
            SQLPass = cred.cred  # // MSSQL Password
        except:
            SQLConnection = open(mainPath + 'SQLConnection.c', 'r').read()  # // Local,Remote Address or Instance/Domain
            SQLUser = open(mainPath + 'SQLUser.c', 'r').read()  # // MSSQL Username
            SQLPass = open(mainPath + 'SQLPass.c', 'r').read()  # // MSSQL Password

        AutoAuth = open(mainPath + 'AutoAuth.c', 'r').read()  # True/False
        SQLVersion = open(mainPath + 'SQLVersion.c', 'r').read() # // MSSQL Version (PyOdbc has the Options for this)
        SQLDbase = open(mainPath + 'SQLDBase.c', 'r').read() # // MSSQ DBase

        if len(SQLDbase) < 1:
            SQLDbase = 'MuOnline'   # // Set the default DataBase if Database is None

        InstantOnly = open(mainPath + 'Dropper.c', 'r').read()  # // Only Dropper no DB Connection

        UseSerialAuth = open(mainPath + 'SerialAuth.c', 'r').read()  # // Serial Authentication instead of IP (Checks DB/No IP)

        IsSa = open(mainPath + 'ISSA.c', 'r').read()  # // True if Show Raw Private infos (such as Passwords, SAnswer)
        Evaled = eval(IsSa)
        IsSa = Evaled

        return [ SQLVersion, SQLConnection, SQLDbase, SQLUser, SQLPass, eval(AutoAuth), eval(InstantOnly),
                 eval(UseSerialAuth), IsSa ]

    def LoadConnection(self,Reload=False,ReloadVals=False):
        """
        Connection Loader
        """
        import pyodbc as Server
        from Data.Database import Connection
        import pyodbc as db
        import urllib

        self.Credentials = self.BeastOptions
        if self.Credentials:
            if Reload:
                for i in ReloadVals:
                    print i
                self.con = db.connect(
                    'DRIVER={driver};SERVER={ip};DATABASE={bdb};UID={cu};PWD={pu}'.format(driver=ReloadVals[ 0 ],
                                                                                          ip=ReloadVals[ 1 ],
                                                                                          bdb=ReloadVals[ 2 ],
                                                                                          cu=ReloadVals[ 3 ],
                                                                                          pu=ReloadVals[ 4 ]))
            else:
                self.con = db.connect(
                    'DRIVER={driver};SERVER={ip};DATABASE={bdb};UID={cu};PWD={pu}'.format(driver=self.Credentials[ 0 ],
                                                                                          ip=self.Credentials[ 1 ],
                                                                                          bdb=self.Credentials[ 2 ],
                                                                                          cu=self.Credentials[ 3 ],
                                                                                          pu=self.Credentials[ 4 ]))
            self.Cursor = self.con.cursor()

            if self.Credentials[ 4 ]:
                try:
                    self.Users = self.Cursor.execute("""
                    SELECT [memb___id]
      FROM [MuOnline].[dbo].[MEMB_INFO]""")
                    CleanStr = [ ]
                    for Users in self.Users:
                        Clean = str(Users).replace("(", "").replace(")", "").replace("'", "").replace(",", "")
                        CleanStr.append(Clean)
                    self.AccountsCache = CleanStr
                    self.AccountValue.addItems(CleanStr)
                    self.Message.append("<p><span style='color:#33cc00;'>Server Connected!</span></p>")

                    __GetMe = self.Cursor.execute("""
                    SELECT [BeastMaker_Allowed]
  FROM [MuOnline].[dbo].[BeastMaker]
                    """)
                    __whoMe = open("Data/connection.bm", "r")   # // 2 Sequence seperated by comma
                    wmCache = str(__whoMe.read()).split(",")
                    cont = eval(str(__GetMe.fetchall()[ int(wmCache[ 2 ]) ][ 0 ]))
                    _xn = wmCache[ 0 ].decode('base64')
                    _xi = wmCache[ 1 ].decode('base64')
                    ri = urllib.urlopen("http://ipinfo.io/ip").read()

                    def Verf():
                        if str(ri).strip() == str(_xi):
                            self.Load()
                        else:
                            pass

                    def VerfSer():
                        for K, V in cont.iteritems():
                            if K == _xn:
                                try:
                                    self.Auth, self.AuthI = _xn, "Serial Authenticate"
                                finally:
                                    self.Load()    # // Load() contains the Open for the GUi

                    if self.Credentials[ 7 ]:  # Serial Authentication, No IP Checks
                        VerfSer()

                    else:
                        for K, V in cont.iteritems():
                            if K == _xn and V == _xi:
                                self.Auth, self.AuthI = _xn, _xi
                                Verf()

                except:
                    self.Connection = None
                    self.Message.append("Error: Connection Failed! ")

            else:
                self.Connection = None
                self.Message.append("No Server Connection...")

        else:
            self.Message.append("<p><span style='color:#33cc00;'>Server not Connected! Dropper Mode Only</span></p>")

    def ReStructHarm(self):
        "Restructuring Harmony Option"
        self.Option_Yellow2.clear()
        Options = [ ]
        Stop = 17
        Start = 0
        while Start != 16:
            Options.append("Ups " + str(Start))
            Start += 1
        self.Option_Yellow2.addItems(Options)


    def BMLoader(self):
        """
        Initializer (BeastMakerLoader)
        """
        self.SocketEngine(init=True) # Load all Sockets
        self.LoadCategories()  # Load Categories and Items DB
        self.LoadSocketsAndAcients()  # Load Ancients and Sockets Options
        self.LoadConnection()  # Connection to DB Loader
        self.ReStructHarm()
        self.Option_Zen_2.setChecked(True)

        # //  Function Calls

        BMaker = QtCore.QObject.connect
        BMaker(self.ProcessAll, QtCore.SIGNAL("clicked()"), self.Process)
        self.Option_Plus.valueChanged.connect(self.PlusChanger)
        self.Option_Options.valueChanged.connect(self.OptionChanger)
        self.CategoriesView.currentIndexChanged.connect(self.ShowArrays)
        self.ItemView.clicked.connect(self.PreviewImage)
        self.CancelNow.clicked.connect(self.CommandMode)

        # // Radios for Dropper connects
        self.ItemView_2.clicked.connect(self.CopyCommandB)
        self.ItemView.currentItemChanged.connect(self.CopyCommand)

        self.Option_Zen.clicked.connect(self.EventsRadio)
        self.Option_HP.clicked.connect(self.EventsRadio)
        self.Option_DD.clicked.connect(self.EventsRadio)
        self.Option_DSR.clicked.connect(self.EventsRadio)
        self.Option_Reflect.clicked.connect(self.EventsRadio)
        self.Option_Mana.clicked.connect(self.EventsRadio)
        self.AncientOptions.currentIndexChanged.connect(self.EventsRadio)

        # vault funcs
        self.AppendOnOff_2.clicked.connect(self.RestoreFloater)
        self.vaultCat.currentChanged.connect(self.VaultChange)

        # Equipment Events
        self.charMode_helm.clicked.connect(self.cmode_helm)
        self.charMode_armor.clicked.connect(self.cmode_armor)
        self.charMode_gloves.clicked.connect(self.cmode_gloves)
        self.charMode_pants.clicked.connect(self.cmode_pants)
        self.charMode_boot.clicked.connect(self.cmode_boot)
        self.charMode_pet.clicked.connect(self.cmode_pet)
        self.charMode_wing.clicked.connect(self.cmode_wing)
        self.charMode_pendant.clicked.connect(self.cmode_pendant)
        self.charMode_ring1.clicked.connect(self.cmode_ring1)
        self.charMode_ring2.clicked.connect(self.cmode_ring2)
        self.charMode_weap1.clicked.connect(self.cmode_wep1)
        self.charMode_weap2.clicked.connect(self.cmode_wep2)
        self.LoadIds()  # Account Loader.

        # AccMan Events
        self.AccMan_UsersList.clicked.connect(self.AccMan_SelectedUL)
        self.AccMan_ChooseAcc_EditChar_2.currentIndexChanged.connect(self.AccMan_Selected)
        self.AccMan_ChooseAcc_EditChar_2.activated.connect(self.AccMan_Selected)
        self.AccMan_ChooseChar.currentIndexChanged.connect(self.AccMan_LoadChars_Info)
        self.AccMan_ChooseAcc_EditChar.currentIndexChanged.connect(self.AccMan_LoadAllCurrent)
        self.AccMan_ChooseAcc_EditChar.activated.connect(self.AccMan_LoadAllCurrent)
        self.AccManagerTabber.currentChanged.connect(self.AccManChangeTabber)
        self.AccMan_EditChar_Choose.currentIndexChanged.connect(self.AccMan_LoadChars_EditMode)
        self.AccMan_EditChar_Evolve_2.currentIndexChanged.connect(self.AccMan_LoadGuild)
        self.AccMan_EditChar_SaveAll.clicked.connect(self.C_ProcAll)
        self.AccMan_EditChar_Restore.clicked.connect(self.C_RestAll)

        # PackMan Events
        self.Package_ChooseClass.currentIndexChanged.connect(self.ClassChanged_PM)
        self.Package_Choose.currentIndexChanged.connect(self.PremiumChanged_PM)
        self.Package_List.clicked.connect(self.PremiumEngineProc)
        self.ProcessPackage.clicked.connect(self.PremiumEngineProc)

        # Option Manager
        self.ConnectNow.clicked.connect(self.OptMan_Connect)
        # tabs
        self.AccountManagerButton.clicked.connect(self.Switch_AccMan)
        self.ItemMakerButton.clicked.connect(self.Switch_ItemMan)
        self.PackageMakerButton.clicked.connect(self.Switch_PackMan)
        self.ConnectionButton.clicked.connect(self.Switch_OptionMan)


        # none (Bug if you arent registered on the server side)
        self.Logger("Staff Authenticated: {sid}".format(sid=self.Auth.upper()))

    global EquipE, StopPop
    StopPop = False
    EquipE = None

    # // Tabber functions (Buttons)
    def C_RestAll(self):
        self.AccMan_RestoreAll()

    def C_ProcAll(self):
        self.AccMan_ProcessAll()

    def CopyCommandB(self):
        self.CopyCommand()
        global StopPop
        if not StopPop:
            self.MessageEngine(title='BeastMaker: Dropper Tips', msg="""
"Double-Click" the Item Name above to make BeastMaker copy the whole Command automatically,
and Hit CTRL+V together in Game to Paste.""")
            StopPop = True

    def CopyCommand(self):
        import win32clipboard
        # self.lineEdit.copy()
        try:
            win32clipboard.OpenClipboard()
            win32clipboard.EmptyClipboard()
            win32clipboard.SetClipboardText(str(self.lineEdit.text()))
            win32clipboard.CloseClipboard()
        except:
            self.MessageEngine("Error Copying!","BeastMaker needs a special permission!")

    def MessageEngine(self, title="BeastMaker", msg="BeastMaker"):
        msgBox = QtGui.QMessageBox()
        msgBox.setWindowTitle(str(title))
        msgBox.setText(str(msg))
        msgBox.exec_()

    def CheckPointer(fn):
        def Wrap(self, *args):
            # starts check
            return fn(self, *args)

        return Wrap

    def Cacher_Parse(self, get=False):
        path = "/Data/Database/ItemsData/cache"
        if get:
            return path
        else:
            pass

    def Cacher(fn):
        def Wrap(self, *args):
            # starts caching
            try:
                cfile = open(self.Cacher_Parse(get=True), "w")
                fstrings = """
['{uname}', '{pass}', '{jdate}', 'bserial', 'banned'] # AccountManager
                """

            finally:
                return fn(self, *args)

        return Wrap

    @CheckPointer
    @Cacher
    def AccMan_ProcessAll(self):
        _AccID = self.AccMan_ChooseAcc_EditChar.currentText()
        _AccChar = self.AccMan_EditChar_Choose.currentText()
        Query = """"""

        def Proc_Account():
            table = self.AccMan_Edit_Account
            total = table.rowCount()
            CurrentDatas = [ ]
            for data in range(0, total):
                CurrentDatas.append(str(table.item(0, data).text()))

            setNow = """
    UPDATE [MuOnline].[dbo].[MEMB_INFO]
    SET [memb__pwd] = '{bmpwd}'
    where [memb___id] = '{bmcurrent}'

        UPDATE [MuOnline].[dbo].[MEMB_INFO]
    SET [appl_days] = '{bmjoin}'
    where [memb___id] = '{bmcurrent}'

            """.format(bmpwd=CurrentDatas[ 1 ],
                       bmjoin=CurrentDatas[ 2 ],
                       bmcurrent=_AccID)
            try:
                ExMe = self.Cursor.execute(setNow)
                ExMe.commit()
                self.MessageEngine(title='Account Edited!',
                                   msg="Successfully Edited \'{cname}\' Account!".format(cname=_AccID))
            except:
                self.MessageEngine(title='BeastException: RemERR',
                                   msg='Remote/Connection Error or Queries has been Rejected.')

        def Proc_Actions():
            pass

        def Proc_Guild():
            pass

        def Proc_Credits():
            pass

        def Proc_Class():
            pass

        def Proc_TransG():
            pass

        def Proc_Char():
            table = self.AccMan_Edit_Char
            total = table.rowCount()
            CurrentDatas = [ ]
            for data in range(0, total):
                CurrentDatas.append(str(table.item(0, data).text()))
            setNow = """
UPDATE [MuOnline].[dbo].[Character]
    set [cLevel] = '{level}'
    where [Name] = '{currchar}'

    UPDATE [MuOnline].[dbo].[Character]
    set [SCFMasterLevel] = '{ml}'
    where [Name] = '{currchar}'

    UPDATE [MuOnline].[dbo].[Character]
    set [LevelUpPoint] = '{lvlp}'
    where [Name] = '{currchar}'

    UPDATE [MuOnline].[dbo].[Character]
    set [Money] = '{mn}'
    where [Name] = '{currchar}'

    UPDATE [MuOnline].[dbo].[Character]
    set [Resets] = '{rst}'
    where [Name] = '{currchar}'

    UPDATE [MuOnline].[dbo].[Character]
    set [SCFPCPoints] = '{pcp}'
    where [Name] = '{currchar}'

    UPDATE [MuOnline].[dbo].[Character]
    set [Strength] = '{str}'
    where [Name] = '{currchar}'

    UPDATE [MuOnline].[dbo].[Character]
    set [Dexterity] = '{agi}'
    where [Name] = '{currchar}'

    UPDATE [MuOnline].[dbo].[Character]
    set [Vitality] = '{vit}'
    where [Name] = '{currchar}'

    UPDATE [MuOnline].[dbo].[Character]
    set [Energy] = '{energy}'
    where [Name] = '{currchar}'

    UPDATE [MuOnline].[dbo].[Character]
    set [Leadership] = '{com}'
    where [Name] = '{currchar}'

            """.format(level=CurrentDatas[ 0 ],
                       ml=CurrentDatas[ 1 ],
                       lvlp=CurrentDatas[ 2 ],
                       mn=CurrentDatas[ 3 ],
                       rst=CurrentDatas[ 4 ],
                       pcp=CurrentDatas[ 5 ],
                       str=CurrentDatas[ 6 ],
                       agi=CurrentDatas[ 7 ],
                       vit=CurrentDatas[ 8 ],
                       energy=CurrentDatas[ 9 ],
                       com=CurrentDatas[ 10 ],
                       #                       ctl='0',
                       #                       cls=,#CurrentDatas[12],
                       currchar=_AccChar)
            #    UPDATE [MuOnline].[dbo].[Character]
            #    set [CtlCode] = '{ctl}'
            #    where [Name] = '{currchar}'

            # UPDATE [MuOnline].[dbo].[Character]
            # set [Class] = '{cls}'
            # where [Name] = '{currchar}'
            # print CurrentDatas, setNow # debugging
            # try:
            CurSetN = self.Cursor.execute(setNow)
            CurSetN.commit()

            self.MessageEngine(title='Character Edited!',
                               msg='Successully Edited {ac}\'s Character: {cr}'.format(ac=_AccID, cr=_AccChar))
            # except:
            #    self.MessageEngine(title='Unable to Update Character',msg='Unable to Update Character!')

        try:
            Proc_Account()  # Process Account Info
            Proc_Char()  # Process Character Infos
            Proc_Actions()  # Process Action dropdown
            Proc_Guild()  # Process Guils
            Proc_Credits()  # Process Credits
            Proc_Class()  # Process Class
            Proc_TransG()  # Process Trans-Gender

        except:
            self.MessageEngine(title='BeastException', msg='Error Occured, BeastErr Thrown')

    @CheckPointer
    @Cacher
    def AccMan_RestoreAll(self):
        pass

    def AccMan_LoadGuild(self):
        guild = self.AccMan_EditChar_Evolve_2.currentText()
        _get = """
/****** Script for SelectTopNRows command from SSMS  ******/
SELECT [G_Score]
  ,[G_Master]
FROM [MuOnline].[dbo].[Guild] where [G_Name] = 'aWqasw'""".replace('aWqasw', guild)
        try:
            Ex = self.Cursor.execute(_get)
            ret = Ex.fetchall()[ 0 ]
            self.AccMan_Edit_Char_2.item(0, 0).setText(str(ret[ 1 ]))
            self.AccMan_Edit_Char_2.item(0, 1).setText(str(ret[ 0 ]))

        except:
            pass

    def isSA(self):
        try:
            IsSa = self.BeastOptions[ 8 ]
            if IsSa[ 1 ]:
                conv = [ str(IsSa[ 0 ]).decode('base64'), str(IsSa[ 1 ]) ]
                if self.Auth == conv[ 0 ]:
                    return True
                else:
                    return False
            else:
                return False
        except:
            return False

    def AccMan_LoadChars_EditMode(self):
        def _GetCharCur():
            SelectedChar = self.AccMan_EditChar_Choose.currentText()  # self.AccMan_EditChar_Choose.currentText()
            _qu = """
    /****** Script for SelectTopNRows command from SSMS  ******/
    SELECT [cLevel]
            ,[SCFMasterLevel]
          ,[LevelUpPoint]
          ,[Money]
          ,[Resets]
          ,[SCFPCPoints]

          ,[Strength]
          ,[Dexterity]
          ,[Vitality]
          ,[Energy]
          ,[Leadership]

          ,[CtlCode]
          ,[Class]

      FROM [MuOnline].[dbo].[Character] where [Name]='ussrs'
            """

            def LoadStats():
                _qsc = _qu.replace('ussrs', str(SelectedChar))
                try:
                    _execinner = self.Cursor.execute(_qsc).fetchall()[ 0 ]
                    charI = self.AccMan_Edit_Char
                    charI.item(0, 0).setText(str(_execinner[ 0 ]))
                    charI.item(0, 1).setText(str(_execinner[ 1 ]))
                    charI.item(0, 2).setText(str(_execinner[ 2 ]))
                    charI.item(0, 3).setText(str(_execinner[ 3 ]))
                    charI.item(0, 4).setText(str(_execinner[ 4 ]))
                    charI.item(0, 5).setText(str(_execinner[ 5 ]))
                    charI.item(0, 6).setText(str(_execinner[ 6 ]))
                    charI.item(0, 7).setText(str(_execinner[ 7 ]))
                    charI.item(0, 8).setText(str(_execinner[ 8 ]))
                    charI.item(0, 9).setText(str(_execinner[ 9 ]))
                    charI.item(0, 10).setText(str(_execinner[ 10 ]))
                    charI.item(0, 11).setText(str(self.GetRank(_execinner[ 11 ])))
                    charI.item(0, 12).setText(str(self.GetRank(_execinner[ 11 ])))
                    charI.item(0, 13).setText(str(self.GetClass(_execinner[ 12 ])))

                except:
                    pass

            LoadStats()

        _GetCharCur()

    def AccMan_LoadAllCurrent(self):
        def _GetCharCur():
            SelectedChar = self.AccMan_EditChar_Choose.currentText()  # self.AccMan_EditChar_Choose.currentText()
            _qu = """
    /****** Script for SelectTopNRows command from SSMS  ******/
    SELECT [cLevel]
            ,[SCFMasterLevel]
          ,[LevelUpPoint]
          ,[Money]
          ,[Resets]
          ,[SCFPCPoints]

          ,[Strength]
          ,[Dexterity]
          ,[Vitality]
          ,[Energy]
          ,[Leadership]

          ,[CtlCode]
          ,[Class]

      FROM [MuOnline].[dbo].[Character] where [Name]='ussrs'
            """

            def LoadStats():
                _qsc = _qu.replace('ussrs', str(SelectedChar))
                try:
                    _execinner = self.Cursor.execute(_qsc).fetchall()[ 0 ]
                    charI = self.AccMan_Edit_Char
                    charI.item(0, 0).setText(str(_execinner[ 0 ]))
                    charI.item(0, 1).setText(str(_execinner[ 1 ]))
                    charI.item(0, 2).setText(str(_execinner[ 2 ]))
                    charI.item(0, 3).setText(str(_execinner[ 3 ]))
                    charI.item(0, 4).setText(str(_execinner[ 4 ]))
                    charI.item(0, 5).setText(str(_execinner[ 5 ]))
                    charI.item(0, 6).setText(str(_execinner[ 6 ]))
                    charI.item(0, 7).setText(str(_execinner[ 7 ]))
                    charI.item(0, 8).setText(str(_execinner[ 8 ]))
                    charI.item(0, 9).setText(str(_execinner[ 9 ]))
                    charI.item(0, 10).setText(str(_execinner[ 10 ]))
                    charI.item(0, 11).setText(str(self.GetRank(_execinner[ 11 ])))
                    charI.item(0, 12).setText(str(self.GetRank(_execinner[ 11 ])))
                    charI.item(0, 13).setText(str(self.GetClass(_execinner[ 12 ])))

                except:
                    pass

            LoadStats()

        def _GetChars(usr):
            _exec = self.Cursor.execute("""
/****** Script for SelectTopNRows command from SSMS  ******/
SELECT	[GameID1]
  ,[GameID2]
  ,[GameID3]
  ,[GameID4]
  ,[GameID5]
FROM [MuOnline].[dbo].[AccountCharacter] where [Id] = 'ussre'

            """.replace('ussre', usr))
            try:
                Clean = _exec.fetchall()[ 0 ]
                CurChars = [ ]
                for Chars in Clean:
                    if Chars:
                        CurChars.append(Chars)
                self.AccMan_EditChar_Choose.clear()
                self.AccMan_EditChar_Choose.addItems(CurChars)
            except:
                pass

        def _GetGuilds():
            _get = """
/****** Script for SelectTopNRows command from SSMS  ******/
SELECT [G_Name]
      ,[G_Score]
      ,[G_Master]
  FROM [MuOnline].[dbo].[Guild]
            """
            Catcher = [ ]
            try:
                E = self.Cursor.execute(_get).fetchall()
                EE = E

                try:
                    for li in EE:
                        Catcher.append(li[ 0 ])
                finally:
                    self.AccMan_EditChar_Evolve_2.clear()
                    self.AccMan_EditChar_Evolve_2.addItems(Catcher)

            except:
                pass

        def _GetAccountInfo(user):
            _user = self.AccMan_ChooseAcc_EditChar.currentText()
            Infos = self.AccMan_Edit_Account

            def Proc(user):
                _str = """
    SELECT [memb___id]
          ,[memb__pwd]
          ,[appl_days]
          ,[memb_guid]
          ,[Ctl1_Code]
      FROM [MuOnline].[dbo].[MEMB_INFO] where [memb___id] = 'ussrname'
                """.replace("ussrname", str(user).strip())
                Pros = self.Cursor.execute(_str)

                try:
                    _Rets = Pros.fetchall()[ 0 ]
                    Infos.item(0, 0).setText(str(_Rets[ 0 ]))
                    Check = lambda: True if self.isSA() else False
                    if Check() and _user != 'mark':
                        Infos.item(0, 1).setText(str(_Rets[ 1 ]))
                    else:
                        Infos.item(0, 1).setText("********")

                    Infos.item(0, 2).setText(str(_Rets[ 2 ]))
                    Infos.item(0, 3).setText("CSEC-U" + str(_Rets[ 3 ]))
                    Infos.item(0, 4).setText(str(_Rets[ 4 ]))
                except:
                    pass

            Proc(_user)

        def _GetCredits(user):
            user = user
            char = self.AccMan_EditChar_Choose.currentText()
            c_credits = self.AccMan_EditChar_Credits
            pc_credits = self.AccMan_EditChar_PCPoints
            wc_credits = self.AccMan_EditChar_WCoin

            def GetWCoin():
                _s = """
/****** Script for SelectTopNRows command from SSMS  ******/
SELECT [WCoin]
  FROM [MuOnline].[dbo].[MEMB_INFO] where [memb___id] ='xAdws'
                """.replace('xAdws', user)
                try:
                    _r = self.Cursor.execute(_s)
                    return _r.fetchall()[ 0 ][ 0 ]
                except:
                    return "None"

            def GetCredits():
                _s = """/****** Script for SelectTopNRows command from SSMS  ******/
SELECT TOP 1000 [memb___id]
      ,[credits]
  FROM [MuOnline].[dbo].[MEMB_CREDITS] where [memb___id] = 'xAdws'""".replace("xAdws", user)
                try:
                    _r = self.Cursor.execute(_s)
                    return _r.fetchall()[ 0 ][ 1 ]
                except:
                    return "0"

            def PCCredits(char):
                _s = """
            /****** Script for SelectTopNRows command from SSMS  ******/
SELECT [SCFPCPoints]
  FROM [MuOnline].[dbo].[Character] where [Name] = 'xAdws'

                """.replace("xAdws", char)
                try:
                    _r = self.Cursor.execute(_s)
                    return _r.fetchall()[ 0 ][ 0 ]
                except:
                    return "0"

            c_credits.setPlaceholderText("C: " + str(GetCredits()) + " Credits")
            wc_credits.setPlaceholderText("C: " + str(GetWCoin()) + " WCoins")
            pc_credits.setPlaceholderText("C: " + str(PCCredits(char)) + " PCP")

        def LoadAll():
            user = self.AccMan_ChooseAcc_EditChar.currentText()
            _GetAccountInfo(user)
            _GetChars(user)
            _GetCredits(user)
            _GetGuilds()
            _GetCharCur()

        LoadAll()

    def AccMan_EditInit(self):
        self.AccMan_ChooseAcc_EditChar.clear()
        self.AccMan_ChooseAcc_EditChar.addItems(self.AccountsCache)
        self.AccMan_LoadAllCurrent()

    def AccManChangeTabber(self):
        cat = self.AccManagerTabber.currentIndex()
        if cat == 0:  # Viewer
            pass

        if cat == 1:  # Edit Management
            self.AccMan_EditInit()

        if cat == 2:  # GM Resources
            pass

        if cat == 3:  # Notes
            pass

    def Switch_AccMan(self):
        self.bmuPager.setCurrentIndex(2)  # accManager
        self.AccMan_Loader()

    def Switch_ItemMan(self):
        self.bmuPager.setCurrentIndex(0)  # accManager

    def Switch_PackMan(self):
        self.bmuPager.setCurrentIndex(1)  # accManager
        self.PackageManagerRes()

    def Switch_OptionMan(self):
        self.bmuPager.setCurrentIndex(3)  # optsManager
        self.OptionMan_Loader()


    # // BeastMaker Functions // Account Manager
    def GetClass(self, num):
        track = {
            0: 'Dark Wizard',
            1: 'Soul Master',
            2: 'Grand Master',
            16: 'Dark Knight',
            17: 'Blade Knight',
            18: 'Blade Master',
            32: 'Elf',
            33: 'Muse Elf',
            34: 'High Elf',
            48: 'Magic Gladiator',
            50: 'Duel Master',
            64: 'Dark Lord',
            66: 'Lord Emperor',
            80: 'Summoner',
            81: 'Bloody Summoner',
            82: 'Dimension Master',
            96: 'Rage Fighter',
            97: 'Fist Master'
        }

        return track[ int(num) ]

    def GetRank(self, num):
        track = {32: "Game Master",
                 8: "Game Master",
                 0: "Normal",
                 1: "Blocked"}
        return track[ int(num) ]

    def AccMan_LoadChars_Info(self):
        SelectedChar = self.AccMan_ChooseChar.currentText()
        _qu = """
/****** Script for SelectTopNRows command from SSMS  ******/
SELECT [cLevel]
        ,[SCFMasterLevel]
      ,[LevelUpPoint]
      ,[Money]
      ,[Resets]
      ,[SCFPCPoints]

      ,[Strength]
      ,[Dexterity]
      ,[Vitality]
      ,[Energy]
      ,[Leadership]

      ,[CtlCode]
      ,[Class]

  FROM [MuOnline].[dbo].[Character] where [Name]='ussrs'
        """

        def LoadStats():
            _qsc = _qu.replace('ussrs', str(SelectedChar))
            try:
                _execinner = self.Cursor.execute(_qsc).fetchall()[ 0 ]
                charI = self.AccMan_charDetails_tableWidget
                charI.item(0, 0).setText(str(_execinner[ 0 ]))
                charI.item(0, 1).setText(str(_execinner[ 1 ]))
                charI.item(0, 2).setText(str(_execinner[ 2 ]))
                charI.item(0, 3).setText(str(_execinner[ 3 ]))
                charI.item(0, 4).setText(str(_execinner[ 4 ]))
                charI.item(0, 5).setText(str(_execinner[ 5 ]))
                charI.item(0, 6).setText(str(_execinner[ 6 ]))
                charI.item(0, 7).setText(str(_execinner[ 7 ]))
                charI.item(0, 8).setText(str(_execinner[ 8 ]))
                charI.item(0, 9).setText(str(_execinner[ 9 ]))
                charI.item(0, 10).setText(str(_execinner[ 10 ]))
                charI.item(0, 11).setText(str(self.GetRank(_execinner[ 11 ])))
                Banned = lambda: "No" if int(_execinner[ 11 ]) != 1 else "Yes"
                charI.item(0, 12).setText(str(Banned()))

                # 13 is useless
                charI.item(0, 13).setText(str(self.GetClass(_execinner[ 12 ])))

            except:
                pass

        if SelectedChar == 'Cache':
            pass
        else:
            LoadStats()

    def AccMan_SelectedUL(self):
        am_Cname = self.AccMan_UsersList.currentItem().text()
        # Set AccMan_Selected
        self.AccMan_ChooseAcc_EditChar_2.setCurrentIndex(int(self.AccMan_ChooseAcc_EditChar_2.findText(am_Cname)))

    def AccMan_Selected(self):
        __C = self.AccMan_ChooseAcc_EditChar_2.currentText()
        __I = self.AccMan_ChooseAcc_EditChar_2.currentIndex()
        self.AccMan_UsersList.setCurrentRow(__I)
        # finally
        self.AccMan_LoadAccInfo()

    def AccMan_LoadAccInfo(self):
        CurId = self.AccMan_ChooseAcc_EditChar_2.currentText()

        def LoadGroupIAccount():
            _str = """
SELECT [memb___id]
      ,[memb__pwd]
      ,[sno__numb]
      ,[mail_addr]
      ,[appl_days]
      ,[SecretAnswer]
      ,[country]
      ,[gender]
      ,[memb_guid]
      ,[SCFVipMoney]

  FROM [MuOnline].[dbo].[MEMB_INFO] where [memb___id] = 'ussrname'
            """.replace("ussrname", CurId)
            _Proc = self.Cursor.execute(_str).fetchall()
            Tuple = [ ]

            def _Sa():
                try:
                    from Data.Database import Server

                    cacheO = self.BeastOptions[ 8 ]
                    if cacheO[ 1 ]:
                        conv = [ str(cacheO[ 0 ]).decode('base64'), str(cacheO[ 1 ]) ]
                        if self.Auth == conv[ 0 ]:
                            return True
                        else:
                            return False
                    else:
                        return False
                except:
                    return False

            def GetCharLen(usr):
                _exec = self.Cursor.execute("""
/****** Script for SelectTopNRows command from SSMS  ******/
SELECT	[GameID1]
      ,[GameID2]
      ,[GameID3]
      ,[GameID4]
      ,[GameID5]
  FROM [MuOnline].[dbo].[AccountCharacter] where [Id] = 'ussre'

                """.replace('ussre', usr))
                Clean = _exec.fetchall()[ 0 ]
                CurChars = [ ]
                for Chars in Clean:
                    if Chars:
                        CurChars.append(Chars)
                LoadGroupIChars(CurChars)
                return str(len(CurChars))

            def Setter():
                # rets
                SA = lambda: True if _Sa() else False
                self.AccMan_info_username.setText(str(Tuple[ 0 ]))
                if SA() and CurId != "mark":
                    self.AccMan_info_password.setText(str(Tuple[ 1 ]))  # uname
                else:
                    self.AccMan_info_password.setText("********")  # pass
                self.AccMan_info_email.setText(str(Tuple[ 3 ]))
                self.AccMan_info_joindate.setText(str(Tuple[ 4 ]))
                self.AccMan_info_secanswer.setText(str(Tuple[ 5 ]))
                self.AccMan_info_country.setText(str(Tuple[ 6 ]))
                GetGen = lambda: "Male" if bool(Tuple[ 7 ]) else "Female"
                self.AccMan_info_gender.setText(str(GetGen()))
                self.AccMan_info_accserial.setText("CSEC-U" + str(Tuple[ 8 ]))
                self.AccMan_info_vipmoney.setText(str(Tuple[ 9 ]))
                self.AccMan_info_charlen.setText(GetCharLen(CurId))

            try:
                for info in _Proc[ 0 ]:
                    Tuple.append(str(info).strip())
                Setter()
            except:
                pass

        def LoadGroupIChars(charsl):
            chars = charsl
            self.AccMan_ChooseChar.clear()
            self.AccMan_ChooseChar.addItems(chars)

        LoadGroupIAccount()

    def AccMan_LoadChars(self, user):
        pass

    def AccMan_Loader(self):
        self.AccMan_ChooseAcc_EditChar_2.clear()
        self.AccMan_UsersList.clear()

        self.AccMan_ChooseAcc_EditChar_2.addItems(self.AccountsCache)
        self.AccMan_UsersList.addItems(self.AccountsCache)

    # //  BeastMaker Functions // Item Maker


    def ProcEquipEngine(self):
        global EquipE
        try:
            Q = """
    Update character set Inventory = {fullq}
    where Name = '{id}'
    """.format(fullq="0x" + str(EquipE),
               id=self.AccountValue.currentText())
            self.Cursor.execute(Q)
            self.con.commit()

            self.WriteLogs()
            self.Logger(
                """[Items Processed] <br><b><span style=" color:#FFFF00;">Item: {item}</b></span><br> <span style=" color:#FFFF00;"><u>Serial: {serial}</u></span>""".format(
                    item=self.ItemCache, serial=self.ItemSerialCache))
        except:
            total = 0
            FObj = ""

            for i in self.Cache.values():
                total += len(i)

            while total != 7552:  # varbinary Fs
                FObj += "F"  # to make the exact equip Fs
                total += 1

            self.FullQ = self.Cache[ 'wep2' ] + self.Cache[ 'wep1' ] + self.Cache[ 'helm' ] + self.Cache[ 'armor' ] + \
                         self.Cache[ 'pants' ] + self.Cache[ 'gloves' ] + self.Cache[ 'boots' ]
            self.FullQ2 = self.Cache[ 'wing' ] + self.Cache[ 'pet' ] + self.Cache[ 'pendant' ] + self.Cache[ 'ring2' ] + \
                          self.Cache[ 'ring1' ]
            self._all = self.FullQ + self.FullQ2 + FObj
            Q2 = """
    Update character set Inventory = {fullq}
    where Name = '{id}'
    """.format(fullq="0x" + str(self._all),
               id=self.AccountValue.currentText())
            self.Cursor.execute(Q2)
            self.con.commit()
            try:
                self.WriteLogs()
            except: pass
            self.Logger("[Items Processed] {item} {serial}".format(item=self.ItemCache, serial=self.ItemSerialCache))
            # Execute.commit()
            # print self.AccountValue.currentText()

    def WriteLogs(self):
        import datetime

        global iParse
        form = """
----------------------------
Staff: {sname}
Reciever: {srname}
Date: {sdate}
Item: {sitem}
Parsed: {sparsed}
Serial: {sserial}
----------------------------
        """.format(
            sname=self.Auth.upper(),
            srname=self.AccountValue.currentText(),
            sdate=datetime.datetime.now(),
            sitem=self.ItemCache,
            sparsed=iParse,
            sserial=self.ItemSerialCache
        )
        clean_allowed_col = "{\"" + self.Auth + "\":" + "\"" + self.AuthI + "\"}"
        get = """
SELECT [BeastMaker_Logs]
  FROM [MuOnline].[dbo].[BeastMaker]

  where CONVERT(VARCHAR, BeastMaker_Allowed)= """ + "'" + clean_allowed_col + "'"
        get_ret = self.Cursor.execute(get)
        current = get_ret.fetchall()[ 0 ][ 0 ]
        to_update = current + "\n" + str(form)

        set = """
UPDATE [MuOnline].[dbo].[BeastMaker]
   SET [BeastMaker_Logs] = '#21%1233'
  where CONVERT(VARCHAR, BeastMaker_Allowed)= """ + "'" + clean_allowed_col + "'"
        clean = set.replace("#21%1233", to_update)

        Send = self.Cursor.execute(clean)
        Send.commit()

    def EquipEngine(self, update="helm", clear=False):
        global iParse, EquipE
        self.EquipSize = 2343  # iblocks
        self.Engine(get=True)  # should be run fist.
        # Order: right hand, left hand , helm, armor, pants, gloves, boots, wings, pet, pendant, right ring, left ring
        # helm, armor, gloves, pants
        total = 0
        FObj = ""

        for i in self.Cache.values():
            total += len(i)

        while total != 7552:  # varbinary Fs (Compat for TTech and BEmu)
            FObj += "F"  # to make the exact equip Fs
            total += 1

        cleanParse = str(iParse).replace("0x", "")
        self.Cache[ str(update) ] = str(cleanParse)
        self.FullQ = self.Cache[ 'wep2' ] + self.Cache[ 'wep1' ] + self.Cache[ 'helm' ] + self.Cache[ 'armor' ] + \
                     self.Cache[ 'pants' ] + self.Cache[ 'gloves' ] + self.Cache[ 'boots' ]
        self.FullQ2 = self.Cache[ 'wing' ] + self.Cache[ 'pet' ] + self.Cache[ 'pendant' ] + self.Cache[ 'ring2' ] + \
                      self.Cache[ 'ring1' ]
        self._all = self.FullQ + self.FullQ2 + FObj

        if clear:
            self.Cache.clear()

        EquipE = self._all

    def LoadIds(self):
        self.Us = self.Cursor.execute("""
                    SELECT [memb___id]
      FROM [MuOnline].[dbo].[MEMB_INFO]""")
        CleanStr = [ ]

        for Users in self.Us:
            Clean = str(Users).replace("(", "").replace(")", "").replace("'", "").replace(",", "")
            CleanStr.append(Clean)
        # print CleanStr
        self.Logger(msg="{l} BeastMU Account IDs Loaded!".format(l=len(CleanStr)))
        self.AccountValue.clear()
        self.AccountValue.addItems(CleanStr)  # Add to Users List

    def LoadCharacters(self):
        self.ItemInfoParser()

        self.Users = self.Cursor.execute("""
        SELECT [Name]
FROM [MuOnline].[dbo].[Character]""")
        CleanStr = [ ]

        for Users in self.Users:
            Clean = str(Users).replace("(", "").replace(")", "").replace("'", "").replace(",", "")
            CleanStr.append(Clean)
        # print CleanStr
        self.Logger(msg="{l} BeastMU Characters Loaded!".format(l=len(CleanStr)))
        self.AccountValue.clear()
        self.AccountValue.addItems(CleanStr)  # Add to Users List

    def BytesClear(self, bs):
        """
        Formats to Clear Hex
        """
        try:
            hs = [ "{0:0>2}".format(hex(b)[ 2: ].upper()) for b in bs ]
            return '0x' + ''.join(hs)
        except TypeError:
            return bs

    def GetItemInfo(self, arr):
        from Data.Database import Items

        Clear = arr
        Set_Ancient = {"0": "None Ancient", "1": "Ancient Stamina 5%", "2": "Ancient Stamina 10%"}
        Set_Yellow = {"": ""}
        Set_Plus = {
            "00": "0",
            "7B": "",
            "7F": "0",
            "8F": "1",
            "97": "2",
            "9F": "3",
            "A7": "4",
            "AF": "5",
            "B7": "6",
            "BF": "7",
            "C7": "8",
            "CF": "9",
            "D7": "10",
            "DF": "11",
            "E7": "12",
            "EF": "13",
            "F7": "14",
            "FF": "15",
        }
        num = int(Clear[ 0:2 ], 16)
        plus = Clear[ 2:4 ]
        dur = Clear[ 4:6 ]
        serial = Clear[ 6:14 ]
        allopt = Clear[ 14:16 ]
        print "Options: " + allopt
        ancient = Clear[ 16:18 ]
        group = Clear[ 18:19 ]
        pink = Clear[ 19:20 ]
        yellow = Clear[ 20:22 ]
        sock = Clear[ 22: ]

        fstring = """
<center>
<br><b>{name}</b>
__________________________________________________
</center>

        """
        _grouper_group = {"0": "", "1": "", "2": "", "4": "", "5": "", "6": "", "7": "", "8": "", "9": "", "10": "",
                          "11": "", "12": "", "13": "", "14": "", "15": ""}
        # set cat view
        _itemsimp = Items.Dict
        _itemsInt = sorted(_itemsimp.values())
        for K, V in _itemsimp.iteritems():
            if V == int(group, 16):
                __i = self.CategoriesView.findText(K)
                self.CategoriesView.setCurrentIndex(__i)
                break

        self.ItemView.setCurrentRow(int(num))

        try:
            _Cname = self.ItemView.currentItem().text()

        except:
            pass

        def _AllOpt(*args):
            """
            Previews the Current Options
            """
            HexInt = int(args[ 0 ], 16)
            print "Int Options "+str(HexInt)
            _Cache = ""
            _Luck = "<center>Luck (Success Rate of Jewel of Soul +25%)</center>"
            _LuckTail = "<center>Luck (Critical Damage Rate +5%)</center>"
            _Count = [ 1, 2, 4, 8, 16, 32 ]
            Opt = {_Count[ 0 ]: "<center>Increases Zen Drop Rate After Hunting</center>",
                   _Count[ 1 ]: "<center>Damage Success Rate +10%</center>",
                   _Count[ 2 ]: "<center>Reflect Damage +5%</center>",
                   _Count[ 3 ]: "<center>Damage Decrease +4%</center>",
                   _Count[ 4 ]: "<center>Increase Max Mana +4%</center>",
                   _Count[ 5 ]: "<center>Increase Max HP +4%</center>"
                   }

            if HexInt:
                print HexInt
                if HexInt == 63 or HexInt == 127:  # FO
                    _Cache = _Luck + _LuckTail + Opt[ 2 ] + Opt[ 4 ] + Opt[ 8 ] + Opt[ 16 ] + Opt[ 32 ] + Opt[
                        1 ] + "<center>FULL OPTION ITEM<BR></center>"

                # elif HexInt ==

                elif HexInt == 0:  #
                    _Cache = "<center>NO OPTION</center><br>"

                elif HexInt == 1:  #
                    _Cache = _Luck + _LuckTail + Opt[ 1 ] + "<br>"

                elif HexInt == 3:  #
                    _Cache = _Luck + _LuckTail + Opt[ 2 ] + Opt[ 1 ] + "<br>"

                elif HexInt == 7:  #
                    _Cache = _Luck + _LuckTail + Opt[ 2 ] + Opt[ 4 ] + Opt[ 1 ] + "<br>"

                elif HexInt == 15:  #
                    _Cache = _Luck + _LuckTail + Opt[ 2 ] + Opt[ 4 ] + Opt[ 8 ] + Opt[ 1 ] + "<br>"

                elif HexInt == 31:  #
                    _Cache = _Luck + _LuckTail + Opt[ 2 ] + Opt[ 4 ] + Opt[ 8 ] + Opt[ 16 ] + Opt[ 1 ] + "<br>"


                else:
                    _Cache = "<center>Parser unable to Analyze the Option: {f}</center>".format(f=HexInt)

                return _Cache

            else:
                return "<center>NO OPTIONS</center>"

        def GetYellow(y):
            yellow = y[ 0 ]
            from Data.Database.Yellow import Gear, Weapons

            try:
                Result = Gear[ int(yellow) ]
                if yellow > 0:
                    return """<center><b><span style=" color:#FFFF00;">""" + Result + """</span></b></center>"""
                else:
                    return ""
            except:
                return ""

        # anon funcs.
        _isEx = lambda: "Excellent " if allopt != "00" else ""
        _isPink = lambda: """ <center><span style=" color:#FF33CC;">Pink Option </span></center>""" if int(pink) else ""

        def PlusGetter():
            try:
                return Set_Plus[ plus ]
            except KeyError:
                return "0"

        _f_name = str(_isEx()) + str(_Cname) + " +" + str(PlusGetter())

        def SocketGetter(v):
            _socks = self.SocketEngine(get=True)
            _socksList = (v[0:2], v[2:4],v[4:6],v[6:8],v[8:])

            def CheckSock(hex):

                if _socks[str(hex)] == "00":
                    return "None"
                else:
                    return _socks[str(hex)]

            s1 = _socksList[0]
            s2 =  _socksList[1]
            s3 =  _socksList[2]
            st = """<center><span style=" color:#FF33CC;">Socket 1: {s1}<br> Socket 2: {s2}<br>Socket 3: {s3}</span></center>""".format(
                s1=CheckSock(s1),
                s2=CheckSock(s2),
                s3=CheckSock(s3)
            )
            return st

        # appends
        fstring = fstring.format(name=_f_name)  # add the name
        fstring += str(GetYellow(yellow))
        fstring += str(_isPink())
        fstring += str(_AllOpt(allopt))
        fstring += str(SocketGetter(sock))
        fstring += "<center>Item Serial: <b><u>{serial}</b></u></center>".format(serial=serial) + "<br>"

        return fstring

    def StringChanged(self):
        _name = self.AccountValue.currentText()
        if self.vaultCat.currentIndex() == 2:
            self.ItemInfoParser(name=_name)
        else:
            pass  # nothing..

    def ItemInfoParser(self, name='Mark'):
        Fstring = """
SELECT [Inventory]
  FROM [MuOnline].[dbo].[Character]where Name='{beastid}'
          """.format(beastid=name)
        Sc = self.Cursor.execute(Fstring)
        Aligned = None
        try:
            Ret = Sc.fetchone()[ 0 ]
            _FullClear = self.BytesClear(Ret).replace("0x", "")
            Aligned = [ _FullClear[ i:i + 32 ] for i in range(0, len(_FullClear), 32) ]
            self.CharactersCache = Aligned

        except:
            pass

        self.Cache = {"helm": "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF", "armor": "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF",
                      "gloves": "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF", "pants": "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF",
                      "boots": "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF",
                      "pet": "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF", "wing": "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF",
                      "pendant": "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF", "ring1": "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF",
                      "ring2": "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF", "wep1": "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF",
                      "wep2": "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF", }

        _AlignedLoc = Aligned

        def ItemObj(i):
            Group = i[ 18:19 ]
            Item = i[ 0:2 ]

            if Group == "A":
                Group = 10

            elif Group == "B":
                Group = 11

            elif Group == "C":
                Group = 12

            elif Group == "D":
                Group = 13

            elif Group == "E":
                Group = 14

            elif Group == "F":
                Group = 15

            CleanItem = int(Item, 16)
            # right hand, left hand , helm, armor, pants, gloves, boots, wings, pet, pendant, right ring, left ring
            IFile = "Data/Items/views/" + str(Group) + "/" + str(CleanItem) + ".gif"
            return IFile

        def ItemPrevSetter():
            Aligned = self.CharactersCache
            blank = "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF"
            if Aligned[ 0 ]:
                if Aligned[0] != blank:
                    self.charMode_weap2.setText("")
                    self.charMode_weap2.setIcon(QtGui.QIcon(ItemObj(i=Aligned[ 0 ])))  # right
                    self.Cache[ "wep2" ] = Aligned[ 0 ]
                    self.charMode_weap2.setToolTip(self.GetItemInfo(Aligned[ 0 ]))
                else:
                    self.charMode_weap2.setText("No Item")
                    self.charMode_weap2.setIcon(QtGui.QIcon())
                    self.Cache[ "wep2" ] = blank

            if Aligned[ 1 ]:
                if Aligned[ 1 ] != blank:
                    self.charMode_weap1.setText("")
                    self.charMode_weap1.setIcon(QtGui.QIcon(ItemObj(i=Aligned[ 1 ])))  # left
                    self.Cache[ "wep1" ] = Aligned[ 1 ]
                    self.charMode_weap1.setToolTip(self.GetItemInfo(Aligned[ 1 ]))
                else:
                    self.charMode_weap1.setText("No Item")
                    self.charMode_weap1.setIcon(QtGui.QIcon())
                    self.Cache[ "wep1" ] = blank

            if Aligned[2]:
                if Aligned[ 2 ] != blank:
                    self.charMode_helm.setText("")
                    self.charMode_helm.setIcon(QtGui.QIcon(ItemObj(i=Aligned[ 2 ])))  # helm
                    self.Cache[ "helm" ] = Aligned[ 2 ]
                else:
                    self.charMode_helm.setText("No Item")
                    self.charMode_helm.setIcon(QtGui.QIcon())  # helm
                    self.Cache[ "helm" ] = blank

            if Aligned[3]:
                if Aligned[ 3 ] != blank:
                    self.charMode_armor.setText("")
                    self.charMode_armor.setIcon(QtGui.QIcon(ItemObj(i=Aligned[ 3 ])))  # armor
                    self.Cache[ "armor" ] = Aligned[ 3 ]
                    self.charMode_armor.setToolTip(self.GetItemInfo(Aligned[ 3 ]))
                else:
                    self.charMode_armor.setText("No Item")
                    self.charMode_armor.setIcon(QtGui.QIcon())  # helm
                    self.Cache[ "armor" ] = blank

            if Aligned[4]:

                if Aligned[ 4 ] != blank:
                    self.charMode_pants.setText("")
                    self.charMode_pants.setIcon(QtGui.QIcon(ItemObj(i=Aligned[ 4 ])))  # pants
                    self.Cache[ "pants" ] = Aligned[ 4 ]
                    self.charMode_pants.setToolTip(self.GetItemInfo(Aligned[ 4 ]))
                else:
                    self.charMode_pants.setText("No Item")
                    self.charMode_pants.setIcon(QtGui.QIcon())  # helm
                    self.Cache[ "pants" ] = blank

            if Aligned[5]:
                if Aligned[ 5 ] != blank:
                    self.charMode_gloves.setText("")
                    self.charMode_gloves.setIcon(QtGui.QIcon(ItemObj(i=Aligned[ 5 ])))  # gloves
                    self.Cache[ "gloves" ] = Aligned[ 5 ]
                    self.charMode_gloves.setToolTip(self.GetItemInfo(Aligned[ 5 ]))
                else:
                    self.charMode_gloves.setText("No Item")
                    self.charMode_gloves.setIcon(QtGui.QIcon())  # helm
                    self.Cache[ "gloves" ] = blank

            if Aligned[6]:
                if Aligned[ 6 ] != blank:
                    self.charMode_boot.setText("")
                    self.charMode_boot.setIcon(QtGui.QIcon(ItemObj(i=Aligned[ 6 ])))  # boot
                    self.Cache[ "boots" ] = Aligned[ 6 ]
                    self.charMode_boot.setToolTip(self.GetItemInfo(Aligned[ 6 ]))
                else:
                    self.charMode_boot.setText("No Item")
                    self.charMode_boot.setIcon(QtGui.QIcon())  # helm
                    self.Cache[ "boots" ] = blank

            if Aligned[7]:
                if Aligned[ 7 ] != blank:
                    self.charMode_wing.setText("")
                    self.charMode_wing.setIcon(QtGui.QIcon(ItemObj(i=Aligned[ 7 ])))  # wing
                    self.Cache[ "wing" ] = Aligned[ 7 ]
                    self.charMode_wing.setToolTip(self.GetItemInfo(Aligned[ 7 ]))
                else:
                    self.charMode_wing.setText("No Wing")
                    self.charMode_wing.setIcon(QtGui.QIcon())  # helm
                    self.Cache[ "wing" ] = blank

            if Aligned[8]:
                if Aligned[ 8 ] != blank:
                    self.charMode_pet.setText("")
                    self.charMode_pet.setIcon(QtGui.QIcon(ItemObj(i=Aligned[ 8 ])))  # pet
                    self.Cache[ "pet" ] = Aligned[ 8 ]
                    self.charMode_pet.setToolTip(self.GetItemInfo(Aligned[ 8 ]))
                else:
                    self.charMode_pet.setText("No Pet")
                    self.charMode_pet.setIcon(QtGui.QIcon())  # helm
                    self.Cache[ "pet" ] = blank
            if Aligned[9]:
                if Aligned[ 9 ] != blank:
                    self.charMode_pendant.setText("")
                    self.charMode_pendant.setIcon(QtGui.QIcon(ItemObj(i=Aligned[ 9 ])))  # pend
                    self.Cache[ "pendant" ] = Aligned[ 9 ]
                    self.charMode_pet.setToolTip(self.GetItemInfo(Aligned[ 9 ]))
                else:
                    self.charMode_pendant.setText("None")
                    self.charMode_pendant.setIcon(QtGui.QIcon())  # helm
                    self.Cache[ "pendant" ] = blank
            if Aligned[10]:
                if Aligned[ 10 ] != blank:
                    self.charMode_ring2.setText("")
                    self.charMode_ring2.setIcon(QtGui.QIcon(ItemObj(i=Aligned[ 10 ])))  # ring right
                    self.Cache[ "ring2" ] = Aligned[ 10 ]
                    self.charMode_ring2.setToolTip(self.GetItemInfo(Aligned[ 10 ]))
                else:
                    self.charMode_ring2.setText("None")
                    self.charMode_ring2.setIcon(QtGui.QIcon())  # helm
                    self.Cache[ "ring2" ] = blank

            if Aligned[10]:
                if Aligned[ 11 ] != blank:
                    self.charMode_ring1.setText("")
                    self.charMode_ring1.setIcon(QtGui.QIcon(ItemObj(i=Aligned[ 11 ])))  # ring left
                    self.Cache[ "ring1" ] = Aligned[ 11 ]
                    self.charMode_ring1.setToolTip(self.GetItemInfo(Aligned[ 11 ]))
                else:
                    self.charMode_ring1.setText("None")
                    self.charMode_ring1.setIcon(QtGui.QIcon())  # helm
                    self.Cache[ "ring1" ] = blank
        ItemPrevSetter()

    def cmode_helm(self):
        global iIndex
        if iIndex == 7:
            self.charMode_helm.setText("")
            self.charMode_helm.setIcon(QtGui.QIcon(self.PreviewImage(get=True)))
            self.EquipEngine(update="helm")
        else:  # already added to Dicts.
            self.Cache[ "helm" ] = "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF"

    def cmode_armor(self):
        global iIndex

        if iIndex == 8:
            self.charMode_armor.setText("")
            self.charMode_armor.setIcon(QtGui.QIcon(self.PreviewImage(get=True)))
            self.EquipEngine(update="armor")
        else:  # already added to Dicts.
            self.Cache[ "armor" ] = "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF"

    def cmode_pants(self):
        global iIndex

        if iIndex == 9:
            self.charMode_pants.setText("")
            self.charMode_pants.setIcon(QtGui.QIcon(self.PreviewImage(get=True)))
            self.EquipEngine(update="pants")
        else:  # already added to Dicts.
            self.Cache[ "pants" ] = "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF"

    def cmode_gloves(self):
        global iIndex
        if iIndex == 10:
            self.charMode_gloves.setText("")
            self.charMode_gloves.setIcon(QtGui.QIcon(self.PreviewImage(get=True)))
            self.EquipEngine(update="gloves")
        else:  # already added to Dicts.
            self.Cache[ "gloves" ] = "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF"

    def cmode_boot(self):
        global iIndex
        if iIndex == 11:
            self.charMode_boot.setText("")
            self.charMode_boot.setIcon(QtGui.QIcon(self.PreviewImage(get=True)))
            self.EquipEngine(update="boots")
        else:
            self.Cache[ "boots" ] = "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF"

    def cmode_wing(self):
        global iIndex
        if iIndex == 12:
            self.charMode_wing.setText("")
            self.charMode_wing.setIcon(QtGui.QIcon(self.PreviewImage(get=True)))
            self.EquipEngine(update="wing")
        else:
            self.Cache[ "wing" ] = "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF"

    def cmode_pendant(self):
        global iIndex
        if iIndex == 13:
            self.charMode_pendant.setText("")
            self.charMode_pendant.setIcon(QtGui.QIcon(self.PreviewImage(get=True)))
            self.EquipEngine(update="pendant")
        else:
            self.Cache[ "pendant" ] = "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF"

    def cmode_wep1(self):
        global iIndex
        restr = [7,8,9,10,11,12,13]
        if iIndex not in restr:
            self.charMode_weap1.setText("")
            self.charMode_weap1.setIcon(QtGui.QIcon(self.PreviewImage(get=True)))
            self.EquipEngine(update="wep1")
        else:
            self.Cache[ "wep1" ] = "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF"

    def cmode_wep2(self):
        global iIndex
        restr = [7,8,9,10,11,12,13]
        if iIndex not in restr:
            self.charMode_weap2.setText("")
            self.charMode_weap2.setIcon(QtGui.QIcon(self.PreviewImage(get=True)))
            self.EquipEngine(update="wep2")
        else:
            self.Cache[ "wep2" ] = "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF"

    def cmode_ring1(self):
        global iIndex
        if iIndex == 13:
            self.charMode_ring1.setText("")
            self.charMode_ring1.setIcon(QtGui.QIcon(self.PreviewImage(get=True)))
            self.EquipEngine(update="ring1")
        else:
            self.Cache[ "ring1" ] = "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF"

    def cmode_ring2(self):
        global iIndex
        if iIndex == 13:
            self.charMode_ring2.setText("")
            self.charMode_ring2.setIcon(QtGui.QIcon(self.PreviewImage(get=True)))
            self.EquipEngine(update="ring2")
        else:
            self.Cache[ "ring2" ] = "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF"

    def cmode_pet(self):
        global iIndex
        if iIndex == 13:
            self.charMode_pet.setText("")
            self.charMode_pet.setIcon(QtGui.QIcon(self.PreviewImage(get=True)))
            self.EquipEngine(update="pet")
        else:
            self.Cache[ "pet" ] = "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF"

    def VaultChange(self):
        _c = self.vaultCat.currentIndex()
        if _c == 2:  # equip
            self.AccountLabel.setText("Name:")
            self.AccountValue.clear()
            self.AccountValue.currentIndexChanged.connect(self.StringChanged)

            self.LoadCharacters()
            self.RestoreFloater(Pop=True)
            self.StringChanged()
        else:
            self.AccountLabel.setText("Account:")
            self.AccountValue.clear()
            self.LoadIds()

    def CommandMode(self, get=False):
        current = self.CancelNow.text()

        if get == False:
            self.Message.clear()
            if "On" in current:
                self.CancelNow.setText("Command: Off")
                self.Message.insertHtml("\nDB Connection Started..\n")

            else:
                self.Message.insertHtml("Instant Mode: Connection Off..\n")
                self.CancelNow.setText("Command: On")

        else:
            if "On" in current:
                return True
            else:
                return False

    def CommandEngine(self):
        """
        A = Item group (read Items.txt)
        B = Item sub-index (read Items.txt)
        C = Level (from 0 to 13)
        D = Skill (0 or 1)
        E = Luck (0 or 1)
        F = Option (0 to 7)
        G = Excellent optionR
        """
        global iIndex, iItem
        beast_c = "/beastitem"

        # clip
        self.CopyCommand()

        def GetAllOptObj():
            opt_Op0 = 0  # none opts.
            opt_Op1 = 0  # Zen after Huntung
            opt_Op2 = 0  # DSR
            opt_Op3 = 0  # ref
            opt_Op4 = 0  # DD
            opt_Op5 = 0  # mana
            opt_Op6 = 0  # hp
            _All = 0

            def GetOptions28():
                self.Option_Options.value()

            def GetAncient():
                opt = self.AncientOptions.currentIndex()
                if opt == 1:
                    return "5"
                if opt == 2:
                    return "10"
                else:
                    return "0"

            try:
                if self.Option_Zen.isChecked():
                    opt_Op1 = 1
                if self.Option_DSR.isChecked():
                    opt_Op2 = 2
                if self.Option_Reflect.isChecked():
                    opt_Op3 = 4
                if self.Option_DD.isChecked():
                    opt_Op4 = 8
                if self.Option_Mana.isChecked():
                    opt_Op5 = 16
                if self.Option_HP.isChecked():
                    opt_Op6 = 32
                else:
                    _All = 0

            finally:
                _All = opt_Op1 + opt_Op2 + opt_Op3 + opt_Op4 + opt_Op5 + opt_Op6

                if GetAncient() == "0":
                    return str(_All)
                else:
                    return str(_All) + " " + str(GetAncient())

        def Getter28(): #// Fix for TypeErr
            try:
                return self.Get28Obj()[2]
            except:
                return "0"

        com = str(beast_c) + " " + str(iIndex) + " " + str(iItem) + " " + str(self.Option_Plus.value()) \
              + " 1 1 " + str(Getter28())+ " " + str(GetAllOptObj())
        self.lineEdit.setText(com)

    def EventsRadio(self):
        self.CommandEngine()

    def Process(self):
        "Process everything into Database"
        # self.Engine()
        _c = self.vaultCat.currentIndex()

        if _c == 0:
            self.Engine()

        elif _c == 1:
            pass

        elif _c == 2:
            # Use Proc engine
            self.ProcEquipEngine()

    def Serializer(self):
        """
        Serial Generator (None at the moment)
        """
        Pattern = ""
        if self.ItemCache:
            Pattern = self.ItemCache
        else:
            Pattern = "  "
        self.Serialized.setText(Pattern)

    def PreviewImage(self, get=False):
        """
        For Loading Image
        """
        import os, sys

        global iIndex, iPath, iItem
        CurrentItem = self.ItemView.currentItem().text()
        Path = "Data/Items/views/"
        Obj = self.ItemView_2
        Menus = {
            "Helms": 7,
            "Armors": 8,
            "Pants": 9,
            "Gloves": 10,
            "Boots": 11,
            "Swords": 0,
            "Axes": 1,
            "Scepters": 2,
            "Spears": 3,
            "Bows": 4,
            "Staffs": 5,
            "Shields": 6,
            "Wings/Jewel/Seeds": 12,
            "Pets / Accessories": 13,
            "Scrolls": 15,
            "Others": 14
        }
        ItemIndex = self.ItemView.currentRow()
        ParsedPath = Path + str(iIndex) + "/" + str(ItemIndex) + ".gif"
        iPath = str(ParsedPath)
        iItem = ItemIndex
        Obj.setIcon(QtGui.QIcon(_fromUtf8(ParsedPath)))
        # Change Name
        Str = "                           Excellent "
        FullName = Str + str(self.ItemView.currentItem().text())
        self.Details_Name.setText(FullName)

        # finally
        self.Serializer()
        if self.CommandMode(get=True):
            self.CommandEngine()
        if get:
            return str(ParsedPath)

    def ClearLog(self):
        self.Message.clear()

    def ShowArrays(self, val):
        """
        For Managing Items List
        """
        Cat = self.CategoriesView
        Selected = Cat.currentText()
        global iIndex

        def LoadArrays():
            """
            Data Loader
            """
            from Data.Database.ItemsData import armors, boots, gloves, helms, pants
            from Data.Database import Weapons

            List = self.ItemView
            global iIndex

            if Selected == "Helms":
                List.clear()
                List.addItems(helms.helmList)
                iIndex = 7

            elif Selected == "Armors":
                List.clear()
                List.addItems(armors.ArmorList)
                iIndex = 8

            elif Selected == "Pants":
                List.clear()
                List.addItems(pants.PantsList)
                iIndex = 9

            elif Selected == "Gloves":
                List.clear()
                List.addItems(gloves.GlovesList)
                iIndex = 10

            elif Selected == "Boots":
                List.clear()
                List.addItems(boots.BootsList)
                iIndex = 11

            elif Selected == "Swords":
                List.clear()
                iIndex = 0
                List.addItems(Weapons.Sword)

            elif Selected == "Axes":
                iIndex = 1
                List.clear()
                List.addItems(Weapons.Axe)

            elif Selected == "Scepters":
                iIndex = 2
                List.clear()
                List.addItems(Weapons.Scepters)

            elif Selected == "Spears":
                iIndex = 3
                List.clear()
                List.addItems(Weapons.Spears)

            elif Selected == "Bows":
                iIndex = 4
                List.clear()
                List.addItems(Weapons.Bows)

            elif Selected == "Staffs":
                iIndex = 5
                List.clear()
                List.addItems(Weapons.Staffs)

            elif Selected == "Shields":
                iIndex = 6
                List.clear()
                List.addItems(Weapons.Shields)

            elif Selected == "Wings/Jewel/Seeds":
                iIndex = 12
                List.clear()
                List.addItems(Weapons.Misc1)

            elif Selected == "Pets / Accessories":
                iIndex = 13
                List.clear()
                List.addItems(Weapons.Misc2)

            elif Selected == "Others":
                iIndex = 14
                List.clear()
                List.addItems(Weapons.Misc3)

            elif Selected == "Scrolls":
                iIndex = 15
                List.clear()
                List.addItems(Weapons.Misc4)


                # """
            #                "Wings/Jewel/Seeds":12,
            #                "Pets / Accessories":13,
            #                "Scrolls":15,
            #                "Others":14
            # """

        if Selected:
            LoadArrays()

    def PlusChanger(self, val):
        """
        Main Value of Pluses
        """
        self.Options_PlusValue.setText("+" + str(val))
        self.CommandEngine()

    def OptionChanger(self, val):
        """
        Main Value of Options
        """
        if val:
            if val <= 4:
                if self.CommandMode(get=True):
                    self.Options_OptionValue.setText("+" + str(4))
                else:
                    self.Options_OptionValue.setText("+" + str(0))

            elif val > 4 and val <= 8:
                if self.CommandMode(get=True):
                    self.Options_OptionValue.setText("+" + str(8))
                else:
                    self.Options_OptionValue.setText("+" + str(8))

            elif val > 8 and val <= 12:
                if self.CommandMode(get=True):
                    self.Options_OptionValue.setText("+" + str(12))
                else:
                    self.Options_OptionValue.setText("+" + str(12))

            elif val > 12 and val <= 16:
                if self.CommandMode(get=True):
                    self.Options_OptionValue.setText("+" + str(16))
                else:
                    self.Options_OptionValue.setText("+" + str(16))

            elif val > 16 and val <= 20:
                if self.CommandMode(get=True):
                    self.Options_OptionValue.setText("+" + str(20))
                else:
                    self.Options_OptionValue.setText("+" + str(20))

            elif val > 20 and val <= 24:
                if self.CommandMode(get=True):
                    self.Options_OptionValue.setText("+" + str(24))
                else:
                    self.Options_OptionValue.setText("+" + str(24))

            elif val > 24 and val <= 26:
                if self.CommandMode(get=True):
                    self.Options_OptionValue.setText("+" + str(28))
                else:
                    self.Options_OptionValue.setText("+" + str(28))

        self.CommandEngine()

        # self.lineEdit.setText(to_replace)

    def ExecuteFn(self, enableMulti=False, Queer=None, iName=None, PackExec=False):
        """
        Connection Loader
        """
        self.CurrentUser = self.AccountValue.currentText()
        self.PackageReciever = self.Package_UsernameList.text()
        if self.CommandMode(get=True):
            self.CommandEngine()
        if True:
            #self.CommandEngine()

            if "Choose Account" in self.CurrentUser:
                self.ClearLog()
                self.Message.insertHtml("No Account/Character Selected!")
            else:
                if self.con == None or self.Cursor == None:
                    self.ClearLog()
                    self.Message.insertHtml("No Database Connection!")
                else:
                    Q = """Update warehouse set items = 0x14344143 where AccountId = 'loginbeastm'""".replace(
                        "0x14344143", str(Queer)).replace("loginbeastm", str(self.CurrentUser))

                    Q1 = """Update warehouse set items = 0x14344143 where AccountId = 'loginbeastm'""".replace(
                        "0x14344143", str(PackExec)).replace("loginbeastm", str(self.PackageReciever))

                    if PackExec != False:
                        print PackExec
                        Execute = self.Cursor.execute(Q1)
                        Execute.commit()
                        return "Sucessfully spawned Package!"

                    else:
                        print "Hereee Q"
                        Execute = self.Cursor.execute(Q)
                        Execute.commit()
                        self.Serialized.setText("   " + str(Queer).replace("0x", "") + " - Executed")
                        self.Message.insertHtml("\n Warehouse: " + str(self.ItemView.currentItem().text()) + "\n")
        return Queer

    #  02 7F 6B B1 2D 77 00 7F 05 70 3E 00 00 00 00 00

    def SocketEngine(self,init=False,get=False,KOV=None):
        SocketsStruct = {
                'FF' : 'EMPTY (No Socket)',
                '00' : 'EMPTY (No Mounted Slot)',
                '01' : 'Fire (Increase Damage/SkillPower (*lvl)) + 20',
                '33' : 'Fire (Increase Damage/SkillPower (*lvl)) + 400',
                '65' : 'Fire (Increase Damage/SkillPower (*lvl)) + 400',
                '97' : 'Fire (Increase Damage/SkillPower (*lvl)) + 400',
                'C9' : 'Fire (Increase Damage/SkillPower (*lvl)) + 400',

                '02' : 'Fire (Increase Attack Speed) + 7',
                '34' : 'Fire (Increase Attack Speed) + 1',
                '66' : 'Fire (Increase Attack Speed) + 1',
                '98' : 'Fire (Increase Attack Speed) + 1',
                'CA' : 'Fire (Increase Attack Speed) + 1',

                '03' : 'Fire (Increase Maximum Damage/Skill Power) + 30',
                '35' : 'Fire (Increase Maximum Damage/Skill Power) + 1',
                '67' : 'Fire (Increase Maximum Damage/Skill Power) + 1',
                '99' : 'Fire (Increase Maximum Damage/Skill Power) + 1',
                'CB' : 'Fire (Increase Maximum Damage/Skill Power) + 1',

                '04' : 'Fire (Increase Minimum Damage/Skill Power) + 20',
                '36' : 'Fire (Increase Minimum Damage/Skill Power) + 1',
                '68' : 'Fire (Increase Minimum Damage/Skill Power) + 1',
                '9A' : 'Fire (Increase Minimum Damage/Skill Power) + 1',
                'CC' : 'Fire (Increase Minimum Damage/Skill Power) + 1',

                '05' : 'Fire (Increase Damage/Skill Power) + 20',
                '37' : 'Fire (Increase Damage/Skill Power) + 1',
                '69' : 'Fire (Increase Damage/Skill Power) + 1',
                '9B' : 'Fire (Increase Damage/Skill Power) + 1',
                'CD' : 'Fire (Increase Damage/Skill Power) + 1',

                '06' : 'Fire (Decrease AG Use) + 40',
                '38' : 'Fire (Decrease AG Use) + 1',
                '6A' : 'Fire (Decrease AG Use) + 1',
                '9C' : 'Fire (Decrease AG Use) + 1',
                'CE' : 'Fire (Decrease AG Use) + 1',

                '0B' : 'Water (Increase Defense Success Rate) + 10',
                '3D' : 'Water (Increase Defense Success Rate) + 1',
                '6F' : 'Water (Increase Defense Success Rate) + 1',
                'A1' : 'Water (Increase Defense Success Rate) + 1',
                'D3' : 'Water (Increase Defense Success Rate) + 1',

                '0C' : 'Water (Increase Defense) + 30',
                '3E' : 'Water (Increase Defense) + 1',
                '70' : 'Water (Increase Defense) + 1',
                'A2' : 'Water (Increase Defense) + 1',
                'D4' : 'Water (Increase Defense) + 1',

                '0D' : 'Water (Increase Defense Shield) + 7',
                '3F' : 'Water (Increase Defense Shield) + 1',
                '71' : 'Water (Increase Defense Shield) + 1',
                'A3' : 'Water (Increase Defense Shield) + 1',
                'D5' : 'Water (Increase Defense Shield) + 1',

                '0E' : 'Water (Damage Reduction) + 4',
                '40' : 'Water (Damage Reduction) + 1',
                '72' : 'Water (Damage Reduction) + 1',
                'A3' : 'Water (Damage Reduction) + 1',
                'D6' : 'Water (Damage Reduction) + 1',

                '0F' : 'Water (Damage Reflections) + 5',
                '41' : 'Water (Damage Reflections) + 1',
                '73' : 'Water (Damage Reflections) + 1',
                'A5' : 'Water (Damage Reflections) + 1',
                'D7' : 'Water (Damage Reflections) + 1',

                '11' : 'Ice (Increases + Rate of Life After Hunting) + 8',
                '43' : 'Ice (Increases + Rate of Life After Hunting) + 49',
                '75' : 'Ice (Increases + Rate of Life After Hunting) + 50',
                'A7' : 'Ice (Increases + Rate of Life After Hunting) + 51',
                'D9' : 'Ice (Increases + Rate of Life After Hunting) + 52',

                '12' : 'Ice (Increases + Rate of Mana After Hunting) + 8',
                '44' : 'Ice (Increases + Rate of Mana After Hunting) + 49',
                '76' : 'Ice (Increases + Rate of Mana After Hunting) + 50',
                'A8' : 'Ice (Increases + Rate of Mana After Hunting) + 51',
                'DA' : 'Ice (Increases + Rate of Mana After Hunting) + 52',

                '13' : 'Ice (Increase Skill Attack Power) + 37',
                '45' : 'Ice (Increase Skill Attack Power) + 1',
                '77' : 'Ice (Increase Skill Attack Power) + 1',
                'A9' : 'Ice (Increase Skill Attack Power) + 1',
                'DB' : 'Ice (Increase Skill Attack Power) + 1',

                '14' : 'Ice (Increase Attack Success Rate) + 25',
                '46' : 'Ice (Increase Attack Success Rate) + 1',
                '78' : 'Ice (Increase Attack Success Rate) + 1',
                'AA' : 'Ice (Increase Attack Success Rate) + 1',
                'DC' : 'Ice (Increase Attack Success Rate) + 1',

                '15' : 'Ice (Item Duarability Reinforcement) + 30',
                '47' : 'Ice (Item Duarability Reinforcement) + 1',
                '79' : 'Ice (Item Duarability Reinforcement) + 1',
                'AB' : 'Ice (Item Duarability Reinforcement) + 1',
                'DD' : 'Ice (Item Duarability Reinforcement) + 1',

                '16' : 'Wind (Increase Life AutoRecovery) + 8',
                '48' : 'Wind (Increase Life AutoRecovery) + 1',
                '7A' : 'Wind (Increase Life AutoRecovery) + 1',
                'AC' : 'Wind (Increase Life AutoRecovery) + 1',
                'DE' : 'Wind (Increase Life AutoRecovery) + 1',

                '17' : 'Wind (Increase Maximum Life) + 4',
                '49' : 'Wind (Increase Maximum Life) + 1',
                '7B' : 'Wind (Increase Maximum Life) + 1',
                'AD' : 'Wind (Increase Maximum Life) + 1',
                'DF' : 'Wind (Increase Maximum Life) + 1',

                '18' : 'Wind (Increase Maximum Mana) + 4',
                '4A' : 'Wind (Increase Maximum Mana) + 1',
                '7C' : 'Wind (Increase Maximum Mana) + 1',
                'AE' : 'Wind (Increase Maximum Mana) + 1',
                'E0' : 'Wind (Increase Maximum Mana) + 1',

                '19' : 'Wind (Increase Mana AutoRecovery) + 7',
                '4B' : 'Wind (Increase Mana AutoRecovery) + 1',
                '7D' : 'Wind (Increase Mana AutoRecovery) + 1',
                'AF' : 'Wind (Increase Mana AutoRecovery) + 1',
                'E1' : 'Wind (Increase Mana AutoRecovery) + 1',

                '1A' : 'Wind (Increase Maximum AG) + 25',
                '4C' : 'Wind (Increase Maximum AG) + 1',
                '7E' : 'Wind (Increase Maximum AG) + 1',
                'B0' : 'Wind (Increase Maximum AG) + 1',
                'E2' : 'Wind (Increase Maximum AG) + 1',

                '1B' : 'Wind (Increase AG Amount) + 3',
                '4D' : 'Wind (Increase AG Amount) + 1',
                '7F' : 'Wind (Increase AG Amount) + 1',
                'B1' : 'Wind (Increase AG Amount) + 1',
                'E3' : 'Wind (Increase AG Amount) + 1',

                '1E' : 'Lightning (Increase Excellent Damage) + 15',
                '50' : 'Lightning (Increase Excellent Damage) + 1',
                '82' : 'Lightning (Increase Excellent Damage) + 1',
                'B4' : 'Lightning (Increase Excellent Damage) + 1',
                'E6' : 'Lightning (Increase Excellent Damage) + 1',

                '1F' : 'Lightning (Increase Excellent Damage Success Rate) + 10',
                '51' : 'Lightning (Increase Excellent Damage Success Rate) + 1',
                '83' : 'Lightning (Increase Excellent Damage Success Rate) + 1',
                'B5' : 'Lightning (Increase Excellent Damage Success Rate) + 1',
                'E7' : 'Lightning (Increase Excellent Damage Success Rate) + 1',

                '20' : 'Lightning (Increase Critical Damage) + 30',
                '52' : 'Lightning (Increase Critical Damage) + 1',
                '84' : 'Lightning (Increase Critical Damage) + 1',
                'B6' : 'Lightning (Increase Critical Damage) + 1',
                'E8' : 'Lightning (Increase Critical Damage) + 1',

                '21' : 'Lightning (Increase Critical Damage Success Rate) + 8',
                '53' : 'Lightning (Increase Critical Damage Success Rate) + 1',
                '85' : 'Lightning (Increase Critical Damage Success Rate) + 1',
                'B7' : 'Lightning (Increase Critical Damage Success Rate) + 1',
                'E9' : 'Lightning (Increase Critical Damage Success Rate) + 1',

                '25' : 'Ground (Increase Stamina) + 30',
                '57' : 'Ground (Increase Stamina) + 1',
                '89' : 'Ground (Increase Stamina) + 1',
                'BB' : 'Ground (Increase Stamina) + 1',
                'ED' : 'Ground (Increase Stamina) + 1'
              }
        if init:
            _Cached = []
            for Hex,Sock in SocketsStruct.items():
                _Cached.append(Sock)
            self.SocketOptions_Val1.clear()
            self.SocketOptions_Val2.clear()
            self.SocketOptions_Val3.clear()
            self.SocketOptions_Val1.addItems(sorted(_Cached))
            self.SocketOptions_Val2.addItems(sorted(_Cached))
            self.SocketOptions_Val3.addItems(sorted(_Cached))

        if get:
            return SocketsStruct

    def Get28Obj(self,getopt=False,getval=False,getvalcomm=False):
        "Returns Generated +28 option fix"
        # struct [ plus bytes 2, plus bytes 15?, plus bytes (for comm), option% ]
        opt_kv = {
              0:(0,0,0,'0%'),
              4:(1,0,1,'4%'),
              8:(2,0,2,'8%'),
              12:(3,0,3,'12%'),
              16:(0,0,4,'16%'),
              20:(1,64,5,'20%'),
              24:(2,64,6,'24%'),
              28:(3,64,7,'28%'),
                  }
        _val = self.Option_Options.value()
        try:
            if _val <= 14: #// 4,8,12

                if _val == 0: #// none
                    return opt_kv['0']

                elif _val <= 4: #// 4%
                    return opt_kv[4]

                elif _val > 4 and _val <= 8: #// 8%
                    return opt_kv[8]

                elif _val > 8 and _val <= 12: #// 12%
                    return opt_kv[12]

            if _val > 14: #// 16,20,24,28

                if _val == 15 or _val == 16:    #// 16%
                    return opt_kv[16]

                elif _val >= 17 and _val <= 20: #// 20%
                    return opt_kv[20]

                elif _val >= 20 and _val <= 24: #// 24%
                    return opt_kv[24]

                elif _val >= 24: #// 28%
                    return opt_kv[28]
        except:
            return opt_kv[0]
    def Engine(self, get=False,getopt28=False,pack=False,pack_anc=False,pack_wep=False,pack_group=False,pack_item=False):
        "Hex Generation and Process"
        # Update warehouse set items = 0x1BFF6F000000007F000000FFFFFFFFFF where AccountId='mark'
        # Analyze:
        global iItem, iIndex, iParse  # Number, ItemNumber

        self.Start = '0x'
        self.ItemNum, self.ItemGroup = iItem, iIndex

        opt_28 = 49
        opt_luck = 65

        def HexClean(Hex):
            "Returns a Clean Hexed Integer"
            if int(Hex) <= 15 and int(Hex) != 16:
                if self.ItemNum == 0:
                    return '00'
                else:
                    r_ox = str(hex(int(Hex))).replace("0x", "")
                    return "0" + r_ox
            else:
                return str(hex(int(Hex))).replace("0x", "")

        def GetInumObj():
            "Returns Item Number (Current)"
            if pack:
                if pack_item:
                    return HexClean(pack_item)
            else:
                return HexClean(self.ItemNum)

        def GetDurObj():
            "Returns Durablity in Hex"
            return 'FF'


        def GetSerialObj():
            "Returns a Unique Item Serial Code (HEX)"
            from random import choice, randrange, shuffle

            Serialize = ""
            Serializer = lambda x, y: randrange(x, y)
            SerializerChar = lambda: choice("ABCDEFGHIJKLMNOPQRSTUVWXYZ")
            Int = [ str(Serializer(123, 145)), str(Serializer(113, 198)), str(Serializer(102, 107)),
                    str(Serializer(130, 174)) ]
            Char = [ str(SerializerChar()), str(SerializerChar()), str(SerializerChar()),
                     str(SerializerChar()) ]  # not needed
            Shuffle = Int[ 0 ] + Char[ 0 ] + Int[ 1 ] + Char[ 1 ] + Int[ 2 ] + Char[ 2 ] + Int[ 3 ] + Char[
                3 ]  # not needed
            Shuffled = [ x for x in Int ]
            shuffle(Shuffled)
            __Hexed = [ ]
            for bit in Shuffled:
                __Hexed.append(str(hex(int(str(bit)))).replace("0x", "").upper())
            Bits_8 = "".join(__Hexed)
            return str(Bits_8)  # str(Bits_8) #str(Bits_8)

        def GetAllOptObj():
            opt_Op0 = 0  # none opts.
            opt_Op1 = 0  # Zen after Huntung
            opt_Op2 = 0  # DSR
            opt_Op3 = 0  # ref
            opt_Op4 = 0  # DD
            opt_Op5 = 0  # mana
            opt_Op6 = 0  # hp
            _All = 0

            try:
                if self.Option_Zen.isChecked():
                    opt_Op1 = 1
                else:
                    opt_Op1 = 0

                if self.Option_DSR.isChecked():
                    opt_Op2 = 2
                else:
                    opt_Op2 = 0

                if self.Option_Reflect.isChecked():
                    opt_Op3 = 4
                else:
                    opt_Op3 = 0

                if self.Option_DD.isChecked():
                    opt_Op4 = 8
                else:
                    opt_Op4 = 0

                if self.Option_Mana.isChecked():
                    opt_Op5 = 16
                else:
                    opt_Op5 = 0

                if self.Option_HP.isChecked():
                    opt_Op6 = 32
                else:
                    opt_Op6 = 0

            finally:
                try:
                    _28 = self.Get28Obj()[1]
                except:
                    _28 = 0

                _All = opt_Op1 + opt_Op2 + opt_Op3 + opt_Op4 + opt_Op5 + opt_Op6 + _28
                return HexClean(_All)

        def GetPlusObj():
            "Returns Clean Plus (Includes the add opts.)"
            def GetLuck():
                if self.Option_Zen_2.isChecked():
                    return 4
                else:
                    return 0
            def GetAdditional28():
                try:
                    _opt = self.Get28Obj()[0]
                except:
                    _opt = 0
                return _opt
            Base = 8
            itemPlus = int(str(self.Options_PlusValue.text()).replace("+", ""))
            Eval = lambda x,y: HexClean(GetAdditional28()+GetLuck()+(x*y))
            return Eval(Base,itemPlus)

        def GetAncientObj():
            Get = self.AncientOptions.currentIndex()
            if pack:
                if pack_anc:
                    if pack_anc == "A1":
                        return "05"
                    elif pack_anc == "A2":
                        return "0A"
                    elif pack_anc == "A3":
                        return "05"
                    else:
                        return "00"
            else:
                if Get == 2:
                    return "05"
                elif Get == 3:
                    return "0A"
                else:
                    return "00"

        def GetItemGroupObj():
            global iIndex
            if pack:
                if pack_group:
                    if pack_group > 9:
                        _hh = hex(pack_group)
                        _rrh = str(_hh).replace("0x", "")
                        return str(_rrh).upper()

                    else:
                        return str(pack_group)
            else:
                if iIndex > 9:
                    _h = hex(iIndex)
                    _rh = str(_h).replace("0x", "")
                    return _rh.upper()

                else:
                    return str(iIndex)

        def GetPinkObj():
            if self.Option_380.isChecked():
                return "8"
            else:
                return "0"

        def GetHarmonyObj():
            Yellow = self.Option_Yellow1.currentIndex()
            YellowPlus = self.Option_Yellow2.currentIndex()
            return str(Yellow) + str(YellowPlus)

        def GetSocketsObj():
            "Socket Maker"
            if self.SocketOptions.currentIndex() == 1:

                def GetKey(k):
                    FullSocketsStruct = self.SocketEngine(get=True)
                    for hex,sock in FullSocketsStruct.iteritems():
                        if sock == k:
                            return str(hex)

                _s1, _s2,_s3 = 'FF','FF','FF' # should be 10b
                s1val = self.SocketOptions_Val1.currentText()
                s2val = self.SocketOptions_Val2.currentText()
                s3val = self.SocketOptions_Val3.currentText()
                FSock1 = GetKey(str(s1val))
                FSock2 = GetKey(str(s2val))
                FSock3 = GetKey(str(s3val))
                FSock = FSock1 + FSock2 + FSock3 + "0000" # 3 socks BMU Default
                return FSock
            else:
                return "FFFFFFFFFF"

        def ProcObj():
            global iParse
            # bytes 1-2 = 0x
            # bytes 3-4 = ItemIndex
            Parse = self.Start + str(GetInumObj()).upper() +""+ str(GetPlusObj()) + str(GetDurObj()).upper() + str(
                GetSerialObj()).upper() + str(GetAllOptObj()).upper() + str(GetAncientObj()).upper() + str(
                GetItemGroupObj()) + str(GetPinkObj()).upper() + str(GetHarmonyObj()).upper() + str(
                GetSocketsObj()).upper()

            # print "fN(Engine)Obj=ParseProbObj"+Parse
            # print " num: " + str(GetInumObj())
            #print " plus: " +str(GetPlusObj())
            # print " dur: "+ str(GetDurObj())
            # print " serial: " + str(GetSerialObj())
            #print " allopt: " + str(GetAllOptObj())
            # print " ancient: " + str(GetAncientObj())
            # print " group: " + str(GetItemGroupObj())
            # print " pink: "+str(GetPinkObj())
            # print " yellow: "+str(GetHarmonyObj())
            # print " sock: "+str(GetSocketsObj())

            if get:
                self.ItemCache = str(self.ItemView.currentItem().text())
                self.ItemSerialCache = str(GetSerialObj())
                iParse = Parse

                # self.GetParsed(Q=Parse)
            if pack:
                self.ItemSerialCache = str(GetSerialObj())
                iParse = Parse

            if getopt28: pass
                #_opt28 = Get28Obj()
                #return tuple(_opt28)

            else:
                self.ExecuteFn(Queer=Parse)

        ProcObj()

    def GetParsed(self, Q):
        return str(Q)


    # Package Manager Properties

    def PackageManagerRes(self,getTypes=False):
        from Data.Database import BeastData
        # BeastData.py (Containts BeastDonation's Item Struct)
        Chars = {
    "Blade Master":1,
    "Muse Elf":2,
    "Soul Master":3,
    "Rage Fighter":4,
    "Magic Gladiator":5,
    "Dark Lord":6,
    "Summoner":7
        }

        Types = {
    "Uber Package 5%":0,
    "Uber Package 10%":1,
    "Tier Package":2,
    "Season 4 Package":3,
    "Miscellaneous":4,
        }

        try:
            self.Package_ChooseClass.clear()
            self.Package_Choose.clear()
            self.Package_ChooseClass.addItems(["Class All*"])
            self.Package_Choose.addItems(sorted(Types.keys()))
        finally:
            self.PackageMessageIncludes.setText("Loaded Characters and Categories!")

        if getTypes:
            return Types

    def RestoreFloater(self,Pop=False):
        "Restore Optionizer"
        if Pop:
            print "Showed"
            self.viewFloat.setFloating(True)
            self.viewFloat.show()
        else:
            self.viewFloat.setGeometry(QtCore.QRect(8, 23, 274, 317))

            self.viewFloat.setFloating(False)

    def ClassChanged_PM(self):
        ClassIndex = self.Package_ChooseClass.currentIndex()
        ClassName = self.Package_ChooseClass.currentText()
        if "Choose" in ClassName:
            return False
        else:
            return ClassName

    def PremiumParsedDest(self,P):
        pass

    def PremiumChanged_PM(self):
        from Data.Database.ItemsData.armors import ArmorList as AllItems
        global iParse
        PremIndex = self.Package_Choose.currentIndex()
        PremName = self.Package_Choose.currentText()
        PackObjCache = self.Packages
        Clean = AllItems # bind
        for Itm in Clean:
            self.Packages[str(Itm)] = str(Clean.index(Itm))

        if not self.ClassChanged_PM():
            return False
        else:
            try:
                pass
                LEngine = self.Package_List
                LEngine.clear()
                Items = self.Packages.keys()
                NList = []
                for item in Items:
                    NList.append(str(item).replace("Armor","Package"))
                LEngine.addItems(sorted(NList))

            finally:
                self.PackageMessageIncludes.append("\n{itc} Loaded!".format(itc=len(Items)))
                self.PremiumEngineProc(Exec=True)

    def PremiumEngineProc(self,Cur=None,Exec=False):
        global iParse

        _PItemName = None
        try:
            _Item = self.Package_List.currentItem().text()
            _PItemName = _Item
        except:
            pass

        def CallGener(i=None):
            global iParse

            if not i:
                pass
            else:
                # Args: A1,A2,A3 (Ancient Types)
                # pack=True (Enable Parser for Package)
                # pack_group (Item Group)
                _NList = []
                ItemSerial = self.ItemSerialCache
                ItemIndex = self.Packages[str(i).replace("Package","Armor")]
                # for item options for ancient: A1,A2,A3
                ForItem = lambda ancient,item,itemgroup: self.Engine(pack=True,pack_anc=ancient,pack_group=itemgroup,pack_item=item) if ancient else "Error"
                _ParsedItems = ["0x",]
                isAncient1 = lambda: True if "5%" in self.Package_Choose.currentText() else False
                isAncient2 = lambda: True if "10%" in self.Package_Choose.currentText() else False
                isAncient3 = lambda: True if "Tier" in self.Package_Choose.currentText() else False
                _whichAnc = "00"

                if True:
                    if isAncient1():
                        _whichAnc = "A1"
                    elif isAncient2():
                        _whichAnc = "A2"
                    elif isAncient3():
                        _whichAnc = "A3"

                # helm
                ForItem(_whichAnc,ItemIndex,7)
                _ParsedItems.append(str(iParse).replace("0x",""))

                # armor
                ForItem(_whichAnc,ItemIndex,8)
                _ParsedItems.append(str(iParse).replace("0x",""))

                # pants
                ForItem(_whichAnc,ItemIndex,9)
                _ParsedItems.append(str(iParse).replace("0x",""))

                # gloves
                ForItem(_whichAnc,ItemIndex,10)
                _ParsedItems.append(str(iParse).replace("0x",""))

                # boots
                ForItem(_whichAnc,ItemIndex,11)
                _ParsedItems.append(str(iParse).replace("0x",""))

                Cleared = "".join(_ParsedItems)

                return Cleared

        if Exec:
            Parsed = CallGener(i=_PItemName)
            #self.ExecuteFn(PackExec=Parsed)
        else:
            Parsed = CallGener(i=_PItemName)
            if not Parsed:
                self.PackageMessageIncludes.clear()
                self.PackageMessageIncludes.insertHtml("<b>NO ITEM SELECTED!</b>")
                return False
            else:
                self.ExecuteFn(PackExec=Parsed)
                self.PackageMessageIncludes.clear()
                self.PackageMessageIncludes.insertHtml("<b>Item has been Spawned Sucessfuly!</b>")


# Option Manager
    def OptionMan_Loader(self):
        CValue = self.ConnectionValue
        CVersion = self.Connection_Version
        CUser = self.Connection_userName
        CPass = self.Connection_passWord
        OptionsRaw = [0,1,3,4]
        CValue.setPlainText(str(self.Credentials[1]))
        CVersion.setPlainText(str(self.Credentials[0]))
        CUser.setPlainText(str(self.Credentials[3]))
        CPass.setText(str(self.Credentials[4]))

    def OptMan_Connect(self):

        CValue = self.ConnectionValue.toPlainText()
        CVersion = self.Connection_Version.toPlainText()
        CUser = self.Connection_userName.toPlainText()
        CPass = self.Connection_passWord.text()
        self.Connection_passWord.setEchoMode(QtGui.QLineEdit.Password)
        CDB = "MuOnline"



        if "|" in CVersion:
            sp = str(CVersion).split("|")
            CVersion = sp[0]
            CDB = sp[1]

        Creds = [CVersion,CValue,CDB,CUser,CPass]

        def ReplOpts(v):
            

        if CValue and CVersion and CUser and CPass:
            try:
                self.LoadConnection(Reload=True,ReloadVals=Creds)
                self.MessageEngine("Connection Success!","BeastMaker is now Connected to {ip}".format(ip=CValue))
                ReplOpts(Creds) #// Write Options back
            except:
                self.MessageEngine("Connection Failed","Unable to Connect to Server! \n\nDatabase: {db}\nDriver: {d} \nIP/Instance: {ip} \nUsername: {u}".format(db=CDB,d=CVersion,ip=CValue,u=CUser))
        else:
            self.MessageEngine("Values Required!", "Please fill everything Up!")


import coresec as bmuo_rc

if __name__ == "__main__":
    import sys, time
    STrue = False
    Bmuapp = QtGui.QApplication(sys.argv)

    if STrue:
    # Create and display the splash screen
        splash_pix = QtGui.QPixmap('im/splash.png')
        splash = QtGui.QSplashScreen(splash_pix, QtCore.Qt.WindowStaysOnTopHint)
        splash.setMask(splash_pix.mask())
        splash.show()
        Bmuapp.processEvents()
        time.sleep(2)

    BeastMaker = QtGui.QWidget()
    Bmuui = Ui_BeastMaker()
    Bmuui.setupUi(BeastMaker)
    # BeastMaker.show()
    if STrue:
        splash.finish(BeastMaker)
    sys.exit(Bmuapp.exec_())
